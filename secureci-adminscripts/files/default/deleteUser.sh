#! /bin/bash

# Must be root as root
if [ "$(/usr/bin/id -u)" != "0" ]; then
   echo "$0 must be run as root" 1>&2
   exit 1
fi

echo
echo "Deleting a user account that can be used"
echo "to log in to the system both via web and cl."
echo

#get user information
read -er -p "Enter username for login user: " username
userdel -f -r $username

if [ -a /var/lib/gitblit ]; then
  rm -f /var/lib/gitblit/data/ssh/$username.keys
fi

if [ -a /var/lib/nexus ]; then
  #add the user to nexus
  a=`curl -s -H "Content-Type: application/json" --user admin:admin123 -X POST  -d "{ \"name\": \"delete\", \"type\": \"groovy\", \"content\": \"security.securitySystem.deleteUser('$username','default')\"}" http://localhost:8181/nexus/service/siesta/rest/v1/script/`
  e=`curl -s -H "Content-Type: text/plain"       --user admin:admin123 -X POST   http://localhost:8181/nexus/service/siesta/rest/v1/script/delete/run`
  d=`curl -s -H "Content-Type: application/json" --user admin:admin123 -X DELETE http://localhost:8181/nexus/service/siesta/rest/v1/script/delete`
fi

#add the new user to htpasswd
htpasswd -D /var/lib/passwd/dav_svn.passwd $username

echo
echo "Done. user $username has been removed."
echo

exit 0
