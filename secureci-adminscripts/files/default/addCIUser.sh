#! /bin/bash

# Must be root as root
if [ "$(/usr/bin/id -u)" != "0" ]; then
   echo "$0 must be run as root" 1>&2
   exit 1
fi

echo
echo "Creating a CI user that can be used for Git," 
echo "Jenkins, Sonar, and Nexus"
echo

#get user information
read -er -p "Enter username for CI user: " username
password=
while [ -z "$password" ]; do
  read -s -p "Enter password for CI user '$username': " password
  echo
  if [ -n "$password" ]; then
    read -s -p "Retype password for CI user '$username': " password2
    echo
    if [ "$password2" != "$password" ]; then
      password=
      echo "Passwords must match!" 1>&2
    fi
  fi
done

if [ -a /var/lib/gitblit ]; then
  #add the new user to git
  echo "[user \"$username\"]
	password = \"#externalAccount\"
	accountType = HTPASSWD
	emailMeOnMyTicketChanges = true" >> /var/lib/gitblit/data/users.conf

  #add the jenkins ssh key to the ci user
  if [ ! -d /var/lib/gitblit/data/ssh ]; then
    mkdir -p /var/lib/gitblit/data/ssh;
  fi
  echo -e "RW $(cat /var/lib/jenkins/.ssh/secureci.pub)" > /var/lib/gitblit/data/ssh/$username.keys
fi

if [ -a /var/lib/nexus ]; then
  #add the user to nexus
  a=`curl -s -H "Content-Type: application/json" --user admin:admin123 -X POST  -d "{ \"name\": \"user\", \"type\": \"groovy\", \"content\": \"security.addUser('$username','','','',true,'$password',['nx-anonymous'])\"}" http://localhost:8181/nexus/service/siesta/rest/v1/script/`
  e=`curl -s -H "Content-Type: text/plain"       --user admin:admin123 -X POST   http://localhost:8181/nexus/service/siesta/rest/v1/script/user/run`
  d=`curl -s -H "Content-Type: application/json" --user admin:admin123 -X DELETE http://localhost:8181/nexus/service/siesta/rest/v1/script/user`
fi

#add the new user to htpasswd
htpasswd -b /var/lib/passwd/dav_svn.passwd $username $password

if [ -a /var/lib/jenkins ]; then
  #add the user into Jenkins keys
  curl -X POST 'http://localhost:8080/jenkins/credentials/store/system/domain/_/createCredentials' \
  --data-urlencode "json={ \
    '': '0', \
    'credentials': { \
      'scope': 'GLOBAL', \
      'id': 'ci', \
      'username': '$username', \
      'password': '$password', \
      'description': 'CI User', \
      '\$class': 'com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl' \
    } \
  }"
 
  if [ -a /var/lib/nexus ]; then
    #setup our nexus creds
    if [ ! -d /var/lib/jenkins/.m2 ]; then
        mkdir /var/lib/jenkins/.m2
        chmod 755 /var/lib/jenkins/.m2
    fi
    echo "<settings xmlns='http://maven.apache.org/SETTINGS/1.0.0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://maven.apache.org/SETTINGS/1.0.0 https://maven.apache.org/xsd/settings-1.0.0.xsd'>
    <servers>
        <server>
            <id>nexus</id>
            <username>$username</username>
            <password>$password</password>
        </server>
    </servers>
</settings>" > /var/lib/jenkins/.m2/setting.xml
    chown -R jenkins.jenkins /var/lib/jenkins/.m2
    chmod 664 /var/lib/jenkins/.m2/setting.xml
  fi
fi

echo
echo "Done. user $username has been added to the tools."
echo

exit 0