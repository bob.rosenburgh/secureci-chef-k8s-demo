#! /bin/bash

# Must be root as root
if [ "$(/usr/bin/id -u)" != "0" ]; then
   echo "$0 must be run as root" 1>&2
   exit 1
fi

DAV_SVN_PW_FILE=/var/lib/passwd/dav_svn.passwd
DAV_SVN_AUTHZ_FILE=/var/lib/passwd/dav_svn.authz
TRAC_PROJECT_DIR=/var/lib/trac/secureci/

echo
echo "Creating an administrative user for Subversion and Trac."
echo

username=
while [ -z "$username" ]; do
  read -er -p "Enter username for the Subversion and Trac administrator: " username
  EXISTS=`/bin/grep "^\s*$username" $DAV_SVN_PW_FILE`
  if [ -n "$username" -a -n "$EXISTS" ]; then
    echo "Subversion user $username already exists."
    echo
    username=
  fi
done

CREATED=1
while [ 0 -ne $CREATED ]; do
  /usr/bin/htpasswd $DAV_SVN_PW_FILE $username
  CREATED=$?
done

/bin/sed -i'.bak' -e"s/^\([ 	]*admin[ 	]*=.*\)[ 	]*/\1,$username/;s/^\([ 	]*admin[ 	]*=[ 	]*\),/\1/" $DAV_SVN_AUTHZ_FILE

trac-admin $TRAC_PROJECT_DIR permission add $username TRAC_ADMIN

echo
echo "Done. User $username has been added with administrative privileges in Trac and " 
echo "full access in Subversion through HTTPS."
echo

exit 0
