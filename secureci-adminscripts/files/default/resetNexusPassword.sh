#! /bin/bash

# Must be root as root
if [ "$(/usr/bin/id -u)" != "0" ]; then
   echo "$0 must be run as root" 1>&2
   exit 1
fi

echo
echo "Resetting Nexus admin password."
echo

password=
while [ -z "$password" ]; do
  echo 
  read -s -p "Enter a new Nexus admin password: " password
  echo
  if [ -n "$password" ]; then
    read -s -p " Retype new Nexus admin password: " password2
    echo
    if [ "$password2" != "$password" ]; then
      password=
      echo "Passwords must match!" 1>&2
    fi
  fi
done
echo

#update the nexus user password
a=`curl -s -H "Content-Type: application/json" --user admin:admin123 -X POST  -d "{ \"name\": \"updateuser\", \"type\": \"groovy\", \"content\": \"security.securitySystem.changePassword('admin','$password')\"}" http://localhost:8181/nexus/service/siesta/rest/v1/script/`
e=`curl -s -H "Content-Type: text/plain"       --user admin:admin123 -X POST   http://localhost:8181/nexus/service/siesta/rest/v1/script/updateuser/run`
d=`curl -s -H "Content-Type: application/json" --user admin:$password -X DELETE http://localhost:8181/nexus/service/siesta/rest/v1/script/updateuser`

#fix subsequent user scripts to use new admin password
sed -i "s/admin123/$password/g" /usr/local/sbin/addAdminUser.sh
sed -i "s/admin123/$password/g" /usr/local/sbin/addCIUser.sh
sed -i "s/admin123/$password/g" /usr/local/sbin/addUser.sh
sed -i "s/admin123/$password/g" /usr/local/sbin/resetNexusPassword.sh


echo
echo "Done. Nexus admin user password has been reset."
echo

exit 0
