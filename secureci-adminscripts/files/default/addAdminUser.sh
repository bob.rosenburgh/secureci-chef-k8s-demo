#! /bin/bash

# Must be root as root
if [ "$(/usr/bin/id -u)" != "0" ]; then
   echo "$0 must be run as root" 1>&2
   exit 1
fi

echo
echo "Creating an admin user account that can be used"
echo "to log in to the system both via web and cl."
echo

#get user information
read -er -p "Enter username for new login user: " username
password=
while [ -z "$password" ]; do
  read -s -p "Enter password for new user '$username': " password
  echo
  if [ -n "$password" ]; then
    read -s -p "Retype password for new user '$username': " password2
    echo
    if [ "$password2" != "$password" ]; then
      password=
      echo "Passwords must match!" 1>&2
    fi
  fi
done
default_pub_key=`cat /home/ubuntu/.ssh/authorized_keys`
echo
echo "Enter a public key for the new user. It should be OpenSSH format:"
echo "  ssh-rsa [[KEY]] [[user]]"
echo "Ensure there are no line breaks, and that it is NOT in OpenSSL format."
echo "Default key is public key initially setup for user Ubuntu."
read -er -p "Enter public key for new user '$username': " pub_key
pub_key=${pub_key:-$default_pub_key}

#create the new user
useradd $username -m -s /bin/bash -p $(echo $password | openssl passwd -1 -stdin)
/usr/sbin/usermod --append --groups svn,sudo $username

if [ -a /var/lib/gitblit ]; then
  #add the new user to git
  echo "[user \"$username\"]
	password = \"#externalAccount\"
	accountType = HTPASSWD
	emailMeOnMyTicketChanges = true
	role = \"#admin\"" >> /var/lib/gitblit/data/users.conf
	
  #add the jenkins ssh key to the ci user
  if [ ! -d /var/lib/gitblit/data/ssh ]; then
    mkdir -p /var/lib/gitblit/data/ssh;
  fi
  echo -e "RW $pub_key" > /var/lib/gitblit/data/ssh/$username.keys
fi

if [ -a /var/lib/nexus ]; then
  #add the user to nexus
  a=`curl -s -H "Content-Type: application/json" --user admin:admin123 -X POST  -d "{ \"name\": \"adminuser\", \"type\": \"groovy\", \"content\": \"security.addUser('$username','','','',true,'$password',['nx-admin'])\"}" http://localhost:8181/nexus/service/siesta/rest/v1/script/`
  e=`curl -s -H "Content-Type: text/plain"       --user admin:admin123 -X POST   http://localhost:8181/nexus/service/siesta/rest/v1/script/adminuser/run`
  d=`curl -s -H "Content-Type: application/json" --user admin:admin123 -X DELETE http://localhost:8181/nexus/service/siesta/rest/v1/script/adminuser`
fi

#add the new user to htpasswd
htpasswd -b /var/lib/passwd/dav_svn.passwd $username $password

#set the user to be a trac admin
trac-admin /var/lib/trac/secureci permission add $username TRAC_ADMIN

#setup the user keys
mkdir -p /home/$username/.ssh
echo "$pub_key" >> /home/$username/.ssh/authorized_keys
chmod 600 /home/$username/.ssh/authorized_keys
chown -R $username /home/$username/.ssh
chgrp -R $username /home/$username/.ssh

echo
echo "Done. user $username has been added with sudo privileges."
echo

exit 0
