#! /bin/bash

# Must be root as root
if [ "$(/usr/bin/id -u)" != "0" ]; then
   echo "$0 must be run as root" 1>&2
   exit 1
fi

SONAR_CONF=/var/lib/sonar/conf/sonar.properties
JENKINS_SONAR_CONF=/var/lib/jenkins/hudson.plugins.sonar.SonarPublisher.xml
TRAC_CONF=/var/lib/trac/secureci/conf/trac.ini

echo
echo "Setting passwords for the MySQL database."
echo

gen_passwd() {
  local pw
  pw=`/usr/bin/apg -n1 -m16 -x20 -a0 -E\'\":@/\\`;
  [ -n "$pw" ] && eval $1=\$pw
}

root_pw=
while [ -z "$root_pw" ]; do
  echo 
  read -s -p "Enter a new MySQL root password: " root_pw
  echo
  if [ -n "$root_pw" ]; then
    read -s -p " Retype new MySQL root password: " root_pw2
    echo
    if [ "$root_pw2" != "$root_pw" ]; then
      root_pw=
      echo "Passwords must match!" 1>&2
    fi
  fi
done
echo

gen_passwd sonar_pw
gen_passwd trac_pw
#gen_passwd root_pw

echo "Sonar and Trac will use random database passwords for security."
echo

echo "Stopping servers to configure new passwords..." 
echo
/etc/init.d/apache2 stop
if [ -a /etc/init.d/jenkins ]; then
	/etc/init.d/jenkins stop
fi
if [ -a /etc/init.d/sonar ]; then
	/etc/init.d/sonar stop
fi
service mysql stop

if [ -a /etc/init.d/sonar ]; then
	/bin/sed -i'.bak' -e"s/^\([ 	]*sonar.jdbc.password=[ 	]*\).*/\1$sonar_pw/" $SONAR_CONF
fi
if [ -a /etc/init.d/jenkins ]; then
	/bin/sed -i'.bak' -e"s/\(<databasePassword>\).*\(<\/databasePassword>\)/\1$sonar_pw\2/" $JENKINS_SONAR_CONF
fi
/bin/sed -i'.bak' -e"s/^\([ 	]*database = mysql:\/\/trac:\).*\(@127\.0\.0\.1\/trac.*\)/\1$trac_pw\2/" $TRAC_CONF

/usr/bin/mysqld_safe --skip-grant-tables --skip-syslog &
sleep 5

/usr/bin/mysql -S /var/run/mysql-secureci/mysqld.sock --batch -uroot mysql <<END
update user set authentication_string=PASSWORD('$sonar_pw') where user="sonar";
update user set authentication_string=PASSWORD('$trac_pw') where user="trac";
update user set authentication_string=PASSWORD('$root_pw') where user="root";
flush privileges;
quit
END

echo "Restarting servers..."
service mysql-secureci restart
if [ -a /etc/init.d/sonar ]; then 
	/etc/init.d/sonar start
fi
if [ -a /etc/init.d/jenkins ]; then 
	/etc/init.d/jenkins start
fi
/etc/init.d/apache2 start

echo
echo "Done. Random MySQL database passwords have been set."
echo

exit 0
