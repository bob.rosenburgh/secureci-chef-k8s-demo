#! /bin/bash

# Must be root as root
if [ "$(/usr/bin/id -u)" != "0" ]; then
   echo "$0 must be run as root" 1>&2
   exit 1
fi

echo
echo "Creating a new SSL certificate for the web server"
echo "so it is unique to your server."
echo

cert_dir=/etc/apache2/ssl;
ca_dir=/usr/share/ca-certificates;

#creating our new cert
/bin/mkdir -p ${cert_dir}
/usr/bin/openssl req -new -x509 -nodes -subj "/C=US/ST=Virginia/L=Fairfax/O=Coveros, Inc./CN=secureci" -out ${cert_dir}/secureci.crt -keyout ${cert_dir}/secureci.key
/etc/init.d/apache2 graceful

#adding our cert to the java keystore
keytool -delete -alias secureci -storepass changeit -noprompt -keystore /usr/lib/jvm/java-8-oracle-amd64/jre/lib/security/cacerts
keytool -import -alias secureci -storepass changeit -noprompt -keystore /usr/lib/jvm/java-8-oracle-amd64/jre/lib/security/cacerts -file ${cert_dir}/secureci.crt

#adding our cert to the CA
rm ${ca_dir}/secureci.crt
ln -s ${cert_dir}/secureci.crt ${ca_dir}/secureci.crt
update-ca-certificates -f

echo
echo "Done. The web server has been restarted using the SSL certificate"
echo "you just created."
echo

exit 0
