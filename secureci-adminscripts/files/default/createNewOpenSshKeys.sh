#! /bin/bash

# Must be root as root
if [ "$(/usr/bin/id -u)" != "0" ]; then
   echo "$0 must be run as root" 1>&2
   exit 1
fi

echo
echo "Creating new security keys for OpenSSH so"
echo "they are unique to your server."
echo

/bin/rm -f /etc/ssh/ssh_host_dsa_key /etc/ssh/ssh_host_dsa_key.pub 
/bin/rm -f /etc/ssh/ssh_host_rsa_key /etc/ssh/ssh_host_rsa_key.pub

/usr/sbin/dpkg-reconfigure openssh-server

fingerprint=`/usr/bin/ssh-keygen -l -f /etc/ssh/ssh_host_dsa_key | cut -d' ' -f2`

echo
echo "Done. The new OpenSSH server key has a fingerprint of ${fingerprint}."
echo

exit 0
