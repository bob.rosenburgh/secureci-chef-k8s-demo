#! /bin/bash

# Must be root as root
if [ "$(/usr/bin/id -u)" != "0" ]; then
  echo "$0 must be run as root" 1>&2
  exit 1
fi

if [ -a /var/lib/jenkins ]; then
  echo "Unable to install AWS credentials for Jenkins as Jenkins is not properly installed" 1>&2
  exit 1
fi

echo
echo "Setting up AWS credentials for cli usage with Jenkins"
echo

#get user information
read -er -p "Enter AWS Access Key ID: " aws_secret_access_key
read -er -p "Enter AWS Secret Access Key: " aws_access_key_id
read -er -p "Enter default region name: " region
read -er -p "Enter default output format: " output

#create the information
if [ ! -d /var/lib/jenkins/.aws ]; then
  mkdir -p /var/lib/jenkins/.aws
  chown jenkins.jenkins /var/lib/jenkins/.aws
fi

echo "[default]
aws_secret_access_key = $aws_secret_access_key
aws_access_key_id = $aws_access_key_id" > /var/lib/jenkins/.aws/credentials
chown jenkins.jenkins /var/lib/jenkins/.aws/credentials
chmod 600 /var/lib/jenkins/.aws/credentials

echo "[default]
output = $output
region = $region" > /var/lib/jenkins/.aws/config
chown jenkins.jenkins /var/lib/jenkins/.aws/config
chmod 600 /var/lib/jenkins/.aws/config

echo
echo "Done. Updated AWS credentials."
echo

exit 0