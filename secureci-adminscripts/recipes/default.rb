#
# Cookbook Name:: secureci-adminscripts
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

#add each of our shell scripts for usr/local/sbin
%w[ addUser deleteUser addAdminUser addCIUser addSvnTracUser configureAWS resetDbPasswords resetNexusPassword createNewOpenSshKeys createNewSslCertificate ].each do |file|
  cookbook_file "#{file}.sh" do
    path File.join("/usr/local/sbin","#{file}.sh")
    mode '0711'
    owner 'root'
    group 'root'
    action :create
  end
end