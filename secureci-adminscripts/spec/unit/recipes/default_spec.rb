#
# Cookbook Name:: secureci-adminscripts
# Spec:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'secureci-adminscripts::default' do
	let :chef_run do
		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
	end

  #Turn this into a loop .. testing loop below
#  it 'create addUser.sh' do 
#  	expect(chef_run).to create_file("/usr/local/sbin/addUser.sh")
#  end
#  it 'create addAdminUser.sh' do 
#  	expect(chef_run).to create_file("/usr/local/sbin/addAdminUser.sh")
#  end
#  it 'create addCIUser.sh' do 
#  	expect(chef_run).to create_file("/usr/local/sbin/addCIUser.sh")
#  end
#  it 'create addSvnTracUser.sh' do 
#  	expect(chef_run).to create_file("/usr/local/sbin/addSvnTracUser.sh")
#  end
#  it 'create configureAWS.sh' do 
#  	expect(chef_run).to create_file("/usr/local/sbin/configureAWS.sh")
#  end			
#  it 'create resetDbPasswords.sh' do 
#  	expect(chef_run).to create_file("/usr/local/sbin/resetDbPasswords.sh")
#  end	
#  it 'create resetNexusPassword.sh' do 
#  	expect(chef_run).to create_file("/usr/local/sbin/resetNexusPassword.sh")
#  end	
#  it 'create createNewOpenSshKeys.sh' do 
#  	expect(chef_run).to create_file("/usr/local/sbin/createNewOpenSshKeys.sh")
#  end	
#  it 'create createNewSslCertificate.sh' do 
#  	expect(chef_run).to create_file("/usr/local/sbin/createNewSslCertificate.sh")
#  end	
shells = %w{ addUser deleteUser addAdminUser addCIUser addSvnTracUser configureAWS resetDbPasswords resetNexusPassword createNewOpenSshKeys createNewSslCertificate }

shells.each do |shell|
	it "should create cookbook file #{shell}" do
		chef_run.converge(described_recipe)
		scripts = shell << ".sh" 
		expect(chef_run).to create_cookbook_file shell
	end
end


shells.each do |shell|
	it "should create cookbook file #{shell}" do
		scripts = shell << ".sh" 
		expect(chef_run).to create_file("/usr/local/sbin/#{shell}")
	end
end

at_exit { ChefSpec::Coverage.report! }
end