#
# Cookbook Name:: secureci-firstrun
# Spec:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'secureci-firstrun::default' do

	let :chef_run do
		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
	end

	it 'converges successfully' do
		chef_run.converge(described_recipe)
		expect { chef_run }.to_not raise_error
	end
	before(:each) do
		allow(File).to receive(:directory?).with(anything).and_call_original
		allow(File).to receive(:directory?).with('home/ubuntu').and_return true 
		allow(File).to receive(:directory?).with('home/ubuntu/.ssh').and_return true 

		allow(File).to receive(:exists?).with(anything).and_call_original
		allow(File).to receive(:exists?).with('home/ubuntu/.ssh/authorized_keys').and_return true
	end

	files = %w{ firstrun setup }

	it 'setup firstrun script' do
		expect(chef_run).to create_template('/etc/init.d/firstrun.sh').with(
			source: 'firstrun.sh.erb',
			mode:   '0755',
			owner:  'root',
			group:  'root',
			) 
	end
	files.each do |file|
		it "should create file #{file}" do
			expect(chef_run).to run_execute("touch /var/lib/#{file}")
		end
	end
	it 'should enforce firstrun execution' do
		expect(chef_run).to create_cookbook_file('/tmp/bashrc')
	end
	it 'should run bash command' do 
		expect(chef_run).to run_bash('append to .bashrc')
	end 
	at_exit { ChefSpec::Coverage.report! }
end
