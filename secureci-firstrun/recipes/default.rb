#
# Cookbook Name:: secureci-firstrun
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

template "/etc/init.d/firstrun.sh" do
  source "firstrun.sh.erb"
  mode '0755'
  owner 'root'
  group 'root'
end

#create our files for monitoring firstrun
%w[ firstrun setup ].each do |file|
  execute "create an empty files #{file}" do
    command "touch /var/lib/#{file}"
  end
end

#enforce our executing firstrun
cookbook_file "bashrc" do
  path "/tmp/bashrc"
  action :create
end
bash "append to .bashrc" do   #TODO - this can't be run twice
  user "root"
  code <<-EOH
    cat /tmp/bashrc >> /home/ubuntu/.bashrc
    rm /tmp/bashrc
  EOH
end


#delete all keypairs
execute "delete all keypairs" do
  command "truncate -s 0 home/ubuntu/.ssh/authorized_keys"
  only_if { ::File.directory?("home/ubuntu") && ::File.directory?("home/ubuntu/.ssh") && ::File.exist?("home/ubuntu/.ssh/authorized_keys") }
end

%w[ /etc/chef /var/chef ].each do |dir|
  directory dir do
    action :delete
    recursive true
  end
end
