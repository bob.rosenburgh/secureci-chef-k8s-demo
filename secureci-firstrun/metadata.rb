name 'secureci-firstrun'
maintainer 'Glenn Buckholz'
maintainer_email 'glenn.buckholz@coveros.com'
license 'All Rights Reserved'
description 'Installs/Configures secureci-firstrun'
long_description 'Installs/Configures secureci-firstrun'
chef_version     '>= 13.0' if respond_to?(:chef_version)
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
supports 'ubuntu'

version '0.1.0'
