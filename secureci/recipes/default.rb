#
# Cookbook Name:: secureci
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

# Order is important. The items with the lease dependancies are on the top.
# It is possible that if you just ran trac and jenkins you would get everything.
# This allows debugging individual components.

include_recipe "aptupdate"
include_recipe "initialuser"
include_recipe "secureci-packages"
include_recipe "secureci-adminscripts"
include_recipe "secureci-hostinfo"
include_recipe "secureci-maven"
# include_recipe "secureci-ant::default"
include_recipe "git"
# include_recipe "sonar"
#include_recipe "yasca"
include_recipe "openscap"
# include_recipe "zap"
# include_recipe "subversion::secureci"
include_recipe "docker"
include_recipe "microk8s"
# include_recipe "nexus"
# include_recipe "nexus::rest_config"
# include_recipe "secureci-jenkins"
# TODO: temp comment due to ubuntu 18.04
# include_recipe "browser::chrome"
# include_recipe "browser::xvfb"
# include_recipe "browser::firefox"
# include_recipe "trac"
include_recipe "secureci-firewall"
include_recipe "secureci-firstrun"
include_recipe "secureci-cleanup"
include_recipe "selenium-tools"
