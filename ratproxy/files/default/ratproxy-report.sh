#!/bin/bash
#
# ratproxy - report generator
# ---------------------------
#
# This is essentially a prettyprinter for ratproxy logs. It removes
# dupes, sorts entries within groups, then sorts groups base don highest
# priority within the group, and produces some nice HTML with form replay
# capabilities.
#
# Author: Michal Zalewski <lcamtuf@google.com>
# CSS / XHTML mods by Gene Gotimer <gene.gotimer@coveros.com>
#
# Copyright 2007, 2008 by Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

function escapeAmps() {
  echo "$1" | sed 's/\&/&amp;/g'
}

if [ "$1" = "" ]; then
  echo "Usage: $0 ratproxy.log" 1>&2
  exit 1
fi

if [ ! -r "$1" ]; then
  echo "Input file not found." 1>&2
  exit 1
fi

INPUTFILE=$1
NOW=`date +'%Y/%m/%d %H:%M'`

test "$RAT_URLPREFIX" = "" || RAT_URLPREFIX="/$RAT_URLPREFIX/"
test "$MESSAGES_LIST" = "" && MESSAGES_LIST=/usr/local/etc/messages.list

if [ ! -r "$MESSAGES_LIST" ]; then
  echo "Messages list file not found at $MESSAGES_LIST." 1>&2
  exit 2
fi

RATPROXY_PNG=/usr/local/etc/ratproxy-back.png
RATPROXY_CSS=/usr/local/etc/ratproxy.css
if [ -r "$RATPROXY_PNG" -a -r "$RATPROXY_CSS" -a -w . ]; then
  cp $RATPROXY_PNG $RATPROXY_CSS .
fi

# Output prologue...

cat <<_EOF_
<?xml version="1.0" encoding="iso-8859-1"?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/plain; charset=iso-8859-1"/>
<title>ratproxy - security testing proxy</title>
<link rel="stylesheet" type="text/css" href="ratproxy.css"/>
</head>

<body>

<div class="column">
<span id="logo"></span>

<h1>Ratproxy audit report</h1>
<p>
Generated on: <strong>$NOW</strong><br/>
Input file: <strong>$INPUTFILE</strong>
</p>
<p class="note">NOTE: Not all of the issues reported necessarily
correspond to actual security flaws. Findings should be validated
by manual testing and analysis where appropriate. When in doubt,
<a href="mailto:lcamtuf@google.com">contact the author</a>.</p>
<br class="clear"/>

<hr/>
<div class="key">
<strong>Report risk and risk modifier designations:</strong>
<table>
<tr>
<td>
<div class="tags">
<span class="risk low">LOW</span>
to
<span class="risk high">HIGH</span>
</div>
</td>
<td>Issue urgency classification (composite of impact and identification accuracy)</td>
</tr>
<tr>
<td>
<div class="tags">
<span class="risk info">INFO</span>
</div>
</td>
<td>Non-discriminatory entry for further analysis</td>
</tr>
<tr>
<td>
<div class="tags">
<span class="modifiers"><span class="on">ECHO</span></span>
/
<span class="modifiers">echo</span>
</div>
</td>
<td>Query parameters echoed back / not echoed in HTTP response, respectively</td>
</tr>
<tr>
<td>
<div class="tags">
<span class="modifiers"><span class="on">PRED</span></span>
/
<span class="modifiers">pred</span>
</div>
</td>
<td>Request URL or query data likely is / is not predictable to third parties, respectively</td>
</tr>
<tr>
<td>
<div class="tags">
<span class="modifiers"><span class="on">AUTH</span></span>
/
<span class="modifiers">auth</span>
</div>
</td>
<td>Request requires / does not require cookie authentication, respectively</td>
</tr>
</table>
</div>
<hr/>

<script type="text/javascript">
function toggle(id) { 
  var i = document.getElementById(id);
  if (i.style.display == 'none') i.style.display = 'inline'; else i.style.display = 'none';
  i = document.getElementById('hid_' + id);
  if (i.style.display == 'none') i.style.display = 'inline'; else i.style.display = 'none';
}
</script>

_EOF_

if [ ! -s "$INPUTFILE" ]; then
cat <<_EOF2_
<h2>No activity to report on found in log file.</h2>
</div>
</body>
</html>
_EOF2_
  exit 1
fi

PREVDESC=X
CNT=0
SCNT=0

# So this is some nearly incomprehensible logic to sort entries by priorities,
# sort groups based on highest priority within a group, and then remove any
# duplicates (paying no attention to some fields, such as trace file location),
# group "offending value" fields, and more. At some point - too late in the
# game - it became painfully obvious that this should not be a shell script ;-)

( sort -t '|' -k 1,8 -k 10,100 -ru <"$INPUTFILE" | grep '^[0123]|' | sort -t '|' -k 3,3 -s | \
  awk -F'|' '{if ($3!=PF) { npri=$1;PF=$3; }; printf "%s-%s|%s\n", npri, $3, $0}' | \
  sort -r -k 1,1 -s | sed 's/|!All /|All /'; echo "Dummy EOF" ) | \
  awk -F'|' '{

               PTRM=TRM; PFR=FR; PTA=TA; 
               FR=""; TA=""; TRM=""; GOTVAL="";

               for (a=1;a<=NF;a++) { 
                 if (a < 5) { 
                   TRM=TRM "|" $a;
                   FR=FR $a "|";
                 } else if (a > 5) {
                   TRM=TRM "|" $a;
                   TA=TA "|" $a;
                 } else GOTVAL=$a;

               }

               if (PTRM == TRM) {
                 if (GOTVAL != "-") {
                   if (LIST == "-") LIST=GOTVAL;
                   else LIST=LIST ", " GOTVAL;
                 }
               } else {
                 if (PTRM != "") print PFR LIST PTA;
                 if (GOTVAL == "-") LIST="-";
                 else LIST=GOTVAL;
               }

             }' | \
  while IFS="|" read -r skip severity modifier desc offend code len mime sniff cset trace method url cookies payload response; do

    # If issue name changed, output a new header, complete with fold / unfold controls.
    # Default groups with 'info' items only to folded state.

    if [ ! "$PREVDESC" = "$desc" ]; then

      if [ ! "X" = "$PREVDESC" ]; then
      	# end prior section
      	echo "</ul></div>"
		echo "</div>"
      fi

      SCNT=$[SCNT+1]

      echo "<div class=\"category\">"
      echo "<h2>$desc</h2>&nbsp;<span class=\"link\">[<a href=\"javascript:void(0)\" onclick=\"toggle('list$SCNT')\">toggle</a>]</span>"

      if [ ! "$severity" = "0" ]; then

        echo "<div id=\"hid_list$SCNT\" style=\"display:none\"><div class=\"hidden\">Section hidden</div></div>"
        echo "<div id=\"list$SCNT\" style=\"display:inline\">"

      else

        echo "<div id=\"hid_list$SCNT\" style=\"display:inline\"><div class=\"hidden\">Section hidden</div></div>"
        echo "<div id=\"list$SCNT\" style=\"display:none\">"

      fi

      echo "<p class=\"description\">"
      grep -F "~$desc~" $MESSAGES_LIST | cut -d'~' -f3 
      echo "</p>"
      echo "<ul>"

      PREVDESC="$desc"

    fi

    # Output severity data.

    echo -n "<li><div class=\"tags\">"

    if [ "$severity" = "3" ]; then
      echo -n "<span class=\"risk high\">HIGH</span>"
    elif [ "$severity" = "2" ]; then
      echo -n  "<span class=\"risk medium\">MEDIUM</span>"
    elif [ "$severity" = "1" ]; then
      echo -n  "<span class=\"risk low\">LOW</span>"
    else
      echo -n  "<span class=\"risk info\">INFO</span>"
    fi

    # Provide additional flags on all but 'All visited URLs' sections.

    if [ ! "$desc" = "All visited URLs" ]; then

      echo -n "<span class=\"modifiers\">"

      if [ "$[modifier & 4]" = "0" ]; then
        echo -n "echo&nbsp;"
      else
        echo -n "<span class=\"on\">ECHO</span>&nbsp;"
      fi

      if [ "$[modifier & 1]" = "0" ]; then
        echo -n "pred&nbsp;"
      else
        echo -n "<span class=\"on\">PRED</span>&nbsp;"
      fi

      if [ "$[modifier & 2]" = "0" ]; then
        echo -n "auth"
      else
        echo -n "<span class=\"on\">AUTH</span>"
      fi

      echo -n "</span>&nbsp;"

    else

      echo -n "&nbsp;"

    fi

    # Prepare trace / decompile links, if available.

    if [ "$trace" = "-" ]; then
      TLINK=""
    else

      if [ -s "$trace.flr" ]; then
        TLINK="&nbsp;<span class=\"link\">[<a href=\"$RAT_URLPREFIX$trace.flr\">decompile</a>]&nbsp;[<a href=\"$RAT_URLPREFIX$trace\">view&nbsp;trace</a>]</span>"
      else
        TLINK="&nbsp;<span class=\"link\">[<a href=\"$RAT_URLPREFIX$trace\">view&nbsp;trace</a>]</span>"
      fi

    fi

    # Output URL, query, and response data.
    eUrl=$(escapeAmps "$url")

    test "$method" = "-" && method="[Referer]"

    if [ "$payload" = "-" ]; then

      echo "<span class=\"$method\">$method</span>&nbsp;<a href=\"$eUrl\">$eUrl</a>&nbsp;&rArr;&nbsp;$code$TLINK</div>"

      if [ ! "$response" = "-" ]; then
        echo "<div class=\"response\">Response&nbsp;($len):&nbsp;<span class=\"value\">$response</span></div>"
      fi

      if [ ! "$cookies" = "-" ]; then
        echo "<div class=\"cookies\">Cookies&nbsp;set:&nbsp;<span class=\"value\">$cookies</span></div>"
      fi

      if [ ! "$offend" = "-" ]; then
        echo "<div class=\"offending\">Offending&nbsp;value:&nbsp;<span class=\"value\">$offend</span></div>"
      fi

      if [ "$method" = "[Referer]" ]; then
        echo "<div class=\"encoding\">Target&nbsp;resource:&nbsp;<a href=\"$sniff\">$sniff</a></div>"
      else
        echo "<div class=\"encoding\">MIME type: <span class=\"value\">$mime</span>, detected: <span class=\"value\">$sniff</span>, charset: <span class=\"value\">$cset</span></div>"
      fi

    else

      isfile=""

      if echo "$payload" | grep -qF "=FILE["; then
        isfile="(FILE)&nbsp;"
      fi
      ePayload=$(escapeAmps "$payload")

      echo "<span class=\"$method\">$isfile$method</span>&nbsp;<a href=\"javascript:void(0)\" onclick=\"document.getElementById('form$CNT').submit();return false;\">$eUrl</a>&nbsp;&rArr;&nbsp;$code$TLINK</div>"
      echo "<div class=\"payload\">Payload:&nbsp;<span class=\"value\">$ePayload</span></div>"

      if [ ! "$response" = "-" ]; then
        echo "<div class=\"response\">Response&nbsp;($len):&nbsp;<span class=\"value\">$response</span></div>"
      fi

      if [ ! "$cookies" = "-" ]; then
        echo "<div class=\"cookies\">Cookies&nbsp;set:&nbsp;<span class=\"value\">$cookies</span></div>"
      fi

      if [ ! "$offend" = "-" ]; then
        echo "<div class=\"offending\">Offending&nbsp;value:&nbsp;<span class=\"value\">$offend</span></div>"
      fi

      echo "<div class=\"encoding\">MIME type: <span class=\"value\">$mime</span>, detected: <span class=\"value\">$sniff</span>, charset: <span class=\"value\">$cset</span></div>"

      if ! echo "$payload" | grep -q '^GWT_RPC\['; then
  
        echo "<input type=\"submit\" value=\"edit values\" onclick=\"document.getElementById('form$CNT').style.display='inline';return false;\"/>"
        echo "<form action=\"$eUrl\" method=\"post\" id=\"form$CNT\" style=\"display: none\">"
        echo "<div>"
        echo "$payload" | sed 's/\&#x\(..\);/%\1/g' | sed 's/&/\
/g' | sed 's/%26/\&/g;s/%3B/;/g' | sed 's/\%\(..\)/\&#x\1;/g' | \
        while IFS='=' read -r param val; do 
          echo "<input type=\"text\" name=\"$param\" value=\"$val\"/>"
        done
        echo "</div>"
        echo "</form>"

      fi

    fi

    echo "</li>"

    CNT=$[CNT+1]

  done

echo "</ul></div>"
echo "</div>"
echo "</div></body></html>"
