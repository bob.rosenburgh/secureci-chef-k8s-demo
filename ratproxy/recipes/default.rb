#
# Cookbook Name:: ratproxy
# Recipe:: default
#
# Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe "openssl"

user node['ratproxy']['user'] do
  system true
  shell "/bin/false"
  action :create
end

directory node['ratproxy']['install_dir'] do
  owner node['ratproxy']['user']
  group node['ratproxy']['group']
  recursive true
  mode 00755
end

remote_file "#{node['ratproxy']['install_dir']}/ratproxy-#{node['ratproxy']['version']}.tar.gz" do
  source node['ratproxy']['url']
  owner node['ratproxy']['user']
  group node['ratproxy']['group']
  mode 0755
  action :create_if_missing
  not_if { ::File.directory?("#{node['ratproxy']['install_dir']}/ratproxy-#{node['ratproxy']['version']}") }
end
execute "untar ratproxy-#{node['ratproxy']['version']}" do
  cwd node['ratproxy']['install_dir']
  command "tar zxvf ratproxy-#{node['ratproxy']['version']}.tar.gz"
  user "root"
  action :run
  not_if { ::File.directory?("#{node['ratproxy']['install_dir']}/ratproxy-#{node['ratproxy']['version']}") }
end
execute "versioning ratproxy" do
  cwd node['ratproxy']['install_dir']
  command "mv ratproxy ratproxy-#{node['ratproxy']['version']}"
  user "root"
  action :run
end
execute "remove ratproxy-#{node['ratproxy']['version']}.tar.gz" do
  cwd node['ratproxy']['install_dir']
  command "rm -rf ratproxy-#{node['ratproxy']['version']}.tar.gz"
  user "root"
  action :run
end

#make soft link from sonar version to sonar
link node['ratproxy']['base_dir'] do
  action :delete
  only_if "test -L #{node['ratproxy']['base_dir']}"
end
link node['ratproxy']['base_dir'] do
  to "#{node['ratproxy']['install_dir']}/ratproxy-#{node['ratproxy']['version']}"
  action :create
end

#fix our make file
cookbook_file "mime.c" do
  path File.join(node['ratproxy']['base_dir'],'mime.c')
  action :create
end

#fix our permissions
execute "chown #{node['ratproxy']['base_dir']}" do
  cwd node['ratproxy']['install_dir']
  command "chown -R #{node['ratproxy']['user']}:#{node['ratproxy']['group']} #{node['ratproxy']['base_dir']}"
  user "root"
  action :run
end
execute "chmod +r #{node['ratproxy']['base_dir']}" do
  cwd node['ratproxy']['install_dir']
  command "chmod -R +r #{node['ratproxy']['base_dir']}"
  user "root"
  action :run
end
execute "chmod +x #{node['ratproxy']['base_dir']}" do
  cwd node['ratproxy']['install_dir']
  command "chmod +r #{node['ratproxy']['base_dir']} #{node['ratproxy']['base_dir']}/flare-dist #{node['ratproxy']['base_dir']}/doc"
  user "root"
  action :run
end

#build ratproxy
execute "build ratproxy" do
  cwd node['ratproxy']['base_dir']
  command "make"
  user "root"
  action :run
end

#copy our flare and ratproxy files
%w[ ratproxy flare ].each do |file|
  remote_file "Copy #{file} file" do 
    path "#{node['ratproxy']['bin_dir']}/#{file}"
    source "file:///#{node['ratproxy']['base_dir']}/#{file}"
    owner node['ratproxy']['user']
    group node['ratproxy']['group']
    mode 0755
    not_if { ::File.exists?("#{node['ratproxy']['base_dir']}/#{file}")}
  end
end

#copy over our modified files
cookbook_file "ratproxy-report.sh" do
  path File.join(node['ratproxy']['bin_dir'],'ratproxy-report.sh')
  action :create
  mode 0755
end
cookbook_file "keyfile.pem" do
  path File.join(node['ratproxy']['bin_dir'],'keyfile.pem')
  action :create
  mode 0644
end
cookbook_file "ratproxy.css" do
  path File.join(node['ratproxy']['etc_dir'],'ratproxy.css')
  action :create
  mode 0644
end
cookbook_file "ratproxy-back.png" do
  path File.join(node['ratproxy']['etc_dir'],'ratproxy-back.png')
  action :create
  mode 0644
end
cookbook_file "messages.list" do
  path File.join(node['ratproxy']['etc_dir'],'messages.list')
  action :create
  mode 0644
end
