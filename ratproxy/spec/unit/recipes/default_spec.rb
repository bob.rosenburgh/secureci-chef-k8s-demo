require 'spec_helper'

describe 'ratproxy::default' do

 before do
    stub_command('/usr/sbin/apache2 -t').and_return('Syntax OK')
    stub_command("test -L /usr/src/ratproxy").and_return('Syntax OK')
    stub_command('/usr/sbin/httpd -t').and_return('Syntax OK')
    stub_command("test -L /usr/share/ca-certificates/secureci.crt").and_return('Syntax OK')
  end

let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end

let :rprox_user do 
     chef_run.node['ratproxy']['user']
  end
let :rprox_group do
     chef_run.node['ratproxy']['group']
  end
let :rprox_base do 
     chef_run.node['ratproxy']['base_dir']
  end
let :rprox_version do 
     chef_run.node['ratproxy']['version']
  end
let :rprox_url do 
     chef_run.node['ratproxy']['url']
  end
let :rprox_home do 
     chef_run.node['ratproxy']['home']
  end
let :rprox_install do 
     chef_run.node['ratproxy']['install_dir']
  end
files = %w{ ratproxy flare }

it 'includes cookbook openssl' do
    expect(chef_run).to include_recipe('openssl::secureci')
  end
it 'creates ratproxy user' do  
    expect(chef_run).to create_user rprox_user
  end 
it 'should create an install dir' do 
    expect(chef_run).to create_directory rprox_install
end
it 'should create a remote file' do 
   expect(chef_run).to create_remote_file_if_missing("#{rprox_install}/ratproxy-#{rprox_version}.tar.gz").with(
   source:  rprox_url,
   owner:   rprox_user,
   group:   rprox_group,
   )
end 
files.each do |file|
it "should copy #{file}" do
    expect(chef_run).to create_remote_file("Copy #{file} file").with(
    path:   "#{chef_run.node['ratproxy']['bin_dir']}/#{file}",
    source: "file:///#{chef_run.node['ratproxy']['base_dir']}/#{file}",
    owner:  rprox_user,
    group:  rprox_group
    ) 
  end
 end 
it 'untar ratproxy tar' do
    expect(chef_run).to run_execute("tar zxvf ratproxy-#{rprox_version}.tar.gz")
  end
it 'versions ratproxy ' do
    expect(chef_run).to run_execute("mv ratproxy ratproxy-#{rprox_version}")
  end
it 'removes ratproxy' do
    expect(chef_run).to run_execute("rm -rf ratproxy-#{rprox_version}.tar.gz")
  end
it 'runs chown on ratproxy' do
    expect(chef_run).to run_execute("chown -R #{rprox_user}:#{rprox_group} #{rprox_base}")
  end 
it 'deletes soft link' do
    expect(chef_run).to delete_link(rprox_base).with(link_type: :symbolic)
  end
it 'creates soft link' do
    link = chef_run.link(rprox_base)
    expect(link).to link_to("#{rprox_install}/ratproxy-#{rprox_version}")
  end
it 'creates a cookbook file' do
    expect(chef_run).to create_cookbook_file('mime.c')
  end
it 'runs chmod' do
    expect(chef_run).to run_execute("chmod +r #{rprox_base}")
  end
it 'runs chmod' do
    expect(chef_run).to run_execute("chmod +r #{rprox_base} #{rprox_base}/flare-dist #{rprox_base}/doc")
 end
 it 'build ratproxy' do
    expect(chef_run).to run_execute("make")
 end
it 'creates a cookbook file' do
    expect(chef_run).to create_cookbook_file('mime.c')
  end
it 'creates a cookbook file' do
    expect(chef_run).to create_cookbook_file('ratproxy-report.sh')
  end
it 'creates a cookbook file' do
    expect(chef_run).to create_cookbook_file('keyfile.pem')
  end
it 'creates a cookbook file' do
    expect(chef_run).to create_cookbook_file('ratproxy.css')
  end
it 'creates a cookbook file' do
    expect(chef_run).to create_cookbook_file('messages.list')
  end
it 'creates a cookbook file' do
    expect(chef_run).to create_cookbook_file('ratproxy-back.png')
  end
at_exit { ChefSpec::Coverage.report! }
end
