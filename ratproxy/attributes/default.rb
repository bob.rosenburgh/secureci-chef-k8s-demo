default['ratproxy']['user'] = 'ratproxy'
default['ratproxy']['group'] = 'ratproxy'

default['ratproxy']['install_dir'] = '/usr/src'
default['ratproxy']['base_dir'] = "#{node['ratproxy']['install_dir']}/ratproxy"
default['ratproxy']['bin_dir'] = '/usr/local/bin'
default['ratproxy']['etc_dir'] = '/usr/local/etc'

default['ratproxy']['version'] = '1.58'
default['ratproxy']['url'] = "https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/ratproxy/ratproxy-#{node['ratproxy']['version']}.tar.gz"
