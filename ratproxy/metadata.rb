name             "ratproxy"
maintainer       "Max Saperstone"
maintainer_email "max.saperstone@coveros.com"
license          "All Rights Reserved"
description      "Install ratproxy into our application"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "install ratproxy"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.0.7"

supports 'ubuntu'

depends 'openssl'

# 0.0.7 resolved dependency conflict + updated  unit tests
# 0.0.6 added tests
# 0.0.5 removed jenkins dependency
# 0.0.4 changed user to jenkins user
# 0.0.3 added all custom files
# 0.0.2 added dependencie on openssl
# 0.0.1 initial recipe
