require 'spec_helper'

def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

ratproxy_user        = node['default']['ratproxy']['user']
ratproxy_bin         = node['default']['ratproxy']['bin_dir']
ratproxy_etc         = node['default']['ratproxy']['etc_dir']
ratproxy_version     = node['default']['ratproxy']['version'] 
ratproxy_install     = node['default']['ratproxy']['install_dir'] 


describe 'ratproxy' do 

describe user(ratproxy_user) do
    it { should exist }
end
describe command("ratproxy --v") do
    its(:stdout) { should contain(ratproxy_version) }
    its(:exit_status) { should eq 0 }
end
describe file(ratproxy_base) do
  it { should be_linked_to "#{ratproxy_install}/ratproxy-#{ratproxy_version}" }
end
describe file("#{ratproxy_bin}/ratproxy-report.sh") do
  it { should exist }
  end
describe file("#{ratproxy_bin}/keyfile.pem") do
  it { should be_file }
  end
describe file("#{ratproxy_etc}/ratproxy.css") do
  it { should be_file }
  end
describe file("#{ratproxy_etc}/ratproxy-back.png") do
  it { should be_file }
  end
 describe file("#{ratproxy_etc}/messages.list") do
  it { should be_file }
  end
end
