name             'java_oracle'
maintainer       'Max Saperstone'
maintainer_email 'Max.Saperstone@coveros.com'
license          'All rights reserved'
description      'Installs/Configures java'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          '2.0.3'

supports 'ubuntu'

# community dependencies
depends 'java'
depends 'apt'
depends 'openssl'

# 2.0.3 - food critic fixes
# 2.0.2 - upgrading to java 8
# 2.0.1 - adding in java home variables for gitblit install to use
