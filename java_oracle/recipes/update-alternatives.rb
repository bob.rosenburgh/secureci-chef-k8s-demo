java_alternatives "set java alternatives" do
    java_location node['java']['oracle']['home']
    bin_cmds ["java", "javac", "jar", "javadoc", "javah", "javap", "javaw"]
    action :set
    priority 1072
    default true
end

