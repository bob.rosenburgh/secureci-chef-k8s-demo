#
# Cookbook Name:: java_oracle
# Recipe:: default
#
# Copyright (C) 2015 Max Saperstone
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'apt'
include_recipe 'java'
include_recipe 'openssl'

#generate our certificate, and add it to our java keystore
execute "import secureci crt" do
  command <<-EOF
    keytool -import -alias #{node['secureci']['hostname']} -file #{node['openssl']['crt']} -storepass changeit -noprompt -keystore #{node['java']['home']}/jre/lib/security/cacerts
    EOF
  not_if { ::File.exists?(node['openssl']['crt'])}
end
