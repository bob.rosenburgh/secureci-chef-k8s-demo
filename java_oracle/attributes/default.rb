# default jdk attributes
default['java']['jdk_version'] = '8'
default['java']['jdk_full_version'] = '1.8.0_101'    #this needs to be updated the java_agileorbit is updated
override['java']['openjdk_packages'] = [
  "openjdk-8-jdk", "openjdk-8-jre-headless"
  ]
default['java']['accept_license_agreement'] = true
default['java']['install_flavor'] = "oracle"
default['java']['oracle']['accept_oracle_download_terms'] = true

default['java']['oracle']['home'] = "/usr/lib/jvm/java-#{node['java']['jdk_version']}-oracle-amd64"
default['java']['home'] = node['java'][node['java']['install_flavor']]['home']


default['java']['jdk']['8']['x86_64']['url'] = 'https://s3.amazonaws.com/securecibuild/tool.tar.gz'
default['java']['jdk']['8']['x86_64']['checksum'] = 'b06c3bba845ccdd6222c911f0da7375b39bf9b10cca1f7ee36383c7503a99a00'

