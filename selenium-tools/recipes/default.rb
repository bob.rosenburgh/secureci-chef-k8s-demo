#
# Cookbook:: selenium-tools
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

include_recipe "ark"

ark 'chromedriver' do
  url 'https://chromedriver.storage.googleapis.com/2.35/chromedriver_linux64.zip'
  path '/usr/local/bin'
  creates 'chromedriver'
  mode 755
  action :dump
end

ark 'geckodriver' do
  url 'https://github.com/mozilla/geckodriver/releases/download/v0.19.1/geckodriver-v0.19.1-linux64.tar.gz'
  path '/usr/local/bin'
  mode 755
  action :dump
end
