default['zap']['user'] = 'zap'
default['zap']['group'] = 'www-data'

default['zap']['version'] = '2.5.0'
default['zap']['url'] = "https://github.com/zaproxy/zaproxy/releases/download/#{node['zap']['version']}/ZAP_#{node['zap']['version']}_Linux.tar.gz"

default['zap']['base'] = '/opt'
default['zap']['home'] = "#{node['zap']['base']}/zap-#{node['zap']['version']}"
default['zap']['output'] = "#{node['zap']['home']}/ZAPScanResults"
default['zap']['home_link'] = "/var/lib/zap"
### Required directory for usage of ZAP within Jenkins
default['JenkinsZap']['home'] = '/home/tomcat7'
default['JenkinsZap']['zap'] = "#{node['JenkinsZap']['home']}/.ZAP"
