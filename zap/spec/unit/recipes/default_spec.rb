require 'spec_helper'
describe 'zap::default' do

    let :chef_run do
        ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
    end
    it 'converges successfully' do
        chef_run.converge(described_recipe)
        expect { chef_run }.to_not raise_error
    end
    it 'should create zap user' do 
        expect(chef_run).to create_user(chef_run.node['zap']['user'])
    end 
    it 'should run ark' do
        expect(chef_run).to install_ark('zap')
    end
    it 'should change ownership of "/opt"' do
        expect(chef_run).to_not create_directory(chef_run.node['zap']['base']).with(
            owner: 'zap',
            group: 'www-data',
            mode:  '00755')
    end
    it 'should change ownership of "ZapScanResults"' do
        expect(chef_run).to_not create_directory(chef_run.node['zap']['output']).with(
            owner: 'zap',
            group: 'www-data',
            mode:  '0777')
    end
    at_exit { ChefSpec::Coverage.report! }
end
