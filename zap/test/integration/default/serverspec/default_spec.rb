require 'spec_helper'

describe 'Zap' do
  let(:node) { JSON.parse(IO.read('/tmp/kitchen/chef_node.json')) }

  describe user("zap") do
    it { should exist }
  end
  describe group("www-data") do
    it { should exist }
  end
  describe command("cut -d: -f1 /etc/passwd") do
    its(:stdout) { should contain(node['default']['zap']['user']) }
    its(:exit_status) { should eq 0 }
  end
  describe file("/opt") do
    it { should be_owned_by node['default']['zap']['user'] }
    it { should be_grouped_into node['default']['zap']['group'] }
  end
 end
