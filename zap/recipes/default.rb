#
# Cookbook Name:: zap
# Recipe:: default
# Author:: Ben Pick
#
# Copyright 2015, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe "java_tools"
include_recipe "java_oracle"
include_recipe "ark"

user node['zap']['user'] do
  system true
  shell "/bin/false"
  action :create
end

directory node['zap']['base'] do
  owner node['zap']['user']
  group node['zap']['group']
  recursive true
  mode 00755
end

#download and unzip the zap file from sourceforge.net
 ark 'zap' do
   url node['zap']['url']
   extension "tar.gz"
   version node['zap']['version']
   owner node['zap']['user']
   group node['zap']['group']
   mode 0755
   path node['zap']['home']
   home_dir node['zap']['home_link']
   prefix_root node['zap']['base'] 
   prefix_home node['zap']['base']
   action :install 
 end

directory node['zap']['output'] do
  owner node['zap']['user']
  group node['zap']['group']
  mode 0777
  only_if do
    File.directory?("/var/lib/jenkins")
  end
end 
