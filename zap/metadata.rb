name             'zap'
maintainer       "Ben Pick"
maintainer_email "ben.pick@coveros.com"
license          "All Rights Reserved"
description      "Install security testing tool, zap, into our application"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "install zap"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.1.5"

supports 'ubuntu'
# 0.1.5 - updating unit test + updating dependencies
# 0.1.4 - food critic fixes
# 0.1.3 - updating ark installion
# 0.1.2 - upgrading ZAP to 2.5.0
# 0.1.1 - upgrading ZAP to 2.4.2 and changing url to github
# 0.0.7 - upgrading ZAP to 2.4.1
# 0.0.6 - made jenkins user dependency more dynamic
# 0.0.5 - updated for more jenkins integration
# 0.0.4 - updated for jenkins integration
# 0.0.3 - revised installation with ark. 
# 0.0.2 - changing dependencies to berkself
# 0.0.1 - initial setup

depends 'ark'
depends 'secureci-jenkins'
depends 'java_oracle'
depends 'java_tools'
