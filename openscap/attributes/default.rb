default['openscap']['user'] = 'openscap'
default['openscap']['group'] = 'www-data'

default['openscap']['version'] = '1.2.11'
default['openscap']['tarurl'] = "https://src.fedoraproject.org/repo/pkgs/openscap/openscap-1.2.11.tar.gz/4cac5357617aa94fa697a8ddaedf8860/openscap-#{node['openscap']['version']}.tar.gz"

default['openscap']['base'] = '/opt'
default['openscap']['home'] = "#{node['openscap']['base']}/openscap-#{default['openscap']['version']}"
default['openscap']['scantemplates'] = "#{node['openscap']['home']}/ScanTemplates"
default['openscap']['output'] = "#{node['openscap']['home']}/OpenSCAPScanResults"
default['openscap']['home_link'] = "/var/lib/openscap"
default['openscap']['installation'] = "/usr/local/openscap-#{default['openscap']['version']}"

# Prerequisite library installations
default['installations']['openscap_all'] = [ "libacl1-dev", "libcap-dev", "libcurl4-gnutls-dev", "libgcrypt11-dev", "libselinux1-dev", "libxml2-dev", "libxslt1-dev", "libpcre3-dev", "libpcre++-dev", "libxml-parser-perl", "libxml-xpath-perl", "libldap2-dev", "make", "rpm", "swig", "python-dev", "python3-dev", "python-openscap" ]
default['installations']['openscap'] = 'libopenscap8'

default['openscap']['scantemplates_xml'] = [ "centos.linux.6", "debian.gnu.linux.7.1", "generic_unix_large", "openscap-cpe-oval", "oval-ubuntuOS_14.04", "oval-ubuntuOS_14.10", "scap-rhel6-oval", "ssg-rhel6-oval", "ubuntu.13.04_patchmanagement", "ubuntu-cpe", "ubuntu-cpe-oval", "ubuntu-xccdf", "usgcb-rhel5desktop-cpe-dictionary", "usgcb-rhel5desktop-cpe-oval", "usgcb-rhel5desktop-oval", "usgcb-rhel5desktop-xccdf" ]
