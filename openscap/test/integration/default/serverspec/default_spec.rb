require 'spec_helper'

def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

openscap_user        = node['default']['openscap']['user']
openscap_group       = node['default']['openscap']['group']
openscap_home        = node['default']['openscap']['home']
openscap_version     = node['default']['openscap']['version'] 
openscap_work        = node['default']['openscap']['work']
openscap_url         = node['default']['openscap']['tarurl']

describe 'openscap' do 

describe user(openscap_user) do
    it { should exist }
end
describe command("oscap --v") do
    its(:stdout) { should contain(openscap_version) }
    its(:exit_status) { should eq 0 }
end
describe file("#{openscap_home}/ScanTemplates") do
  it { should be_directory }
  it { should be_owned_by openscap_user }
  it { should be_grouped_into openscap_group }
  end
describe file("#{openscap_home}/OpenSCAPScanResults") do
  it { should be_directory }
  it { should be_owned_by openscap_user }
  it { should be_grouped_into openscap_group }
  end
end
