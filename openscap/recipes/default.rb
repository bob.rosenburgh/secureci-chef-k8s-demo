#
# Cookbook Name:: openscap
# Recipe:: default
# Author:: Ben Pick
#
# Copyright 2015, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
include_recipe "apt"
include_recipe "secureci-ant"
include_recipe "ark"

user node['openscap']['user'] do
  system true
  shell "/bin/false"
  action :create
end

directory node['openscap']['base'] do
  owner node['openscap']['user']
  group node['openscap']['group']
  recursive true
  mode 00755
end

for prepackage in node['installations']['openscap_all'] do
	package prepackage do
	  action :install
	end
end

package "libbz2-dev" do 
  action :install
end

#download and unzip the openscap file from open-scap.org
 ark 'openscap' do
   url node['openscap']['tarurl']
   extension "tar.gz"
   version node['openscap']['version']
   owner node['openscap']['user']
   group node['openscap']['group']
   mode 0755
   path node['openscap']['home']
   home_dir node['openscap']['home_link']
   prefix_root node['openscap']['base'] 
   prefix_home node['openscap']['base']
   action [ :install, :configure, :install_with_make ]
#   action [ :configure, :install_with_make ]
 end

execute "configure openscap" do
  cwd node['openscap']['home']
  command "sudo ./configure"
  user "root"
  action :run
end
execute "configure openscap make script" do
  cwd node['openscap']['home']
  command "sudo make"
  user "root"
  action :run
end
execute "configure openscap make install" do
  cwd node['openscap']['home']
  command "sudo make install"
  action :run
  user "root"
end

execute "load oscap shared libs" do
  command "sudo ldconfig"
  action :run
  user "root"
end

# Installing 2 forms of openscap is intentional. 
# Later configure and installation commands will fail if the below installation is not performed. 
# Note that if this step is performed first, the oscap --v returns the correct year
# However, errors occur:
# symbol lookup error: oscap: undefined symbol: oscap_source_new_from_file
package node['installations']['openscap'] do
  action :install
end

directory node['openscap']['scantemplates'] do
  owner node['openscap']['user']
  group node['openscap']['group']
  mode 0755
  action :create
end

# Place policy files to use as scanning templates
for scantemplates_xml in node['openscap']['scantemplates_xml'] do 
	scantemplates_xml = scantemplates_xml+".xml"
	cookbook_file scantemplates_xml do
    	path "#{node['openscap']['scantemplates']}/#{scantemplates_xml}"
    	action :create_if_missing
    	mode 0644
    end
end

directory node['openscap']['output'] do
  owner node['openscap']['user']
  group node['openscap']['group']
  mode 0755
  action :create
end
