name             'openscap'
maintainer       "Ben Pick"
maintainer_email "ben.pick@coveros.com"
license          "All Rights Reserved"
description      "Install policy analysis tool, openscap, into our application"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "install openscap"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.0.12"

supports 'ubuntu'
# 0.0.12 - updating unit tests / removing old  spec matchers
# 0.0.11 - food critic fixes 
# 0.0.10 - updating ark installation 
# 0.0.9 - set dependency to use secureci-ant
# 0.0.8 - upgraded version to 1.2.11
# 0.0.7 - removed jenkins dependency
# 0.0.6 - upgraded version to 1.2.6
# 0.0.5 - made jenkins user dependency more dynamic
# 0.0.4 - post install customizations
# 0.0.3 - additional dependencies
# 0.0.2 - changing dependencies to berkself
# 0.0.1 - initial setup

depends 'apt'
depends 'secureci-ant'
depends 'ark'
depends 'poise-python'
