require 'spec_helper'

describe 'openscap::default' do

#before do
#    stub_command('/usr/sbin/apache2 -t').and_return('Syntax OK')
#    stub_command("test -L /opt/nexus").and_return('Syntax OK')
#    stub_command("test -L /etc/init.d/nexus").and_return(true)
#    stub_command("test -L /var/lib/nexus").and_return('Syntax OK')
#    stub_command('/usr/sbin/httpd -t').and_return('Syntax OK')
#  end

let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end

let :oscap_base do 
     chef_run.node['openscap']['base']
  end
let :openscap_user do 
     chef_run.node['openscap']['user']
  end
let :oscap_install_all do 
     chef_run.node['installations']['openscap_all']
  end
let :oscap_install do 
     chef_run.node['installations']['openscap']
  end
packages = %w{libacl1-dev libcap-dev libcurl4-gnutls-dev libgcrypt11-dev libselinux1-dev libxml2-dev libxslt1-dev libpcre3-dev libpcre++-dev libxml-parser-perl libxml-xpath-perl libldap2-dev make rpm swig libbz2-dev libopenscap8
}
scans = %w{centos.linux.6 debian.gnu.linux.7.1 generic_unix_large openscap-cpe-oval oval-ubuntuOS_14.04 oval-ubuntuOS_14.10 scap-rhel6-oval ssg-rhel6-oval ubuntu.13.04_patchmanagement ubuntu-cpe ubuntu-cpe-oval ubuntu-xccdf usgcb-rhel5desktop-cpe-dictionary usgcb-rhel5desktop-cpe-oval usgcb-rhel5desktop-oval usgcb-rhel5desktop-xccdf
}

it 'includes cookbook secureci-ant' do
    expect(chef_run).to include_recipe('secureci-ant')
  end
it 'includes cookbook ark' do
    expect(chef_run).to include_recipe('ark')
  end
it 'includes cookbook python' do
    expect(chef_run).to include_recipe('python')
  end
it 'should create nexus user' do
    expect(chef_run).to create_user openscap_user
  end
it 'should set the base dir user/group' do
    expect(chef_run).to create_directory oscap_base
  end
packages.each do |package|
it 'should install all packages' do
    expect(chef_run).to install_package(package)
  end
 end
it "should install PACKAGE via ark" do
    expect(chef_run).to install_ark "openscap"
    expect(chef_run).to install_with_make_ark "openscap"
    expect(chef_run).to configure_ark "openscap"
  end
it 'run a command' do
    expect(chef_run).to run_execute("sudo ./configure")
  end
it 'runs a command' do
    expect(chef_run).to run_execute("sudo make")
  end
it 'runs a command' do
    expect(chef_run).to run_execute("sudo make install")
  end
it 'should install all packages' do
    expect(chef_run).to install_package(oscap_install)
  end 
it 'should create new dir' do 
    expect(chef_run).to create_directory chef_run.node['openscap']['scantemplates']
  end
scans.each do |scan|
scans_xml = scan << ".xml"
it "should create cookbook file #{scans_xml}" do
    expect(chef_run).to create_cookbook_file_if_missing scans_xml
  end
 end
it 'should create new dir' do 
    expect(chef_run).to create_directory chef_run.node['openscap']['output']
  end
at_exit { ChefSpec::Coverage.report! }
end

