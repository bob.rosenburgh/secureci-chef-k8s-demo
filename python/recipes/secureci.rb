#
# Author:: Max Saperstone <max.saperstone@coveros.com>
# Cookbook Name:: python
# Recipe:: default
#
# Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

package "python-pip" do
  action :purge
end

include_recipe "python::default"

package "python-setuptools" do
  action :install
end

package "python-mysqldb" do
  action :install
end

package "python-subversion" do
  action :install
end

package "python-babel" do
  action :install
end

package "libapache2-mod-python" do
  action :install
end
package "python-pip" do
  action :install
end

package "libmysqlclient-dev" do
  action :install
end
execute "mysql-python" do
  command "pip install mysql-python"
end