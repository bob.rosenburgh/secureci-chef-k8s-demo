name              "python"
description       "Installs Python, pip and virtualenv. Includes LWRPs for managing Python packages with `pip` and `virtualenv` isolated Python environments."
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
maintainer        "Noah Kantrowitz"
maintainer_email  "noah@coderanger.net"
license           "All Rights Reserved"
recipe            "default", "package source pip virtualenv"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"  
chef_version     '>= 13.0' if respond_to?(:chef_version)

version           "1.5.0"

supports 'ubuntu'

depends           'build-essential'
depends           'yum-epel'
depends           'apt'

#1.5.0 - Adding installation of python-mysql
#1.4.9 - upgrading version to 2.7.12
