require 'spec_helper'

def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

describe 'python' do 

version = node['default']['python']['version']

describe command('python -V') do
  its(:stdout) { should contain("#{version}") }
  its('exit_status') { should eq 0 }
 end
describe command('sudo apt-get install python-setuptools') do
  its(:stdout) { should contain("python-setuptools is already the newest version")}
  its('exit_status') { should eq 0 }
end
describe command('sudo apt-get install python-mysqldb') do
  its(:stdout) { should contain("python-mysqldb is already the newest version")}
  its('exit_status') { should eq 0 }
end
describe command('sudo apt-get install python-subversion') do
  its(:stdout) { should contain("python-subversion is already the newest version")}
  its('exit_status') { should eq 0 }
end
describe command('sudo apt-get install python-babel') do
  its(:stdout) { should contain("python-babel is already the newest version")}
  its('exit_status') { should eq 0 }
end
describe command('sudo apt-get install libapache2-mod-python') do
  its(:stdout) { should contain("libapache2-mod-python is already the newest version")}
  its('exit_status') { should eq 0 }
end
describe command('sudo apt-get install python-pip') do
  its(:stdout) { should contain("python-pip is already the newest version")}
  its('exit_status') { should eq 0 }
end
end
