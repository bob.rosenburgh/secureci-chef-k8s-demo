require 'spec_helper'

describe 'python::secureci' do

let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '14.04')
 end

it 'installs python-setuptools' do
    chef_run.converge(described_recipe)
    expect(chef_run).to install_package('python-setuptools')
 end
it 'installs python-mysqldb' do
    chef_run.converge(described_recipe)
    expect(chef_run).to install_package('python-mysqldb')
 end
it 'installs python-subversion' do
    chef_run.converge(described_recipe)
    expect(chef_run).to install_package('python-subversion')
 end
it 'installs python-babel' do
    chef_run.converge(described_recipe)
    expect(chef_run).to install_package('python-babel')
 end
it 'installs libapache2-mod-python' do
    chef_run.converge(described_recipe)
    expect(chef_run).to install_package('libapache2-mod-python')
 end
it 'installs python-pip' do
    chef_run.converge(described_recipe)
    expect(chef_run).to install_package('python-pip')
 end
at_exit { ChefSpec::Coverage.report! }
end
