# Spec:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

require 'spec_helper'


describe 'openssl::default' do

  let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
  end

  it 'installs openssl' do
    expect(chef_run).to install_package('openssl')
  end
  it 'installs libssl-dev' do
    expect(chef_run).to install_package('libssl-dev')
  end


  it 'executes "generate crt"' do 
    expect(chef_run).to_not run_execute("generate crt")
  end
  it 'should create a link' do 
    link = chef_run.link("/usr/share/ca-certificates/#{chef_run.node['secureci']['hostname']}.crt")
    expect(link).to link_to(chef_run.node['openssl']['crt'])
  end
  it 'should execute a command' do 
    expect(chef_run).to run_execute('add crt to ca-certs')
  end

  let :openssl_user do 
    chef_run.node['openssl']['user']
  end
  let :openssl_group do
    chef_run.node['openssl']['group']
  end

  at_exit { ChefSpec::Coverage.report! }
end
