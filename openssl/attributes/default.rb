default['openssl']['user'] = 'root'
default['openssl']['group'] = 'root'

default['openssl']['home'] = "/usr/share/openssl"

default['openssl']['key'] = "#{node['openssl']['home']}/secureci.key"
default['openssl']['csr'] = "#{node['openssl']['home']}/secureci.csr"
default['openssl']['crt'] = "#{node['openssl']['home']}/secureci.crt"
  
default['secureci']['hostname'] = "secureci"
