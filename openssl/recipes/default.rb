#
# Cookbook Name:: openssl
# Recipe:: default
#
# Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
include_recipe "apt"
include_recipe "aptupdate"

package 'openssl' do
  action :install
end

package 'libssl-dev' do
  action :install
end

directory node['openssl']['home'] do
  owner node['openssl']['user']
  group node['openssl']['group']
  recursive true
  mode 00755
end

#generate our certificate, and add it to our java keystore
execute "generate crt" do
  command <<-EOF
    openssl req -new -x509 -nodes -out #{node['openssl']['crt']} -keyout #{node['openssl']['key']} -subj '/C=#{node['country']}/ST=#{node['state']}/L=#{node['locality']}/O=#{node.default['name']}/CN=#{node.default['name']}/emailAddress=#{node['email']}'
    EOF
  not_if { ::File.exists?(node['openssl']['crt'])}
end
#add our certificate to the ca-certs files
link "/usr/share/ca-certificates/#{node['secureci']['hostname']}.crt" do
  action :delete
  only_if "test -L /usr/share/ca-certificates/#{node['secureci']['hostname']}.crt"
end
link "/usr/share/ca-certificates/#{node['secureci']['hostname']}.crt" do
  to node['openssl']['crt']
  action :create
end
execute "add crt to ca-certs" do
  command <<-EOF
    echo "#{node['secureci']['hostname']}.crt" >> /etc/ca-certificates.conf
    update-ca-certificates -f
  EOF
end
