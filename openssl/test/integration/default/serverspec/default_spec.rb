require 'spec_helper'

describe 'openssl install' do
  let(:node) { JSON.parse(IO.read('/tmp/kitchen/chef_node.json')) }

describe command("dpkg -l | grep -i openssl ") do
    its(:stdout) { should contain    'libgnutls-openssl27:amd64'}
    its(:exit_status) { should eq 0 }
end
describe command("sudo ls /usr/lib/x86_64-linux-gnu/libssl* ") do
    its(:stdout) { should contain  'libssl.a' }
    its(:exit_status) { should eq 0 }
  end

end
describe 'firewall install' do


end

describe 'secureci' do


end
