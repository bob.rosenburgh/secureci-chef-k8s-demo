name             "openssl"
maintainer       "Max Saperstone"
maintainer_email "max.saperstone@coveros.com"
license          "All Rights Reserved"
description      "Install openssl into our application"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "install openssl"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.2.0"

supports 'ubuntu'

depends "aptupdate"
depends "apt"

# 0.2.0 removed things that are not openssl releated
# 0.0.19 updating unit tests
# 0.0.18 food critic fixes; adding in nexus integration
# 0.0.17 adding dependency of java needed for keystore
# 0.0.16 adding generate cert into java keystore and ca-cert
# 0.0.15 setting the gitblit port to an attribute; fixed reverse proxy; updated port forwarding
# 0.0.14 externalized htpasswd reference to subversion attributes
# 0.0.13 added unit/integration tests
# 0.0.12 minor update to add port selection for sonar.
# 0.0.11 minor update to add port selection for jenkins.
# 0.0.9 adding in switch statement for setting ip based on ami or vmi
# 0.0.8 adding in startup script to fix ip
# 0.0.7 adding in gitblit to ssl proxy forwarding
# 0.0.6 fixing ssl config (only allowing access over https)
# 0.0.5 added trac config
# 0.0.4 added firewall recipe
# 0.0.3 added ssl templates
# 0.0.2 adding certificate and other apache mods
# 0.0.1 initial file of package
