name             'secureci-demojobs'
maintainer       'Coveros'
maintainer_email 'glenn.buckholz@coveros.com'
license          'All rights reserved'
description      'Installs/Configures secureci-demojobs'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

supports 'ubuntu'

version          '0.1.0'
