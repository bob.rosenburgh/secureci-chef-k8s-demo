#
# Cookbook Name:: secureci-demojobs
# Recipe:: default
#
# Copyright 2017, Coveros
#
# All rights reserved - Do Not Redistribute
#
add_demo=node['demo']

if add_demo == 1
  
  package "xvfb" do
    action :install
  end

  package "firefox" do
    action :install
  end

  package "uuid" do
    action :install
  end

  directory '/var/lib/jenkins/jobs/demo/jobs' do
    owner 'jenkins'
    group 'jenkins'
    mode '0755'
    recursive true
    action :create
  end

  directory '/var/lib/jenkins/jobs/demo/jobs/Dockerbuild-build' do
    owner 'jenkins'
    group 'jenkins'
    mode '0755'
    action :create
  end

  directory '/var/lib/jenkins/jobs/demo/jobs/Dockerbuild-mysql' do
    owner 'jenkins'
    group 'jenkins'
    mode '0755'
    action :create
  end

  directory '/var/lib/jenkins/jobs/demo/jobs/Dockerbuild-tomcat' do
    owner 'jenkins'
    group 'jenkins'
    mode '0755'
    action :create
  end

  directory '/var/lib/jenkins/jobs/demo/jobs/dockerpipeline' do
    owner 'jenkins'
    group 'jenkins'
    mode '0755'
    action :create
  end

  directory '/var/lib/jenkins/jobs/demo/jobs/javademo' do
    owner 'jenkins'
    group 'jenkins'
    mode '0755'
    action :create
  end

  template '/var/lib/jenkins/jobs/demo/config.xml' do
    owner 'jenkins'
    group 'jenkins'
    mode '0644'
    source 'demofolder-config.xml.erb'
  end

  template '/var/lib/jenkins/jobs/demo/jobs/Dockerbuild-build/config.xml' do
    owner 'jenkins'
    group 'jenkins'
    mode '0644'
    source 'dockerbuild-build-config.xml.erb'
  end

  template '/var/lib/jenkins/jobs/demo/jobs/Dockerbuild-mysql/config.xml' do
    owner 'jenkins'
    group 'jenkins'
    mode '0644'
    source 'dockerbuild-mysql-config.xml.erb'
  end

  template '/var/lib/jenkins/jobs/demo/jobs/Dockerbuild-tomcat/config.xml' do
    owner 'jenkins'
    group 'jenkins'
    mode '0644'
    source 'dockerbuild-tomcat-config.xml.erb'
  end

  template '/var/lib/jenkins/jobs/demo/jobs/dockerpipeline/config.xml' do
    owner 'jenkins'
    group 'jenkins'
    mode '0644'
    source 'dockerpipeline-config.xml.erb'
  end

  template '/var/lib/jenkins/jobs/demo/jobs/javademo/config.xml' do
    owner 'jenkins'
    group 'jenkins'
    mode '0644'
    source 'javademo-config.xml.erb'
  end
  service "jenkins demojobs restart" do
    service_name   "jenkins"
    action :restart
  end
end
