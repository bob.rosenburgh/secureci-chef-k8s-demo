#
# Cookbook Name:: secureci-packages
# Spec:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'secureci-packages::default' do
	let :chef_run do
		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
	end


	it 'converges successfully' do
		chef_run.converge(described_recipe)
		expect { chef_run }.to_not raise_error
	end
	it 'installs package enscript' do 
		expect(chef_run).to install_package('enscript')
	end
	it 'installs package vim' do 
		expect(chef_run).to install_package('vim')
	end
	it 'installs package build-essential' do 
		expect(chef_run).to install_package('build-essential')
	end
	it 'installs package iptables' do 
		expect(chef_run).to install_package('iptables')
	end
	it 'installs package apg' do 
		expect(chef_run).to install_package('apg')
	end
	it 'installs package man-db' do 
		expect(chef_run).to install_package('man-db')
	end
	it 'installs package curl' do 
		expect(chef_run).to install_package('curl')
	end
	at_exit { ChefSpec::Coverage.report! }
end
