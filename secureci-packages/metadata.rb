name 'secureci-packages'
maintainer 'Glenn Buckholz'
maintainer_email 'glenn.buckholz@coveros.com'
license 'All Rights Reserved'
description 'Installs/Configures secureci-packages'
long_description 'Installs/Configures secureci-packages'
chef_version     '>= 13.0' if respond_to?(:chef_version)
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
version '0.1.0'

supports 'ubuntu'

depends 'aptupdate'
depends 'postfix'
