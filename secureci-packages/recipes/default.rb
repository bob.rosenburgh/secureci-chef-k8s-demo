#
# Cookbook Name:: secureci-packages
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

include_recipe 'aptupdate'

%w{enscript vim build-essential iptables apg man-db}.each do |pkg|
  package pkg do
    action :install
  end
end

include_recipe 'postfix'
