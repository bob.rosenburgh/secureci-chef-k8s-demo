require 'spec_helper'

describe 'secureci-packages::default' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html
  describe command("dpkg -l ") do 
  	its(:stdout) { should contain("enscript")}
  	its(:stdout) { should contain("vim")}
  	its(:stdout) { should contain("build-essential")}
  	its(:stdout) { should contain("iptables")} 
  	its(:stdout) { should contain("apg")}
  	its(:stdout) { should contain("man-db")}
  	its(:stdout) { should contain("zip")}
  end
end
