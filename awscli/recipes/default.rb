#
# Cookbook:: aws-cli
# Recipe:: default
#
# Copyright:: 2017, Coveros Inc, All Rights Reserved.

#install aws command line tools
package "awscli" do
  version node['awscli']['install_version']
  action :install
end