require 'spec_helper'
require 'json'

def node
   JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end


describe command('aws --version') do
  its(:stdout) { should contain(node['default']['awscli']['version'])}
  its(:exit_status) { should eq 0 }
end