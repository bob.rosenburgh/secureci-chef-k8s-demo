default['awscli']['version'] = '1.10.1'
default['awscli']['install_version'] = "#{node['awscli']['version']}-1"

default['awscli']['home'] = '/usr/bin/aws'