name 'awscli'
maintainer 'Glenn Buckholz'
maintainer_email 'glenn.buckholz@coveros.com'
license 'All Rights Reserved'
description 'Installs/Configures aws-cli'
long_description 'Installs/Configures aws-cli'
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version '0.1.0'

supports 'ubuntu'

depends "apt"
