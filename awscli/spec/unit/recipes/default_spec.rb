#
# Cookbook:: awscli
# Spec:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

require 'spec_helper'

describe 'awscli::default' do
  let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04')
  end

  it 'should install awscli package' do
  	chef_run.converge(described_recipe)
    expect(chef_run).to install_package('awscli').with(
      version:  chef_run.node['awscli']['install_version']
    )
  end   
  at_exit { ChefSpec::Coverage.report! }
end