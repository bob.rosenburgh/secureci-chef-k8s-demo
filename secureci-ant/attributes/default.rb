default['ant']['version']        = '1.9.7'
default['ant']['home']           = '/usr/share/ant'

default['secureci-ant']['version'] = node['ant']['version']
default['secureci-ant']['home'] = node['ant']['home']