#
# Cookbook:: secureci-ant
# Spec:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

require 'spec_helper'

describe 'secureci-ant::default' do
	context 'When all attributes are default, on an unspecified platform' do
		let :chef_run do
			ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
		end

		it 'converges successfully' do
			chef_run.converge(described_recipe)
			expect { chef_run }.to_not raise_error
		end
	end
	at_exit { ChefSpec::Coverage.report! }
end
