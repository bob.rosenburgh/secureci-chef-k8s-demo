#
# Cookbook Name:: prime-demo
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

#  Run all the docker jobs
add_demo=node['demo']

if add_demo == 1

  execute "Creating Docker Containers via CLI" do
    command <<-EOF
    #This needs to be replaced I hate sleeps
    sleep 50;
    echo "Creating Docker Containers";
    java -jar /var/cache/jenkins/war/WEB-INF/jenkins-cli.jar -s http://127.0.0.1:#{node['jenkins']['master']['port']}/jenkins build 'demo/Dockerbuild-build';
    java -jar /var/cache/jenkins/war/WEB-INF/jenkins-cli.jar -s http://127.0.0.1:#{node['jenkins']['master']['port']}/jenkins build 'demo/Dockerbuild-mysql';
    java -jar /var/cache/jenkins/war/WEB-INF/jenkins-cli.jar -s http://127.0.0.1:#{node['jenkins']['master']['port']}/jenkins build 'demo/Dockerbuild-tomcat';

    # Give the jobs time to register as building
    sleep 50;

    #I need a wait here so the build jobs finish. 
    while [ `curl -k http://#{node['initial_user']}:#{node['password']}@127.0.0.1:#{node['jenkins']['master']['port']}/jenkins/job/demo/job/Dockerbuild-build/lastBuild/api/xml?depth=1 2>/dev/null | grep building | sed -e 's/.*<building>\\(.*\\)<\\/building>.*/\\1/g'` = "true" ] ; do echo "Sleeping until build container built!"; sleep 5; done;
    while [ `curl -k http://#{node['initial_user']}:#{node['password']}@127.0.0.1:#{node['jenkins']['master']['port']}/jenkins/job/demo/job/Dockerbuild-mysql/lastBuild/api/xml?depth=1 2>/dev/null | grep building | sed -e 's/.*<building>\\(.*\\)<\\/building>.*/\\1/g'` = "true" ] ; do echo "Sleeping until mysql container built!"; sleep 5; done;
    while [ `curl -k http://#{node['initial_user']}:#{node['password']}@127.0.0.1:#{node['jenkins']['master']['port']}/jenkins/job/demo/job/Dockerbuild-tomcat/lastBuild/api/xml?depth=1 2>/dev/null | grep building | sed -e 's/.*<building>\\(.*\\)<\\/building>.*/\\1/g'` = "true" ] ; do echo "Sleeping until tomcat container built!"; sleep 5; done;
    EOF
  end
end
