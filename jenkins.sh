#!/bin/bash
. /etc/profile.d/aws.sh

size="t2.micro"
case "$Machine_Size" in
  Micro)
    size="t2.micro"
    ;;
  Small)
    size="t2.small"
    ;;
  Medium)
    size="t2.medium"
    ;;
  Large)
    size="m3.large"
    ;;
  X-Large)
    size="m3.xlarge"
    ;;
esac

ami="ami-9eaa1cf6"
user="ubuntu"
case "$Machine_Base" in
  Ubuntu_14.04)
    ami="ami-9eaa1cf6"
    user="ubuntu"
    ;;
esac

echo ""
echo "Installing Software on ${Machine_Base/_/ }:"

IFS=',' read -a softwareArray <<< "initialize::initialize,${Software,,},trac,initialize::cleanup"
if $Package; then
  softwareArray+=('initialize::package')
fi

software=""
for element in "${softwareArray[@]}"
do
  echo "  $element"
  software="$software recipe[$element],"
done

software=$(echo $software | cut -c 1-)
echo ""
echo ""

taggedName="SecureCI_${Machine_Name}_$BUILD_ID"

#using knife ec2
#knife client list -c ~/.chef/knife.rb
#knife ec2 flavor list -c ~/.chef/knife.rb -A AKIAITDPHF26TLCJPNMQ -K ITTslsX34E3BjbpRARrLz4JCUb3TcyNy8dcoSza9 -VV
knife ec2 server create -c ~/.chef/knife.rb -I $ami -f $size -x $user -N $taggedName -A AKIAITDPHF26TLCJPNMQ -K ITTslsX34E3BjbpRARrLz4JCUb3TcyNy8dcoSza9 -S jenkins-secureci -i ~/.chef/jenkins-secureci.pem --subnet subnet-280ce803 --ebs-size ${Drive_Size} -g sg-1897ad7d -T "Jenkins Build Number"=$BUILD_NUMBER,"Jenkins Build Id"=$BUILD_ID,"Name"=$taggedName --associate-public-ip -r "$software"

#locked down security group - -g sg-1897ad7d
#public security group - -g sg-31eed754

public_ip=$(/opt/chef/embedded/bin/ruby /var/lib/jenkins/cd_platform/bin/get_aws_data.rb --instance-name $taggedName --data-id "PublicIpAddress")
echo ""
echo ""
if ! $Package; then
  if [ "${Username}" != "" ]; then
    echo "Setting-up username of ${Username} for Web Access"
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ubuntu@${public_ip} -i ~/.chef/jenkins-secureci.pem "sudo htpasswd -b /var/lib/passwd/dav_svn.passwd ${Username} ${Password}"
    echo "Setting-up user on the box"
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ubuntu@${public_ip} -i ~/.chef/jenkins-secureci.pem "sudo adduser --disabled-password --gecos '' ${Username}"
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ubuntu@${public_ip} -i ~/.chef/jenkins-secureci.pem "sudo usermod -a -G sudo ${Username}"
    if [ "$Public_Key" != "" ]; then
      echo "Setting up public key on the box"
      ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no ubuntu@${public_ip} -i ~/.chef/jenkins-secureci.pem "sudo mkdir /home/${Username}/.ssh; cd /home/${Username}/.ssh; sudo touch authorized_keys; sudo chmod 777 authorized_keys; echo '${Public_Key}' > authorized_keys; sudo chmod 600 authorized_keys; cd ../; sudo chown -R ${Username}.${Username} .ssh"
    fi
  fi
  echo ""
  echo ""
  echo "Machine is up and running."
  
  if [ "${Username}" != "" ]; then
    echo "Machine can be accessed at 'https://${public_ip}/trac' via a browser, and logged into via username and password provided in build parameters."
    if [ "$Public_Key" != "" ]; then
      echo "Machine can be accessed at '${public_ip}' via ssh, and logged into via username and key provided in build parameters."
    fi
  fi
  
  echo ""
  echo "To destroy the machine, click this link: https://coveros.secureci.com/jenkins/job/SecureCI-Destroy/buildWithParameters?Machine_Name=SecureCI_${Machine_Name}_$BUILD_ID"
  echo "Nothing will show on the screen"
else
  instance_id=$(/opt/chef/embedded/bin/ruby /var/lib/jenkins/cd_platform/bin/get_aws_data.rb --instance-name $taggedName --data-id "InstanceId")

  echo "Packaging generated instance into an AMI."
  aws ec2 create-image --instance-id $instance_id --name $Machine_Name --description $taggedName
  
  echo "Terminating generated instance."
  aws ec2 terminate-instances --instance-ids $instance_id
fi