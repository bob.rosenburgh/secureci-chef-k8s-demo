#
# Cookbook Name:: zip
# Spec:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

require 'spec_helper'
describe 'zip::default' do

	let :chef_run do
		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
	end

	it 'installs package zip' do 
		expect(chef_run).to install_package('zip')
	end
	it 'installs package unzip' do 
		expect(chef_run).to install_package('unzip')
	end
	it 'installs package p7zip-full' do 
		expect(chef_run).to install_package('p7zip-full')
	end
end