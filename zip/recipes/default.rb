#
# Cookbook Name:: zip
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.
include_recipe "apt"
include_recipe "aptupdate"

%w{zip unzip p7zip-full}.each do |pkg|
  package pkg do
    action :install
  end
end