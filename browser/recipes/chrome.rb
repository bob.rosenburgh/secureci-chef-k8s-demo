#
# Cookbook Name:: browser
# Recipe:: chrome
# Author:: Max Saperstone
#
# Copyright 2015, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe "browser::default"

chrome_deb = node['browser']['chrome']['file']
remote_file "/root/#{chrome_deb}" do
  source node['browser']['chrome']['url']
  mode 0755
  action :create_if_missing
end


########## sudo apt-get install libglib2.0-dev libglib2.0-0=2.48.2-0ubuntu1

# TODO:  libgtk2.0-0-dbg invalid in 16.04
package %w( zip libgtk2.0-bin libgtk2.0-cil libgtk2.0-dev libgtk2.0-doc libgtk2.0-0 libgtk2.0-common libgtk2.0-cil-dev libgdk-pixbuf2.0 libatk1.0 libgconf-2-4 libfontconfig1 libcairo2 libasound2 gconf-service libnspr4 libnss3 libpango1.0 libpango1.0 libxss1 libxtst6 fonts-liberation libappindicator1 xdg-utils )


execute "ensure target dir for existance" do
  command "mkdir -p #{node['tools']['base_dir']}/secureci-testing-framework-#{node['tools']['secureci-testing-framework']['version']}"
  not_if { ::File.directory?("#{node['tools']['base_dir']}/secureci-testing-framework-#{node['tools']['secureci-testing-framework']['version']}") }
end

execute "dpkg #{chrome_deb}" do
  cwd "/root/"
  command "dpkg -i #{chrome_deb}"
  user "root"
  action :run
  ignore_failure true      #has a failure - need to ignore it - below line will fix it
end
execute "install #{chrome_deb}" do
  command "apt-get -f -y install"
  user "root"
  action :run
end
execute "remove #{chrome_deb}" do
  cwd "/root/"
  command "rm -rf #{chrome_deb}"
  user "root"
  action :run
end

#get our chrome driver to go with our chrome browser
chromedriver_zip = node['browser']['chromedriver']['file']
remote_file "/root/#{chromedriver_zip}" do
  source node['browser']['chromedriver']['url']
  mode 0755
  action :create_if_missing
  not_if { ::File.exists?("/root/#{chromedriver_zip}") || ::File.exists?("/root/chromedriver")}
end
execute "unzip #{chromedriver_zip}" do
  cwd "/root/"
  command "unzip #{chromedriver_zip}"
  user "root"
  action :run
  not_if { ::File.exists?("/root/chromedriver")}
end
execute "install chromedriver" do
  cwd "/root/"
  command "mv chromedriver #{node['tools']['base_dir']}/secureci-testing-framework-#{node['tools']['secureci-testing-framework']['version']}/"
  user "root"
  action :run
end
execute "chown chromedriver" do
  cwd node['tools']['base_dir']
  command "chown #{node['tools']['user']}:#{node['tools']['group']} secureci-testing-framework-#{node['tools']['secureci-testing-framework']['version']}/chromedriver"
  user "root"
  action :run
end
execute "chmod chromedriver" do
  cwd node['tools']['base_dir']
  command "chmod +x secureci-testing-framework-#{node['tools']['secureci-testing-framework']['version']}/chromedriver"
  user "root"
  action :run
end
execute "remove #{chromedriver_zip}" do
  cwd "/root/"
  command "rm -rf #{chromedriver_zip}"
  user "root"
  action :run
end

include_recipe "browser::xvfb"
