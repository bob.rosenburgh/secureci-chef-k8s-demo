#
# Cookbook Name:: browser
# Recipe:: xvfb
# Author:: Max Saperstone
#
# Copyright 2015, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
include_recipe "browser::default"

#need to install xvfb - just use aptitude
package "xvfb" do
  action :install
end

#need to configure xvfb plugin in jenkins
execute "configure Xvfb Plugin" do
	command "curl -v --data-urlencode \"script=$(echo 'a=Jenkins.instance.getExtensionList(org.jenkinsci.plugins.xvfb.XvfbInstallation.DescriptorImpl.class)[0];a.setInstallations(new org.jenkinsci.plugins.xvfb.XvfbInstallation(\"Default\", \"\", []));a.save();')\" http://localhost:8080/jenkins/scriptText"
end