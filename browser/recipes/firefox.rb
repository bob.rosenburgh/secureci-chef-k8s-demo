#
# Cookbook Name:: browser
# Recipe:: firefox
# Author:: Max Saperstone
#
# Copyright 2015, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
include_recipe "browser::default"

firefox = node['browser']['firefox']['install_file']
remote_file "#{node['browser']['firefox']['home']}/#{firefox}" do
  source node['browser']['firefox']['url']
  mode 0755
  action :create_if_missing
  not_if { ::File.directory?("#{node['browser']['firefox']['home']}/firefox") }
end
execute "untar #{firefox}" do
    cwd node['browser']['firefox']['home']
    command "tar xvjf #{firefox}"
    user "root"
    action :run
    not_if { ::File.directory?("#{node['browser']['firefox']['home']}/firefox") }
end
execute "remove #{firefox}" do
  cwd node['browser']['firefox']['home']
  command "rm -rf #{firefox}"
  user "root"
  action :run
end
execute "create link for firefox" do
  command "sudo ln -s #{node['browser']['firefox']['home']}/firefox/firefox #{node['browser']['firefox']['base']}/firefox"
  user "root"
  action :run
end

#link "#{node['browser']['firefox']['home']}/firefox/firefox" do
#  to "#{node['browser']['firefox']['base']}/firefox"
# link_type :symbolic
#  action :create
# end

include_recipe "browser::xvfb"
