require 'spec_helper'
require 'json'

def node
   JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

firefox_install = node['default']['browser']['firefox']['install']

describe command('firefox --version') do
  its(:stdout) { should contain(node['default']['browser']['firefox']['version'])}
  its(:exit_status) { should eq 0 }
 end

describe file(firefox_install) do
  it { should be_directory }
  it { should be_owned_by 'root' }
  it { should be_grouped_into 'root' }
end

describe command('google-chrome --version') do
   its(:stdout) { should contain(node['default']['browser']['chrome']['version'])}
   its(:exit_status) { should eq 0 }
 end
describe command('xdpyinfo -display :0 >/dev/null 2>&1 && echo "In use" || echo "Free"') do
   its(:stdout) { should match "Free" }
   its(:exit_status) { should eq 0 }
end
