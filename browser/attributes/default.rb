default['tools']['user'] = 'www-data'
default['tools']['group'] = 'www-data'
default['tools']['base_dir'] = '/usr/java'
default['tools']['secureci-testing-framework']['version'] = "1.3.0"
default['browser']['firefox']['version'] = '47.0'
default['browser']['firefox']['install_file'] = "firefox-#{node['browser']['firefox']['version']}.tar.bz2"
default['browser']['firefox']['base'] = '/usr/bin'
default['browser']['firefox']['home'] = '/usr/local'
default['browser']['firefox']['install'] = "#{node['browser']['firefox']['home']}/firefox"
default['browser']['firefox']['url'] = "http://ftp.mozilla.org/pub/mozilla.org/firefox/releases/#{node['browser']['firefox']['version']}/linux-x86_64/en-US/#{node['browser']['firefox']['install_file']}"

#files found here - http://dl.google.com/linux/chrome/deb/pool/main/g/google-chrome-stable/
default['browser']['chrome']['deps'] = "libgtk2 libgdk-pixbuf2 libgconf libfontconfig1 libcairo2 libatk1 libasound2 gconf-service"
default['browser']['chrome']['version'] = '54.0.2840.100'
default['browser']['chrome']['file'] = "google-chrome-stable_#{node['browser']['chrome']['version']}-1_amd64.deb"
default['browser']['chrome']['url'] = "http://mirror.glendaleacademy.org/chrome/pool/main/g/google-chrome-stable/#{node['browser']['chrome']['file']}"
#default['browser']['chrome']['url'] = "http://dl.google.com/linux/chrome/deb/pool/main/g/google-chrome-stable/#{no    de['browser']['chrome']['file']}"
default['browser']['chrome']['install'] = "/opt/google/chrome"

#verify chromedriver version supports chrome version - see release note http://chromedriver.storage.googleapis.com/2.14/notes.txt 
default['browser']['chromedriver']['version'] = '2.24'
default['browser']['chromedriver']['file'] = 'chromedriver_linux64.zip'
default['browser']['chromedriver']['url'] = "http://chromedriver.storage.googleapis.com/#{node['browser']['chromedriver']['version']}/#{node['browser']['chromedriver']['file']}"

