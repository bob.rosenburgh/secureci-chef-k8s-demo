name             "browser"
maintainer       "Max Saperstone"
maintainer_email "max.saperstone@coveros.com"
license          "All Rights Reserved"
description      "Install browsers and tools into our application"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "firefox chrome xvfb"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.0.9"

supports 'ubuntu'

depends 'zip'
depends 'aptupdate'
depents 'secureci-jenkins'

# 0.0.9 - updating for foodcritic
# 0.0.8 - updating versions compatible with ubuntu 16.04
# 0.0.7 - updating versions & vagrant/kitchen compatibility
# 0.0.6 - updating versions
# 0.0.5 - updating versions
# 0.0.4 - locked chrome to singular version
# 0.0.3 - tied xvfb install to version specified in attributes
# 0.0.2 - tied firefox install to version specified in attributes
# 0.0.1 - initial browser recipes
