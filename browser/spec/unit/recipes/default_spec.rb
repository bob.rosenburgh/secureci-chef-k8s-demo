# Spec:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
require 'spec_helper'

describe 'browser::chrome' do


let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end

let :chromedriver_zip do
    chef_run.node['browser']['chromedriver']['file']
 end
let :tools do 
    chef_run.node['tools']['base_dir']
 end
let :secureci_version do
    chef_run.node['tools']['secureci-testing-framework']['version']
 end
let :chrome_deb do
    chef_run.node['browser']['chrome']['file']
 end     

it 'should create a remote file' do 
    expect(chef_run).to create_remote_file_if_missing("/root/#{chrome_deb}").with(
    source:   chef_run.node['browser']['chrome']['url'],
  )
  end
it "should execute dpkg" do 
    expect(chef_run).to run_execute("dpkg -i #{chrome_deb}")
  end
it "should execute install chrome" do  
    expect(chef_run).to run_execute("apt-get -f -y install")
  end     
it "should execute remove chrome zip" do
    expect(chef_run).to run_execute("rm -rf #{chrome_deb}")
  end 
it 'should create a remote file' do  
    expect(chef_run).to create_remote_file_if_missing("/root/#{chromedriver_zip}").with(
    source:   chef_run.node['browser']['chromedriver']['url'],
  )
  end 
it "should execute unzip chromedriver}" do  
    expect(chef_run).to run_execute("unzip #{chromedriver_zip}")
  end 
it "should execute mv chromedriver" do
    expect(chef_run).to run_execute("mv chromedriver #{tools}/secureci-testing-framework-#{secureci_version}/")
  end
it "should execute chown chromedriver" do
    user  = chef_run.node['tools']['user']
    group = chef_run.node['tools']['group']
    expect(chef_run).to run_execute("chown #{user}:#{group} secureci-testing-framework-#{secureci_version}/chromedriver")
  end
it "should execute chmod chromedriver" do
    expect(chef_run).to run_execute("chmod +x secureci-testing-framework-#{secureci_version}/chromedriver")
  end
it "should execute remove chromedriver_zip" do
    expect(chef_run).to run_execute("rm -rf #{chromedriver_zip}")
  end
 end
describe 'browser::firefox' do
let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
  end
 
let :firefox_install do 
    chef_run.node['browser']['firefox']['install_file']
  end
let :firefox_home do 
    chef_run.node['browser']['firefox']['home']
  end

it 'should create a remote file' do  
    expect(chef_run).to create_remote_file_if_missing("#{firefox_home}/#{firefox_install}").with(
    source:   chef_run.node['browser']['firefox']['url'],
  )
  end
it "should execute untar firefox" do  
    expect(chef_run).to run_execute("tar xvjf #{firefox_install}")
  end 
it "should remove firefox" do 
    expect(chef_run).to run_execute("rm -rf #{firefox_install}")
  end
it "should create a link to firefox" do 
    base = chef_run.node['browser']['firefox']['base']
    expect(chef_run).to run_execute("sudo ln -s #{firefox_home}/firefox/firefox #{base}/firefox")
  end
describe 'browser::xvfb' do
let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
  end 

it 'should install xvfb package' do
   expect(chef_run).to install_package('xvfb').with(
    version:  chef_run.node['browser']['xvfb']['install_version']
  )
  end   
 end
at_exit { ChefSpec::Coverage.report! }
end
