# Cookbook Name:: trac
# Attribute :: default
#
# Copyright 2014 Max Saperstone <max.saperstone@coveros.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0c
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#



##############################
#############default attributes
##############################
default['trac']['user'] = 'www-data'
default['trac']['group'] = 'svn'

default['trac']['version'] = '1.2'
default['trac']['url'] = "https://download.edgewall.org/trac/Trac-#{node['trac']['version']}.tar.gz"

default['trac']['base'] = '/opt'
default['trac']['home'] = "#{node['trac']['base']}/trac-#{node['trac']['version']}"
default['trac']['home_link'] = "/var/lib/trac"

default['trac']['project']['name'] = "secureci"
default['trac']['project']['title'] = "SecureCI"
default['trac']['project']['description'] = ""
default['trac']['project']['dir'] = "#{node['trac']['home']}/#{node['trac']['project']['name']}"
##############################
#############database attributes
##############################
default['trac']['database']['name'] = 'trac'
default['trac']['database']['user'] = 'trac'
default['trac']['database']['password'] = 'potatOEz'
default['trac']['database']['host'] = '127.0.0.1'
default['trac']['database']['domain'] = 'localhost'

##############################
#############plugin attributes
##############################
default['trac']['plugins'] = [ "tracjenkinsplugin", "tracwysiwygplugin", 
    "tracsvnpoliciesplugin", "svnauthzadminplugin", "accountmanagerplugin", 
    "navaddplugin", "iniadminplugin", "breadcrumbsnavplugin", "tocmacro", 
    "xmlrpcplugin", "batchmodifyplugin", "ticketimportplugin", 
    "externallinksnewwindowplugin" ]
#jenkins plugin
default['trac']['tracjenkinsplugin']['version'] = 'trunk'
#wysiwyg plugin
default['trac']['tracwysiwygplugin']['version'] = '0.12'
#svn policies plugin
default['trac']['tracsvnpoliciesplugin']['version'] = '0.11'
#svn auths admin plugin
default['trac']['svnauthzadminplugin']['version'] = '1.0'
#account manager plugin
default['trac']['accountmanagerplugin']['version'] = 'trunk' #version 0.11 doesn't work
#nav add plugin
default['trac']['navaddplugin']['version'] = 'trunk'  #version 0.9 doesn't work
#ini admin plugin
default['trac']['iniadminplugin']['version'] = '0.11'
#breadcrumbs nav plugin
default['trac']['breadcrumbsnavplugin']['version'] = 'trunk'
#toc macro plugin
default['trac']['tocmacro']['version'] = '0.11'
#xml rpc plugin
default['trac']['xmlrpcplugin']['version'] = '0.10'
#batch modify plugin
default['trac']['batchmodifyplugin']['version'] = '0.12/trunk'
#ticket import plugin
default['trac']['ticketimportplugin']['version'] = '0.11'
#open links in new window
default['trac']['externallinksnewwindowplugin']['version'] = '0.11'
##############################
#############wiki attributes
##############################
default['trac']['wiki']['charset'] = "utf-8"

default['trac']['wiki']['footer'] = "{{{
#!comment
Documentation for SecureCI by Coveros, Inc. is licensed under a Creative Commons Attribution 3.0 United States License.
}}}"

###############################
#############agilo attribute
###############################
default['agilo']['version'] = "0.9.15"
default['agilo']['tar'] = "agilo_source-#{node['agilo']['version']}.tar.gz"

default['agilo']['base'] = "/opt"
default['agilo']['home'] = "#{node['agilo']['base']}/agilo-#{node['agilo']['version']}"
default['agilo']['link'] = "/var/lib/agilo"


###############################
#TODO: these exist because there is no null gaurd in the template render FIXME
###############################
default['browser']['firefox']['install'] = "/usr/local"
default['browser']['chrome']['install'] = "/opt/google/chrome"
default['ratproxy']['base_dir'] = "/usr/src/ratproxy"
default['zap']['home'] = "/opt/zap-2.5.0"
default['openscap']['home'] = "/opt/openscap-1.2.11"
default['tomcat']['install_dir'] = "/var/lib/tomcat7"
default['yasca']['home'] = "/opt/yasca"
default['gitblit']['home'] = "/opt/gitblit-1.8.0"
default['maven']['m2_home'] = "/usr/local/maven"
default['secureci-ant']['home'] = "/usr/share/ant"
default['jenkins']['home_dir'] = "/var/lib/jenkins" 
default['nexus']['home'] = "/opt/nexus-3.2.0-01"
default['sonar']['home'] = "/opt/sonar-6.7.6"
default['secureci-firewall']['home'] = "/etc/firewall"
default['browser']['chromedriver']['version'] = ""
default['browser']['xvfb']['version'] = ""
default['postfix']['mail_version'] = "none"
default['python']['version']= "none"