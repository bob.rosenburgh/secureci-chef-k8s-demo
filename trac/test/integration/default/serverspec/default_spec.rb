require 'spec_helper'
def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end
RSpec::Matchers.define :match_key_value do |key, value|
  match do |actual|
    actual =~ /^\s*?#{key}\s*?=\s*?#{value}/
  end
end
port       = node['default']['mysql']['port']
socket     = node['default']['mysql']['socket']
sqlcommand = "mysql -S #{socket} -e 'SHOW DATABASES;' -uroot -p#{node['default']['mysql']['server_root_password']}"
trac_home     = node['default']['trac']['home']
trac_version  = node['default']['trac']['version'] 
trac_basedir  = node['default']['trac']['basedir']
htpasswd_file = node['default']['subversion']['auth_user_file']
trac_plugins  = node['default']['trac']['plugins']
project_dir   = "/var/lib/trac/secureci"
trac_ini      = "#{project_dir}/conf/trac.ini"
mysqlcheck    = "mysql -e 'show tables from trac;' -uroot -p#{node['default']['trac']['database']['password']}"
wsgi_conf     = "/etc/apache2/mods-enabled/wsgi.conf" #not enabled due to bug / ommitting related tests until fix is released
ini_plugins   = [
"tracwysiwyg.*", 
"svnpolicies.*",
"svnauthz.*",
"navadd.*",
"iniadmin.iniadmin.iniadminplugin",
"breadcrumbsnavplugin.*",
"tractoc.*",
"batchmod.*",
"talm_importer.*",
"tracextlinksnewwindow.*"
]
describe 'trac' do
describe file("#{trac_home}") do
  it { should be_directory }
  it { should be_owned_by node['default']['trac']['user'] }
  it { should be_grouped_into node['default']['trac']['group'] }
  it { should_not be_readable.by  'other'}
  end
describe file(trac_ini) do 
    it { should be_owned_by node['default']['trac']['user'] }
    it { should be_grouped_into node['default']['trac']['group'] }
    its(:content) { should match_key_value('htpasswd_file', node['default']['subversion']['auth_user_file']) }
    its(:content) { should match_key_value('name', node['default']['trac']['project_name']) }
    its(:content) { should match_key_value('authz_file',  node['default']['subversion']['auth_access_file']) }
    its(:content) { should match_key_value('database',  "mysql://trac:#{node['default']['mysql']['server_root_password']}@127.0.0.1/trac") }
    its(:content) { should match_key_value('default_charset',   node['default']['trac']['wiki']['charset']) }
    its(:content) { should match_key_value('.dir',  "#{node['default']['subversion']['repo_dir']}/#{node['default']['subversion']['repo_name']}" ) }
    its(:content) { should match_key_value('authz_file',  node['default']['subversion']['auth_access_file']) }
end
describe file (trac_ini) do
    its(:content) { should match_key_value('tracwysiwyg.*', 'enabled') }
    its(:content) { should match_key_value('svnpolicies.*', 'enabled') } 
    its(:content) { should match_key_value('svnauthz.*', 'enabled' )}
    its(:content) { should match_key_value('navadd.*',' enabled' )}
    its(:content) { should match_key_value('iniadmin.iniadmin.iniadminplugin',' enabled') }
    its(:content) { should match_key_value('breadcrumbsnavplugin.*', 'enabled') } 
    its(:content) { should match_key_value('tractoc.*', 'enabled' )}
    its(:content) { should match_key_value('batchmod.*', 'enabled') }
    its(:content) { should match_key_value('talm_importer.*', 'enabled') }
    its(:content) { should match_key_value('tracextlinksnewwindow.*', 'enabled') }
 end
describe command("sudo trac-admin #{project_dir} upgrade") do 
  its(:stdout) { should contain("Database is up to date, no upgrade necessary.")}
  its(:exit_status) { should eq 0 }
end
describe command("sudo trac-admin #{project_dir} repository resync *") do 
  its(:stdout) { should contain("0 revisions cached.")}
  its(:exit_status) { should eq 0 }
end
describe command("#{mysqlcheck}") do 
  its(:stdout) { should contain("permission")}
  its(:stdout) { should contain("system")}
  its(:stdout) { should contain("wiki")}
end 
#describe file (wsgi_conf) do
#  it { should exist }
#  it { should be_owned_by('root') }
#  it { should be_grouped_into('root') }
#  it { should be_readable.by 'others'}
#end
describe file ("#{project_dir}/htdocs/secureci.css") do
  it { should exist }
  it { should be_owned_by node['default']['trac']['user'] }
  it { should be_grouped_into node['default']['trac']['group'] }
  it { should_not be_readable.by 'others'}
end
describe command("sudo ls #{project_dir}/htdocs/images") do
  its(:stdout) { should contain("cc-80x15.png") }
  its(:stdout) { should contain("coveros-sm.png") }
  its(:stdout) { should contain("secureci.png") } 
  its(:stdout) { should contain("favicon.ico") }
 end
describe file ("#{project_dir}/templates/site.html") do
  it { should exist }
  it { should be_owned_by node['default']['trac']['user'] }
  it { should be_grouped_into node['default']['trac']['group'] }
  it { should_not be_readable.by 'others'}
end
#describe file ("#{trac_basedir}/apache/trac.wsgi") do
#  it { should exist }
#  it { should be_owned_by node['default']['trac']['owner'] }
#  it { should be_grouped_into node['default']['trac']['group'] }
#  it { should_not be_readable.by 'others'}
#end
describe command("#{sqlcommand} | grep trac") do 
  its(:stdout) { should contain("trac")}
  its(:exit_status) { should eq 0 }
end 
end
