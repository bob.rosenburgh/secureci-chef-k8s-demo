require 'spec_helper'

describe 'trac::default' do

  let :chef_run do
   ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end
 it 'converges successfully' do
  chef_run.converge(described_recipe)
  expect { chef_run }.to_not raise_error
end
let :trac_user do 
 chef_run.node['trac']['user']
end 
let :trac_group do 
 chef_run.node['trac']['group']
end 
let :trac_home do 
 chef_run.node['trac']['home']
end
let :base_dir do 
 chef_run.node['trac']['base']
end
let :trac_project do 
 chef_run.node['trac']['project']['dir']
end
let :trac_dir do
 chef_run.node['trac']['project']['dir']
end
before(:each) do
  stub_command('/usr/sbin/apache2 -t').and_return('Syntax OK')
  stub_command("test -L /var/lib/trac/eggs/VERSION").and_return('Syntax OK')
  stub_command("test -L /var/lib/trac/apache/VERSION").and_return(true)
  stub_command("test -L /var/lib/trac/contrib/VERSION").and_return('Syntax OK')
  stub_command("test -L /usr/share/ca-certificates/secureci.crt").and_return('Syntax OK')
  stub_command("test -L /var/lib/trac").and_return('Syntax OK')
  
  allow(File).to receive(:exists?).with(anything).and_call_original
  allow(File).to receive(:exists?).with("/opt/trac-1.2/secureci").and_return(true)
end

it 'includes cookbook apache2' do
  expect(chef_run).to include_recipe('apache2')
end
it 'includes cookbook python' do
  expect(chef_run).to include_recipe('python::secureci')
end
it 'includes cookbook openssl' do
  expect(chef_run).to include_recipe('openssl::secureci')
end
it 'includes cookbook mysql' do
  expect(chef_run).to include_recipe('secureci-mysql')
end
it 'create template file create-trac-db.sql' do
  expect(chef_run).to create_template('/root/create-trac-db.sql').with(
    source: 'create-trac-db.sql.erb',
    ) 
end
it 'run mysql script' do
 expect(chef_run).to run_execute("mysql -S #{socket} --user=root --password=#{password} < /root/create-trac-db.sql")
end
it 'should delete a file' do 
 expect(chef_run).to delete_file('/root/create-trac-db.sql')
end
it 'includes cookbook subversion' do
  expect(chef_run).to include_recipe('subversion::secureci')
end
it 'includes cookbook perl' do
  expect(chef_run).to include_recipe('secureci-perl')
end
it 'should add trac user to svn' do 
  expect(chef_run).to run_execute("usermod -a -G #{chef_run.node['subversion']['group']} #{trac_user}")
end
it 'should set the base dir user/group' do
  expect(chef_run).to create_directory(base_dir).with(
    owner: "www-data",
    group: "svn",
    ) 
end
it 'trac user has access to svn' do
  sub_group = chef_run.node['subversion']['group']
  "usermod -a -G #{sub_group} www-data"
end
it "should install trac via ark" do
  expect(chef_run).to setup_py_install_ark "trac"
end
it 'should create a link' do
  link = chef_run.link(chef_run.node['trac']['home_link']) 
  expect(link).to link_to chef_run.node['trac']['home']
end
it 'executes trac-admin' do
  expect(chef_run).to run_execute("initialize trac-project")
end
it 'should create a directory' do
  expect(chef_run).to create_directory("#{trac_project}/templates").with(
    owner:  trac_user,
    group:  trac_group,
    )
end
it 'should add site.html to template' do
  expect(chef_run).to create_cookbook_file("#{trac_project}/templates/site.html")
end
it 'should create a directory' do
  expect(chef_run).to create_directory("#{trac_project}/htdocs/images").with(
    owner:  trac_user,
    group:  trac_group,
    )   
end 
it 'should add secureci.css to htdocs' do
  expect(chef_run).to create_cookbook_file("secureci.css")
end
%w[ cc-80x15.png coveros-sm.png secureci.png favicon.ico ].each do |file|
  it 'should add files to htdocs' do
    expect(chef_run).to create_cookbook_file("#{trac_project}/htdocs/images/#{file}")
  end
end
it 'execute trac-file-change' do
  expect(chef_run).to run_execute("find #{trac_home} -type f -exec chmod 0660 {} \\;")
end
it 'execute trac-directory-change' do
  expect(chef_run).to run_execute("find #{trac_home} -type d -exec chmod 2770 {} \\;")
end
it 'execute trac-owner-change' do
  expect(chef_run).to run_execute("chown -Rf #{trac_user}.#{trac_group} #{trac_home}")
end
it 'execute trac-passwd-change' do
  sub_pass = chef_run.node['subversion']['password_dir']
  expect(chef_run).to run_execute("chown -Rf #{trac_user}.#{trac_group} #{sub_pass}")
end
it 'execute set trac-project authentication' do
  initial_user = chef_run.node['initial_user']
  expect(chef_run).to run_execute("trac-admin #{trac_project} permission add #{initial_user} TRAC_ADMIN")
end 
it 'creates a template with attributes' do
  expect(chef_run).to create_template("#{trac_project}/conf/trac.ini").with(
    source: 'trac.ini.erb',
    mode:   '0660',
    owner:  trac_user,
    group:  trac_group,
    )
end
it 'execute upgrade trac' do
  expect(chef_run).to run_execute("trac-admin #{trac_project} upgrade")
end
it 'execute upgrade trac wiki' do
  expect(chef_run).to run_execute("trac-admin #{trac_project} wiki upgrade")
end
it 'should execute cron job' do 
  expect(chef_run).to create_cron("trac-admin #{trac_project} repository resync '*'").with(
    minute: '0',
    user: trac_user,
    )
end
at_exit { ChefSpec::Coverage.report! }
end
describe 'trac::plugins' do

  let :chef_run do
   ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end 
 it 'converges successfully' do
  chef_run.converge(described_recipe)
  expect { chef_run }.to_not raise_error
end
let :trac_user do  
 chef_run.node['trac']['user']
end 
let :trac_group do  
 chef_run.node['trac']['group']
end 
let :plugins do 
 chef_run.node['trac']['plugins']
end

before(:each) do
  stub_command('/usr/sbin/apache2 -t').and_return('Syntax OK')
end

it 'execute install pygments for trac' do
  expect(chef_run).to run_execute("easy_install Pygments")
end
%w[ tracjenkinsplugin tracwysiwygplugin 
  tracsvnpoliciesplugin svnauthzadminplugin accountmanagerplugin 
  navaddplugin iniadminplugin breadcrumbsnavplugin tocmacro 
  xmlrpcplugin batchmodifyplugin ticketimportplugin 
  externallinksnewwindowplugin ].each do |plugin|
    it "execute install #{plugin} for trac" do
      expect(chef_run).to run_execute("easy_install -Z https://trac-hacks.org/svn/#{plugin}/#{chef_run.node['trac'][plugin]['version']}")
    end
  end
  it 'creates a template with attributes' do
    expect(chef_run).to create_template("/usr/local/lib/python2.7/dist-packages/TracSVNPoliciesPlugin-0.2-py2.7.egg/svnpolicies/svnpolicy.conf").with(
      source: 'svnpolicy.conf.erb',
      mode:   '0640',
      owner:  trac_user,
      group:  trac_group,
      )
  end
  it 'should create cookbook file' do
    expect(chef_run).to create_cookbook_file("trac-post-commit-hook")
  end
  it 'creates a template with attributes' do
    svn_repo_dir  = chef_run.node['subversion']['repo_dir'] 
    svn_repo_name = chef_run.node['subversion']['repo_name']
    expect(chef_run).to create_template("#{svn_repo_dir}/#{svn_repo_name}/hooks/post-commit").with(
      source: 'post-commit.erb',
      mode:   '0755',
      owner:  trac_user,
      group:  trac_group,
      )
  end
  it 'should execute "set trac-project authentication"' do
    expect(chef_run).to run_execute("trac-admin #{trac_dir} permission remove #{initial_user} TRAC_ADMIN")
  end
  at_exit { ChefSpec::Coverage.report! }
end
