require 'chefspec'
require 'chefspec/berkshelf'

def setup_py_install_ark(resource_name)
  ChefSpec::Matchers::ResourceMatcher.new(:ark, :setup_py_install, resource_name)
end
