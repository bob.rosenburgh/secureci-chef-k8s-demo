#
# Cookbook Name:: trac
# Recipe:: wiki
# Author:: Max Saperstone <max.saperstone@coveros.com>
#
#Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
use_inline_resources 
action :create do
  wiki = [ "AboutSecureCI", "AboutSecureCI%2FDevelopment", "AboutSecureCI%2FProjectManagement", "AboutSecureCI%2FSupport", "AboutWiki", "HowTo", "HowTo%2FAddNewLoginUser", "HowTo%2FBackup", "HowTo%2FCustomizeTickets", "HowTo%2FEnableHttp", "HowTo%2FInstallSslCert", "HowTo%2FOpenPortInFirewall", "HowTo%2FTicketWorkflow", "HowTo%2FTimeZone", "HowTo%2FTracPostCommitHook", "HowTo%2FUseExistingSubversion", "WikiStart" ]

  #setup our wiki
  directory "/tmp/wiki" do
    owner node['trac']['user']
    group node['trac']['group']
    mode 0777
    recursive true
  end
  
  #add our about pages
  if ::File.directory?(node['browser']['firefox']['install']) || ::File.directory?(node['browser']['chrome']['install'])
    wiki.push "AboutSecureCI%2FBrowser"
  end
  if ::File.directory?(node['ratproxy']['base_dir']) || ::File.directory?(node['zap']['home']) || ::File.directory?(node['openscap']['home'])
    wiki.push "AboutSecureCI%2FSecurityScanning"
  end
  anytests = false
  node['tools']['testing'].each do |tool|
    if ::File.directory?("#{node['tools']['base_dir']}/#{tool}-#{node['tools'][tool]['version']}") || ::File.exists?("#{node['tools']['base_dir']}/#{tool}-#{node['tools'][tool]['version']}.jar")
      anytests = true
    end
  end
  if anytests
    wiki.push "AboutSecureCI%2FTesting"
  end
  anyanalysis = false
  node['tools']['analysis'].each do |tool|
    if ::File.directory?("#{node['tools']['base_dir']}/#{tool}-#{node['tools'][tool]['version']}") || ::File.exists?("#{node['tools']['base_dir']}/#{tool}-#{node['tools'][tool]['version']}.jar")
      anyanalysis = true
    end
  end
  if ::File.directory?(node['yasca']['home'])
    anyanalysis = true
  end
  if anyanalysis
    wiki.push "AboutSecureCI%2FCodeAnalysis"
  end
  #add our how to pages
  node['tools']['all'].each do |tool|
    if ::File.directory?("#{node['tools']['base_dir']}/#{tool}-#{node['tools'][tool]['version']}") || ::File.exists?("#{node['tools']['base_dir']}/#{tool}-#{node['tools'][tool]['version']}.jar")
      wiki.push "HowTo%2F#{node['tools'][tool]['description']['howto_link']}"
    end
  end
  if ::File.directory?("#{node['tools']['base_dir']}/pmd-bin-#{node['tools']['pmd-bin']['version']}")
    wiki.push "HowTo%2FAddPMDCPD"
  end
  if ::File.directory?(node['tomcat']['install_dir'])
    wiki.push "HowTo%2FAddTomcatInstance"
  end 
  if ::File.directory?(node['jenkins']['home_dir'])
    wiki.push "HowTo%2FUseJenkins"
  end
  if ::File.directory?(node['nexus']['home'])
    wiki.push "HowTo%2FUseNexus"
  end
  if ::File.directory?(node['ratproxy']['base_dir'])
    wiki.push "HowTo%2FUseRatproxy"
  end
  if ::File.directory?(node['sonar']['home'])
    wiki.push "HowTo%2FUseSonar"
  end
  if ::File.directory?(node['openscap']['home'])
    wiki.push "HowTo%2FUseOpenSCAP"
  end
  if ::File.directory?(node['zap']['home'])
    wiki.push "HowTo%2FUseZAP"
  end
  if ::File.directory?(node['yasca']['home'])
    wiki.push "HowTo%2FUseYASCA"
  end
  if ::File.directory?(node['gitblit']['home'])
    wiki.push "HowTo%2FUseGit"
    wiki.push "HowTo%2FUseGitBlit"
  end
  if ::File.directory?(node['browser']['firefox']['install']) || ::File.directory?(node['browser']['chrome']['install'])
    wiki.push "HowTo%2FUseXvfb"
  end
  if ::File.directory?(node['maven']['m2_home'])
    wiki.push "HowTo%2FStartWithMaven"
  end
  if ::File.directory?(node['secureci-ant']['home'])
    wiki.push "HowTo%2FStartWithAnt"
  end
  
  #deploy our wiki
  wiki.each do |file|
    template "/tmp/wiki/#{file}" do
      source "wiki/#{file}.erb"
      mode '0777'
      owner node['trac']['user']
      group node['trac']['group']
    end
  end
  execute "setup our wiki files" do
    user "root"
    #command "LC_ALL=en_US.#{node['trac']['wiki']['charset'].upcase} trac-admin #{node['trac']['projectdir']} wiki load /tmp/wiki"
    command "trac-admin #{node['trac']['project']['dir']} wiki load /tmp/wiki"
  end
  directory "/tmp/wiki" do
    recursive true
    action :delete
  end
end
