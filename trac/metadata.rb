name             "trac"
maintainer       "Max Saperstone"
maintainer_email "max.saperstone@coveros.com"
license          "All Rights Reserved"
description      "Installs/Configures Trac"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.6.3"

supports 'ubuntu'

depends           "apt"
depends           "aptupdate"
depends           "secureci-git"
depends           "secureci-mysql"
depends           "secureci-perl"
depends           "subversion"
depends           "poise-python"
depends           "openssl"
depends           "sonar"
depends           "nexus"
depends           "java_tools"
depends           "java_oracle"
depends           "initialuser"
depends           "secureci-hostinfo"

#0.6.3 updating unit tests
#0.6.2 adding in tomcat7 dependecy for wiki
#0.6.1 food critic fixes
#0.6.0 rewrote recipe to install trac 1.2
#0.5.4 set dependency to use secureci-ant
#0.5.3 Updating Agilo home link
#0.5.2 Upgrading to trac 1.0.13
#0.5.1 Change plugin download URL to https
#0.5.0 Upgrading to trac 1.0.9
#0.4.11 Updated CSS on wiki-toc
#0.4.10 fixing post-commit for bug in ticket 404
#0.4.9 removing SubversionLocationPlugin as the functionality of this plugin is included in Trac 0.12
#0.4.8 updates from testing - tomcat not used as container for nexus, sonar or jenkins
#0.4.7 upgraded plugin versions
#0.4.6 added wiki pages for oscap
#0.4.5 added wiki pages for security tools - yasca and zap
#0.4.4 added wiki pages for maven
#0.4.3 tied trac install to version specified in attributes
#0.4.2 updated wiki, and images, added favicon
#0.4.1 adding in browser information
#0.4.0 re-worked how agilo is installed to fit better into installation method
#0.3.9 added in trac hacks for opening links in new window
#0.3.8 added in agilo recipe
#0.3.7 added in git to wiki
#0.3.6 added in mysql db attributes
#0.3.5 splitting plugins to separate recipe
#0.3.4 modifying wiki to only added required files
#0.3.3 splitting wiki to separate recipe
#0.3.2 added wiki
#0.3.1 adding in menu items
#0.3.0 cleaning up cookbook
#0.2.2 added trac configurations
#0.2.1 added trac plugins
#0.2.0 default recipe
