= How do I use Selenium to test my web application? =
[[TOC(heading=How do I...?,depth=1,HowTo/*)]]

[http://seleniumhq.org/ Selenium] is a web application testing system. It works by simulating a user interacting with a 
web application through a browser. By using a "real" browser such as Internet Explorer or Firefox, the web application 
behavior is much closer to what a user would experience than if the browser itself were simulated. Selenium WebDriver can also 
run with a minimalistic Mozilla based browser, similar to typical unit tests, called `HTMLUnit`. 

== Selenium IDE ==
Selenium IDE is an add-on for the Firefox (2+) browser, meaning that Firefox is required to record the test case. The IDE 
allows the recording, editing, debugging, and replaying of the test case. To get started, download the 
[http://seleniumhq.org/projects/ide/ Selenium IDE] and follow the simple installation procedures.

=== Record and Playback ===
The Selenium IDE is located under the ''Tools'' menu of the browser and will start the IDE in a separate window. 
Once the window has been started, begin the test case as the IDE records it. End the recording by clicking on the red 
dot icon in the IDE window. To save the test case, choose the ''Save Test Case'' option under the ''Files'' menu. 
Choosing one of the playback options allows the test case back to be replayed in the currently selected browser.

The use of the base URL field in the IDE allows the playback of a test case against different instances of the 
applications, such as integration and test. This only works when the test case is built with relative paths, 
the default for IDE recording.

=== Test Suites ===
The IDE allows the creation of a test suite or group of test cases, by selecting the ''New Test Suite'' option under the 
''File'' menu. The saved test cases are added to the test suite by either right clicking in the test suite's test case 
box and selecting ''Add Test Case...'' or by selecting ''Add Test Case'' under the ''File'' menu. The test suite can be 
saved by selecting ''Save Test Suite'' under the ''File'' menu.

The test suite can be run as a whole or a test case can be selected for running from the test suite. The log in the 
lower portion of the IDE records the results of each step of the test case.

== Continuous integration ==
One way of integrating Selenium into SecureCI™ is to export the test cases and test suites as Java classes and 
set them up to run under JUnit. It requires the use of 
[http://www.seleniumhq.org/docs/03_webdriver.jsp Selenium WebDriver] which is installed on the VM.

=== Exporting as Java classes ===
This can be accomplished by the ''Export Test Case As...'' and ''Export Test Suite As...'' under the ''File'' menu of
the IDE. These classes can be used in your project's JUnit tests.

=== Building Selenium Test Java Classes ===
Using the [http://www.seleniumhq.org/docs/03_webdriver.jsp#selenium-webdriver-api-commands-and-operations Selenium Java API], 
new test cases and suites can be update and/or built by hand. The advantage of this is the ability to add greater programmatic control 
such as loops and branching. The iteration within a test case provides great flexibility to verify a repeating 
field on the page.

== For Maven Projects ==
The easiest way to set up a Selenium 2.0 Java project is to use Maven. Maven will download the java bindings 
(the Selenium 2.0 java client library) and all its dependencies, and will create the project for you, using a 
maven pom.xml (project configuration) file. Once you’ve done this, you can import the maven project into your 
preferred IDE, IntelliJ IDEA or Eclipse.

First, create a folder to contain your Selenium project files. Then, to use Maven, you need a pom.xml file. 
This can be created with a text editor. We won’t teach the details of pom.xml files or for using Maven since 
there are already excellent references on this. Your pom.xml file will look something like this. Create this 
file in the folder you created for your project.
{{{
#!text/xml
	<?xml version="1.0" encoding="UTF-8"?>
	<project xmlns="http://maven.apache.org/POM/4.0.0"
	                 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	                 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	        <modelVersion>4.0.0</modelVersion>
	        <groupId>MySel20Proj</groupId>
	        <artifactId>MySel20Proj</artifactId>
	        <version>1.0</version>
	        <dependencies>
	            <dependency>
	                <groupId>org.seleniumhq.selenium</groupId>
	                <artifactId>selenium-java</artifactId>
	                <version>2.44.0</version>
	            </dependency>
	            <dependency>
	                <groupId>com.opera</groupId>
	                <artifactId>operadriver</artifactId>
	            </dependency>
	        </dependencies>
	        <dependencyManagement>
	            <dependencies>
	                <dependency>
	                    <groupId>com.opera</groupId>
	                    <artifactId>operadriver</artifactId>
	                    <version>1.5</version>
	                    <exclusions>
	                        <exclusion>
	                            <groupId>org.seleniumhq.selenium</groupId>
	                            <artifactId>selenium-remote-driver</artifactId>
	                        </exclusion>
	                    </exclusions>
	                </dependency>
	            </dependencies>
	        </dependencyManagement>
	</project>
}}}
Using the [http://seleniumhq.org/docs/05_selenium_rc.html#learning-the-api Selenium Java API], new test cases and 
suites can be update and/or built by hand. The advantage of this is the ability to add greater programmatic control 
such as loops and branching. The iteration within a test case provides great flexibility to verify a repeating 
field on the page.

== For Ant builds ==
The following targets should to be added to the JUnit task in the Ant script to compile your tests, build junt report, 
and publish results. You can then tie results to continuous integration server to automate tests and keep junit report logs.  

Add the following targets to your `build.xml` file. `build.dir` should be set appropriately. Then simply call the target
`run_all_tests` to execute your tests and view the report. You need to ensure that a compile target exists for building your
tests.

{{{
#!text/xml
	<target name="run_tests"> 
		<junit printsummary="yes" fork="yes" errorproperty="JUnit.error" failureproperty="JUnit.error"> 
			<classpath refid="JUnit.class.path" /> 
			<formatter type="xml"/> 
			<test todir="${testresults}" name="com.selenium.testPackage.testClassName"/> 
		</junit> 
		<antcall target="report-JUnit"/> 
		<fail if="JUnit.error" message="Selenium test(s) failed. See reports!"/> 
	</target> 

	<target name="report-JUnit"> 
		<junitreport todir="${testreport}"> 
			<fileset dir="${testresults}"> 
				<include name="TEST-*.xml"/> 
			</fileset> 
			<report format="frames" todir="${testreport}"/> 
		</junitreport> 
	</target> 

	<target name="run_all_tests" depends="compile" description="The Main target for running all tests"> 
		<antcall target="run_tests"></antcall>	
	</target>
}}}

Once the JUnit tests run, the Selenium results are recorded and reported as JUnit reports.

=== More information ===

 * [http://seleniumhq.org/projects/ Selenium projects]

<%= node['trac']['wiki']['footer'] %>