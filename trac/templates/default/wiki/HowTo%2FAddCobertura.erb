= How do I add Cobertura code coverage to my project? =
[[TOC(heading=How do I...?,depth=1,HowTo/*)]]

[http://cobertura.sourceforge.net/ Cobertura] is a free Java tool that calculates the percentage of code accessed by 
tests. It can be used to identify which parts of your Java program are lacking test coverage.

== For Maven projects ==
To use Cobertura in a Maven project, add the following lines to your `pom.xml` file:

{{{
#!text/xml
<project>
  ...  
  <reporting>
    <plugins>
      ...
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>cobertura-maven-plugin</artifactId>
      </plugin>
    </plugins>
  </reporting>
</project>
}}}

Cobertura will report its findings as either HTML or XML, the default is HTML.

== For Ant builds ==
To use Cobertura in an Ant project, add the following files to a directory in your source tree 
(`tools/cobertura-<%= node['tools']['cobertura']['version'] %>`):

 * `cobertura.jar`
 * `lib/`
   * `asm-3.0.jar`
   * `asm-tree-3.0.jar`
   * `jakarta-oro-2.0.8.jar`
   * `log4j-1.2.9.jar`

If you prefer to keep the Cobertura libraries outside your source tree, change the locations for the `tools.dir` and/or 
the `cobertura.lib.dir` properties in the lines below.  

Add the following lines to your `build.xml` file. `build.dir`, `classes.dir`, `compile.classpath`, `junit.lib.dir`, 
`src.dir`, and `test.src.dir` should be set appropriately.

{{{
#!text/xml
  <property name="tools.dir" location="tools"/>
  <property name="cobertura.dir" location="${tools.dir}/cobertura-<%= node['tools']['cobertura']['version'] %>"/>
	<property name="cobertura.lib.dir" location="${cobertura.dir}/lib"/>
	
	<property name="report.dir" location="${build.dir}/reports"/>
  <property name="cobertura.report.dir" location="${report.dir}/cobertura"/>

  <path id="test.classpath">
    <path refid="compile.classpath"/>
    <fileset dir="${junit.lib.dir}" includes="**/*.jar"/>
  </path>
  <path id="cobertura.classpath">
    <fileset dir="${cobertura.dir}">
      <include name="cobertura.jar"/>
      <include name="lib/**/*.jar"/>
    </fileset>
  </path>

  <taskdef classpathref="cobertura.classpath" resource="tasks.properties"/>

	<target name="coverage" depends="compile-tests" description="Runs JUnit tests and measures code coverage"/>
    <mkdir dir="${cobertura.report.dir}"/>
    <property name="cobertura.report.file" location="${cobertura.report.dir}/cobertura.ser"/>
    <delete file="${cobertura.report.file}" quiet="true"/>

    <property name="cobertura.build.dir" location="${build.dir}/cobertura"/>
    <delete dir="${cobertura.build.dir}" quiet="true"/>
    <mkdir dir="${cobertura.build.dir}"/>
    <cobertura-instrument todir="${cobertura.build.dir}" datafile="${cobertura.report.file}">
      <fileset dir="${classes.dir}">
        <include name="**/*.class" />
        <exclude name="**/*Test.class" />
      </fileset>
    </cobertura-instrument>

    <mkdir dir="${cobertura.report.dir}"/>
    <junit fork="yes" forkmode="once" printsummary="true" failureProperty="test.failed">
      <sysproperty key="net.sourceforge.cobertura.datafile" file="${cobertura.report.file}" />
      <classpath location="${cobertura.build.dir}"/>
      <classpath refid="test.classpath"/>
      <classpath refid="cobertura.classpath"/>
      <formatter type="xml"/>
      <batchtest fork="true" todir="${cobertura.report.dir}">
        <fileset dir="${test.src.dir}">
          <include name="**/*Test.java"/>
          <exclude name="**/AllTests.java"/>
        </fileset>
      </batchtest>
    </junit>

    <cobertura-report format="xml" datafile="${cobertura.report.file}" destdir="${cobertura.report.dir}" srcdir="${src.dir}" />
    <property name="cobertura.report.html" location="${cobertura.report.dir}/index.html"/>
    <cobertura-report format="html" datafile="${cobertura.report.file}" destdir="${cobertura.report.dir}" srcdir="${src.dir}" />
    <echo message="Coverage report is at ${cobertura.report.html}"/>
  </target>
}}}

== Validation of Coverage ==

Validation of the coverage of the test against the code base is available in both Ant and Maven. This validation will
cause the build to fail if the code coverage is not at the specified levels.

For Ant, the <cobertura-check> task runs the validation of the test case coverage:

{{{
#!text/xml
<junit fork="yes" dir="${basedir}" failureProperty="test.failed">

	...
	
	<cobertura-check branchrate="80" linerate="80">
    <regex pattern="com.example.gui.*" branchrate="85" linerate="90"/>
    <regex pattern="com.example.tool.*" branchrate="55" linerate="80"/>
    <regex pattern="com.example.util.*" branchrate="85" linerate="95"/>
	</cobertura-check>
</junit>

}}}

The Maven plugin can check the post-testing coverage percentages to ensure a valid level of test coverage.

{{{
<project>
  ...
  <build>
    ...
    <plugins>
      ...
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>cobertura-maven-plugin</artifactId>
        <configuration>
          <check>
            <branchRate>85</branchRate>
            <lineRate>85</lineRate>
            <haltOnFailure>true</haltOnFailure>
            <totalBranchRate>85</totalBranchRate>
            <totalLineRate>85</totalLineRate>
            <packageLineRate>85</packageLineRate>
            <packageBranchRate>85</packageBranchRate>
            <regexes>
              <regex>
                <pattern>com.example.reallyimportant.*</pattern>
                <branchRate>90</branchRate>
                <lineRate>80</lineRate>
              </regex>
              <regex>
                <pattern>com.example.boringcode.*</pattern>
                <branchRate>40</branchRate>
                <lineRate>30</lineRate>
              </regex>
            </regexes>
          </check>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>clean</goal>
              <goal>check</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</project>
}}}

=== More information ===

 * [http://mojo.codehaus.org/cobertura-maven-plugin/ Maven options]
 * [http://cobertura.sourceforge.net/introduction.html Ant options]
 
<%= node['trac']['wiki']['footer'] %>