#
# Cookbook Name:: trac
# Recipe:: default
# Author:: Max Saperstone <max.saperstone@coveros.com>
#
#Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe "apt"
include_recipe "aptupdate"
include_recipe "subversion::secureci"
include_recipe "apache2::mod_rewrite"
include_recipe "apache2::mod_fcgid"
include_recipe "apache2::mod_deflate"
include_recipe "secureci-apache2"
include_recipe "secureci-perl"
include_recipe "secureci-mysql"
include_recipe "openssl"
include_recipe "initialuser"


package "libmysqlclient-dev" do
  action :install
end

python_package 'mysql-python' do
end

#ensure our trac user had access to svn
group node['subversion']['group'] do
  action :modify
  members node['trac']['user']
  append true
end

directory node['trac']['base'] do
  owner node['trac']['user']
  group node['trac']['group']
  recursive true
  mode 00755
end

#install trac
ark "trac" do
  url node['trac']['url']
  version node['trac']['version']
  prefix_root node['trac']['base']
  home_dir node['trac']['home_link']
  owner node['trac']['user']
  group node['trac']['group']
  action :setup_py_install
end

template "create-trac-db.sql" do
  path File.join('/root/','create-trac-db.sql')
  action :create
end

#set up track db
execute "secureci trac db" do
  command "mysql -S #{node['mysql']['socket']} --user=root --password=#{node['mysql']['server_root_password']} < /root/create-trac-db.sql"
end

file "create-trac-db.sql" do
  path File.join('/root/','create-trac-db.sql')
  action :delete
end



#make soft link from trac home
link node['trac']['home_link'] do
  action :delete
  only_if "test -L #{node['trac']['home_link']}"
end
link node['trac']['home_link'] do
  to node['trac']['home']
  action :create
end

execute "initialize trac-project" do
  command "trac-admin #{node['trac']['project']['dir']} initenv '#{node['trac']['project']['title']}' 'mysql://#{node['trac']['database']['user']}:#{node['trac']['database']['password']}@#{node['trac']['database']['host']}/#{node['trac']['database']['name']}' 'svn' '#{node['subversion']['repo_dir']}/#{node['subversion']['repo_name']}'"
  not_if { ::File.directory?(node['trac']['project']['dir']) }
end

#setup our site.html template
directory "#{node['trac']['project']['dir']}/templates" do
  owner node['trac']['user']
  group node['trac']['group']
  recursive true
end
cookbook_file "site.html" do
  path File.join(node['trac']['project']['dir'], "templates", "site.html")
  action :create
end

#setup our secureci look and feel
directory "#{node['trac']['project']['dir']}/htdocs/images" do
  owner node['trac']['user']
  group node['trac']['group']
  recursive true
end
cookbook_file "secureci.css" do
  path File.join(node['trac']['project']['dir'], "htdocs", "secureci.css")
  action :create
end
%w[ cc-80x15.png coveros-sm.png secureci.png favicon.ico ].each do |file|
  cookbook_file file do
    path File.join(node['trac']['project']['dir'], "htdocs", "images",file)
    action :create
  end
end

#fix some permissions
execute "trac-file-change" do
  command "find #{node['trac']['home']} -type f -exec chmod 0660 {} \\;"
end
execute "trac-directory-change" do
  command "find #{node['trac']['home']} -type d -exec chmod 2770 {} \\;"
end
execute "trac-owner-change" do
  command "chown -Rf #{node['trac']['user']}.#{node['trac']['group']} #{node['trac']['home']}"
end
execute "trac-passwd-change" do
  command "chown -Rf #{node['trac']['user']}.#{node['trac']['group']} #{node['subversion']['password_dir']}"
end

#setup some simple authentication
execute "set trac-project authentication" do
  user "root"
  command "trac-admin #{node['trac']['project']['dir']} permission add #{node['initial_user']} TRAC_ADMIN"
end

include_recipe "trac::plugins"

#setup our trac.ini file
template "#{node['trac']['project']['dir']}/conf/trac.ini" do
  source "trac.ini.erb"
  mode '0660'
  owner node['trac']['user']
  group node['trac']['group']
end

#upgrade our trac instance
execute "upgrade trac" do
  command "trac-admin #{node['trac']['project']['dir']} upgrade"
end
execute "upgrade trac wiki" do
  command "trac-admin #{node['trac']['project']['dir']} wiki upgrade"
end

include_recipe "trac::wiki"

# help keep the repo in sync
cron "trac-sync" do
  minute "0"
  command "trac-admin #{node['trac']['project']['dir']} repository resync '*'"
  user node['trac']['user']
  only_if do ::File.exists?(node['trac']['project']['dir']) end
end

#install agilo
execute "install agilo from source" do
  user "root"
  cwd "#{node['agilo']['home']}/"
  command "python setup.py install"
  action :run
  only_if { ::File.directory?(node['agilo']['home']) }
end



#remove authentication from default user
execute "set trac-project authentication" do
  user "root"
  command "trac-admin #{node['trac']['project']['dir']} permission remove #{node['initial_user']} TRAC_ADMIN"
end
