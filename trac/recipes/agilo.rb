#
# Cookbook Name:: trac
# Recipe:: agilo
# Author:: Max Saperstone <max.saperstone@coveros.com>
#
#Copyright 2015, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#copy over source
cookbook_file node['agilo']['tar'] do
  path "#{node['agilo']['base']}/#{node['agilo']['tar']}"
  action :create
  not_if { ::File.directory?(node['agilo']['home']) }
end

#untar source
execute "untar agilo source" do
  user "root"
  cwd "#{node['agilo']['base']}/"
  command "tar xvfz #{node['agilo']['tar']}"
  action :run
  not_if { ::File.directory?(node['agilo']['home']) }
end

#make soft link from agilo version to agilo
link node['agilo']['link'] do
  action :delete
  only_if "test -L #{node['agilo']['link']}"
end
link node['agilo']['link'] do
  to node['agilo']['home']
  action :create
end

#remove source folder
execute "remove tar file and directory" do
  user "root"
  cwd "#{node['agilo']['base']}/"
  command "rm -rf #{node['agilo']['tar']}"
  action :run
end
