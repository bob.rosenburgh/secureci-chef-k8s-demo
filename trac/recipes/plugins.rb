#
# Cookbook Name:: trac
# Recipe:: plugins
# Author:: Max Saperstone <max.saperstone@coveros.com>
#
#Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#install our plugins
execute "install pygments for trac" do
  command "easy_install Pygments"
end

#TODO
# find out why easy_install fails on jenkins track plugin
#install jenkinsapi plugin with pip so easy_install doesn't 
execute "install jenkinsapi for trac" do
  command "pip install jenkinsapi"
end

for plugin in node['trac']['plugins'] do
  execute "install #{plugin} for trac" do
    command "easy_install -Z https://trac-hacks.org/svn/#{plugin}/#{node['trac'][plugin]['version']}"
  end
end
#configure our SVNPoliciesPlugin
template "/usr/local/lib/python2.7/dist-packages/TracSVNPoliciesPlugin-0.2-py2.7.egg/svnpolicies/svnpolicy.conf" do
  source "svnpolicy.conf.erb"
  mode '0640'
  owner node['trac']['user']
  group node['trac']['group']
end

#setup our trac.ini file
template "#{node['trac']['project']['dir']}/conf/trac.ini" do
  source "trac.ini.erb"
  mode '0660'
  owner node['trac']['user']
  group node['trac']['group']
end

#Integrate Trac tickets with Subversion commits
cookbook_file "trac-post-commit-hook" do
  path File.join(node['trac']['home'], "contrib","trac-post-commit-hook")
  action :create
end
template "#{node['subversion']['repo_dir']}/#{node['subversion']['repo_name']}/hooks/post-commit" do
  source "post-commit.erb"
  mode '0755'
  owner node['trac']['user']
  group node['trac']['group']
end
svn_db = "#{node['trac']['user']}.#{node['trac']['group']} #{node['subversion']['repo_dir']}/#{node['subversion']['repo_name']}/db/rep-cache.db"
execute "db-owner-change" do
  command "chown -Rf #{svn_db}"
  only_if { ::File.directory?(svn_db) }
end
