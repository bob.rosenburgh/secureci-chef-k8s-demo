name             'secureci-maven'
maintainer       'Max Saperstone'
maintainer_email 'max.saperstone@coveros.com'
license          'All rights reserved'
description      'Installs/Configures secureci-maven'
long_description 'Installs/Configures secureci-maven'
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)
version          '0.2.0'

supports 'ubuntu'

depends 'apt'
depends 'maven'
depends 'java_oracle'
