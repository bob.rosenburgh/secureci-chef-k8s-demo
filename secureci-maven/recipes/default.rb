#
# Cookbook Name:: secureci-maven
# Recipe:: default
#
# Copyright (C) 2016 Max Saperstone
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'apt'
include_recipe 'java_oracle'
include_recipe 'maven'
