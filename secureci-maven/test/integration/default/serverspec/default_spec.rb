require 'spec_helper'

def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

describe 'maven' do

home = node['default']['maven']['m2_home']
version = node['default']['maven']['version']

describe file(home) do
  it { should exist }
  it { should be_directory }
 end
describe command("#{home}/bin/mvn --version") do
  its (:stdout) { should contain("#{version}") }
 end
end
