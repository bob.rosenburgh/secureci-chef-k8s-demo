require 'spec_helper'

describe 'secureci-maven::default' do

let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04')
 end 

it 'includes cookbook maven' do
    chef_run.converge(described_recipe)
    expect(chef_run).to include_recipe('maven')
  end
at_exit { ChefSpec::Coverage.report! }
end
