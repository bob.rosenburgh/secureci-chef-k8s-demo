name             'docker'
maintainer       'Coveros'
maintainer_email 'glenn.buckholz@coveros.com'
license          'All rights reserved'
description      'Installs/Configures docker'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

supports 'ubuntu'

