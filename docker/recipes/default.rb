#
# Cookbook Name:: docker
# Recipe:: default
#
# Copyright 2017, Coveros
#
# All rights reserved - Do Not Redistribute
#

package "docker" do
  action :install
end



package "docker.io" do
  action :install
end

template "/etc/default/docker" do
  owner 'root'
  group 'root'
  mode '0644'
  source 'docker.erb'
  notifies :restart, "service[docker]", :delayed
end

# TODO: /etc/docker not found
# execute "Enable Insecure Registry in Docker" do
#   IO.write("/etc/docker/daemon.json", { 'insecure-registries' => ['52.203.157.75:32002', 'sonatype-nexus-service:8084'] }.to_json)
# end

service "docker" do
  action [ :start, :enable ]
end
