default['tools']['user'] = 'www-data'
default['tools']['group'] = 'www-data'

default['tools']['base_dir'] = '/usr/java'

#file lists
default['tools']['jars'] = [ "checkstyle", "selenium-server", "junit", "testng" ]
default['tools']['zips'] = [ "jdepend", "pmd-bin", "selenium", "secureci-testing-framework", "findbugs" ]
default['tools']['tars'] = [ "cobertura" ]

#This URL sometimes changes and it is present in a few places; this might not be super helpful though if the rest of the URL changes format also.
default['tools']['sf']['url'] = "https://sourceforge.net/projects"

#group our tools into lists, so that they are listed in our wiki
default['tools']['all'] = [ "checkstyle", "findbugs", "jdepend", "pmd-bin", "cobertura", "selenium-server", "junit", "selenium", "secureci-testing-framework", "testng" ]
default['tools']['testing'] = [ "selenium-server", "junit", "selenium", "secureci-testing-framework", "testng" ]
default['tools']['analysis'] = [ "checkstyle", "findbugs", "jdepend", "pmd-bin", "cobertura" ]

#checkstyle
default['tools']['checkstyle']['version'] = "7.2"
#default['tools']['checkstyle']['url'] = "http://downloads.sourceforge.net/project/checkstyle/checkstyle/#{node['tools']['checkstyle']['version']}/checkstyle-#{node['tools']['checkstyle']['version']}-all.jar"
#default['tools']['checkstyle']['url'] = "http://iweb.dl.sourceforge.net/project/checkstyle/checkstyle/#{node['tools']['checkstyle']['version']}/checkstyle-#{node['tools']['checkstyle']['version']}-all.jar"
default['tools']['checkstyle']['url'] = "http://repo1.maven.org/maven2/com/puppycrawl/tools/checkstyle/#{node['tools']['checkstyle']['version']}/checkstyle-#{node['tools']['checkstyle']['version']}.jar"
#default['tools']['checkstyle']['url'] = "#{node['tools']['sf']['url']}/checkstyle/files/checkstyle/#{node['tools']['checkstyle']['version']}/checkstyle-#{node['tools']['checkstyle']['version']}-all.jar"
default['tools']['checkstyle']['name'] = "Checkstyle"
default['tools']['checkstyle']['name_friendly'] = node['tools']['checkstyle']['name']
default['tools']['checkstyle']['description']['link'] = "http://checkstyle.sourceforge.net/" 
default['tools']['checkstyle']['description']['howto_link'] = "AddCheckstyle" 
default['tools']['checkstyle']['description']['howto_text'] = "add Checkstyle analysis to my project?" 
default['tools']['checkstyle']['description']['short'] = "Development tool to help programmers write Java code that adheres to a coding standard"
default['tools']['checkstyle']['description']['long'] = "runs against Java source files, and focuses on stylistic coding practices. It is often used to fail a build in order to enforce a team's coding standards."

#selenium-server
default['tools']['selenium-server']['version_major'] = "2.53"
default['tools']['selenium-server']['version_minor'] = "2.53.0"
default['tools']['selenium-server']['version'] = node['tools']['selenium-server']['version_minor']
default['tools']['selenium-server']['url'] = "http://selenium-release.storage.googleapis.com/#{node['tools']['selenium-server']['version_major']}/selenium-server-standalone-#{node['tools']['selenium-server']['version_minor']}.jar"
default['tools']['selenium-server']['name'] = "Selenium Server"
default['tools']['selenium-server']['name_friendly'] = node['tools']['selenium-server']['name']
default['tools']['selenium-server']['description']['howto_link'] = "UseSeleniumServer" 
default['tools']['selenium-server']['description']['howto_text'] = "use Selenium Server to execute my Selenium tests for my project?" 
default['tools']['selenium-server']['description']['short'] = "Is needed in order to run either Selenium RC style scripts or Remote Selenium !WebDriver ones"
default['tools']['selenium-server']['description']['long'] = "The Selenium Server is needed in order to run either Selenium RC style scripts or Remote Selenium !WebDriver ones. The 2.x server is a drop-in replacement for the old Selenium RC server and is designed to be backwards compatible with your existing infrastructure."

#junit
default['tools']['junit']['version'] = "4.12"
default['tools']['junit']['url'] = "https://github.com/junit-team/junit/releases/download/r#{node['tools']['junit']['version']}/junit-#{node['tools']['junit']['version']}.jar"
default['tools']['junit']['url'] = "https://github.com/junit-team/junit/releases/download/r#{node['tools']['junit']['version']}/junit-#{node['tools']['junit']['version']}.jar"
default['tools']['junit']['name'] = "JUnit"
default['tools']['junit']['name_friendly'] = node['tools']['junit']['name']
default['tools']['junit']['description']['howto_link'] = "AddJUnit" 
default['tools']['junit']['description']['howto_text'] = "add JUnit test to my project?" 
default['tools']['junit']['description']['short'] = "A simple framework to write repeatable tests"
default['tools']['junit']['description']['long'] = "One of the core practices in Continuous Integration is to 'make your build self-testing'. As code is written, tests are written as well. The goal is not just to prove that the code works, but also to document (in code) the behavior of a method or section of code. This is important both for a new developer trying to learn how to use the code and for existing developers refactoring and/or changing other code behaviors.

If I make a code change that causes a previously passing test to start failing, I know that I have changed the behavior of the code. If that change is expected or desirable, I can change the test to document the new behavior. I would also then need to make sure that other code is not relying on the previous behavior, or else that code needs to be changed as well. On the other hand, if the change in behavior was unwanted, I can back out my code changes and go about my implementation another way.

The definition of 'unit tests' can be restrictive, but generally refers to testing independent units of code (individual methods, for example). [http://www.junit.org/ JUnit] is a popular framework for unit testing Java code, and can also be used for testing larger blocks of code representing discrete functionality (single features, that more pedantically might be considered 'functional tests' or 'integration tests')."

#findbugs
default['tools']['findbugs']['version'] = "3.0.1"
default['tools']['findbugs']['url'] = "#{node['tools']['sf']['url']}/findbugs/files/findbugs/#{node['tools']['findbugs']['version']}/findbugs-#{node['tools']['findbugs']['version']}.zip"
default['tools']['findbugs']['name'] = "FindBugs"
default['tools']['findbugs']['name_friendly'] = "!FindBugs"
default['tools']['findbugs']['description']['link'] = "http://findbugs.sourceforge.net/"
default['tools']['findbugs']['description']['howto_link'] = "AddFindBugs" 
default['tools']['findbugs']['description']['howto_text'] = "add FindBugs analysis to my project?" 
default['tools']['findbugs']['description']['short'] = "A program which uses static analysis to look for bugs in Java code"
default['tools']['findbugs']['description']['long'] = "runs against compiled Java class files. It can find bad practices (e.g., unclosed streams, misuse of equals()/==) , null pointer dereferences, static use of non-thread safe code (e.g., Calendar or !DateFormat), and security issues (e.g., possible SQL injection or HTTP response splitting)."

#jdepend
default['tools']['jdepend']['version'] = "2.9.1"
default['tools']['jdepend']['url'] = "http://www.clarkware.com/software/jdepend-#{node['tools']['jdepend']['version']}.zip"
default['tools']['jdepend']['name'] = "JDepend"
default['tools']['jdepend']['name_friendly'] = node['tools']['jdepend']['name']
default['tools']['jdepend']['description']['link'] = "http://clarkware.com/software/JDepend.html"
default['tools']['jdepend']['description']['howto_link'] = "AddJDepend" 
default['tools']['jdepend']['description']['howto_text'] = "add JDepend analysis to my project?" 
default['tools']['jdepend']['description']['short'] = "Traverses Java class file directories and generates design quality metrics for each Java package"
default['tools']['jdepend']['description']['long'] = "analyzes Java class files to generates design quality metrics for each Java package, in terms of its extensibility, reusability, and maintainability."

#pmd
default['tools']['pmd-bin']['version'] = "5.5.2"
default['tools']['pmd-bin']['url'] = "#{node['tools']['sf']['url']}/pmd/files/pmd/#{node['tools']['pmd-bin']['version']}/pmd-bin-#{node['tools']['pmd-bin']['version']}.zip"
default['tools']['pmd-bin']['name'] = "PMD"
default['tools']['pmd-bin']['name_friendly'] = node['tools']['pmd-bin']['name']
default['tools']['pmd-bin']['description']['link'] = "http://pmd.sourceforge.net/"
default['tools']['pmd-bin']['description']['howto_link'] = "AddPMD" 
default['tools']['pmd-bin']['description']['howto_text'] = "add PMD analysis to my project?" 
default['tools']['pmd-bin']['description']['short'] = "Finds common programming flaws"
default['tools']['pmd-bin']['description']['long'] = "runs against Java source files. It can find dead code (e.g., assignments to variables that are never subsequently read), performance issues (e.g., misuse of + in !StringBuffer assignments, instantiating new objects in loops), style issues (e.g., final static variables should be all caps, unused import statements), and potentially dangerous code practices (e.g., assignments from instance to static field). There is a good deal of overlap with !FindBugs, but not so much that using both is redundant.

Part of PMD is the PMD CPD (copy/paste detector) that can be used to search for duplicate code in Java, JSP, C, C++, Fortran, or PHP source code files. Such code might be suitable for refactoring or abstracting in order to reduce the amount of code that must be maintained or the number of places a single change must be made. CPD can ignore differences in literals (e.g., same method with different hard-coded constants) and differences in identifiers (e.g., same method but different variable names). It can also be configured to ignore duplicated blocks of less than a certain size.

 * [wiki:HowTo/AddPMDCPD How do I add PMD CPD duplicate code detection to my project?]"

#selenium
default['tools']['selenium']['version']['major'] = "2.53"
default['tools']['selenium']['version']['minor'] = "2.53.1"
default['tools']['selenium']['url'] = "http://selenium-release.storage.googleapis.com/#{node['tools']['selenium']['version']['major']}/selenium-java-#{node['tools']['selenium']['version']['minor']}.zip"
default['tools']['selenium']['version'] = node['tools']['selenium']['version']['minor']
default['tools']['selenium']['name'] = "Selenium (Java Client)"
default['tools']['selenium']['name_friendly'] = node['tools']['selenium']['name']
default['tools']['selenium']['description']['howto_link'] = "UseSelenium" 
default['tools']['selenium']['description']['howto_text'] = "add Selenium tests to my project?" 
default['tools']['selenium']['description']['short'] = "A java client driver to interact with the Selenium Server or create local Selenium !WebDriver scripts"
default['tools']['selenium']['description']['long'] = "Functional testing means verifying that the application behaves as it is expected to and that the functionality is present and appropriate. It is a much higher level of testing than unit testing. 

[http://seleniumhq.org/ Selenium] is a web application testing system. It works by simulating a user interacting with a web application through a browser. By using a 'real' browser such as Internet Explorer or Firefox, the web application behavior is much closer to what a user would experience than if the browser itself were simulated.

Selenium has a tool (Selenium IDE) to record tests through a browser as well as scripting APIs (Selenium RC) through various programming languages (Java, C#, Python, Ruby, Groovy, and PHP).

A project could use a combination of Selenium and [../SecurityScanning#ratproxy ratproxy] to incorporate security scanning into the continuous integration process. 

 * [http://seleniumhq.org/download/ Selenium IDE add-on for Firefox]"

#selenium-testing-framework
default['tools']['secureci-testing-framework']['version'] = "1.3.0"
default['tools']['secureci-testing-framework']['url'] = "https://s3.amazonaws.com/SecureCI_Tools/secureci-testing-framework-#{node['tools']['secureci-testing-framework']['version']}.zip"
default['tools']['secureci-testing-framework']['name'] = "SecureCI Testing Framework"
default['tools']['secureci-testing-framework']['name_friendly'] = node['tools']['secureci-testing-framework']['name']
default['tools']['secureci-testing-framework']['description']['howto_link'] = "UseTestFramework" 
default['tools']['secureci-testing-framework']['description']['howto_text'] = "use SecureCI Testing Framework for testing on my project?" 
default['tools']['secureci-testing-framework']['description']['short'] = "A java testing framework for unit or functional testing"
#default['tools']['secureci-testing-framework']['description']['long'] = "The SecureCI Testing Framework is a custom developed testing framework. I wrote this framework to build upon Selenium’s tools, specifically to provide more error handling capabilities and custom output reporting. While the SecureCI Testing Framework is both designed and optimized for Selenium WebDriver, it can be run for any type of tests, including used for unit tests. An entire testing framework, including custom reporting metrics, is built on top of the basic TestNG framework. All of the Selenium functionality is wrapped, providing fallback capabilities, so that if an element is missing, or a check is performed that fails, the test do not crash, they continue forward, logging the error. All Selenium calls are automatically documented, and screenshots are taken anytime an action or check is performed. Setup was designed to be simple and quick, allowing more concentration on creating tests, and less worry about configuring. By simply not including the setup for Selenium Tests, a browser will not be launched, and any Java code can be executed within a Unit Framework."
default['tools']['secureci-testing-framework']['description']['long'] = "TBD"
default['tools']['secureci-testing-framework']['description']['long'] = "The SecureCI Testing Framework is a custom developed testing framework. This framework was written to build upon Selenium's tools, specifically to provide more error handling capabilities and custom output reporting. While the SecureCI Testing Framework is both designed and optimized for Selenium !WedDriver, it can be run for any type of tests, including unit tests. An ensure testing framework, including custom reporting metrics, is built on top of the basic TestNG framework. All of the Selenium functionality is wrapped, providing fallback capabilities, so that if an element is missing, or a check is performed that fails, the tests do not crash, they continue forward, logging the error. All Selenium calls are automatically documented, and screenshots are taken anytime an action or check is performed. Setup was designed to be simple and quick, allowing more concentration on creating tests, and less worry about configuring. By simply not including the setup for Selenium Tests, a browser will not be launched, and any Java code can be executed within a Unit Framework."

#testng
default['tools']['testng']['version'] = "6.9.13.6"
default['tools']['testng']['url'] = "http://central.maven.org/maven2/org/testng/testng/6.9.13.6/testng-#{node['tools']['testng']['version']}.jar"
default['tools']['testng']['name'] = "TestNG"
default['tools']['testng']['name_friendly'] = node['tools']['testng']['name']
default['tools']['testng']['description']['howto_link'] = "AddTestNG" 
default['tools']['testng']['description']['howto_text'] = "add TestNG tests to my project?" 
default['tools']['testng']['description']['short'] = "A testing framework inspired from JUnit and NUnit"
default['tools']['testng']['description']['long'] = "TestNG is a testing framework inspired from JUnit and NUnit but introducing some new functionalities that make it more powerful and easier to use, such as:
 * Annotations.
 * Run your tests in arbitrarily big thread pools with various policies available (all methods in their own thread, one thread per test class, etc...).
 * Test that your code is multithread safe.
 * Flexible test configuration.
 * Support for data-driven testing (with @!DataProvider).
 * Support for parameters.
 * Powerful execution model (no more !TestSuite).
 * Supported by a variety of tools and plug-ins (Eclipse, IDEA, Maven, etc...).
 * Embeds !BeanShell for further flexibility.
 * Default JDK functions for runtime and logging (no dependencies).
 * Dependent methods for application server testing.
TestNG is designed to cover all categories of tests:  unit, functional, end-to-end, integration, etc..."

#cobertura
default['tools']['cobertura']['version'] = "2.1.1"
default['tools']['cobertura']['url'] = "#{node['tools']['sf']['url']}/cobertura/files/cobertura/#{node['tools']['cobertura']['version']}/cobertura-#{node['tools']['cobertura']['version']}-bin.tar.gz"
default['tools']['cobertura']['name'] = "Cobertura"
default['tools']['cobertura']['name_friendly'] = node['tools']['cobertura']['name']
default['tools']['cobertura']['description']['howto_link'] = "AddCobertura" 
default['tools']['cobertura']['description']['howto_text'] = "add Cobertura analysis to my project?" 
default['tools']['cobertura']['description']['link'] = "http://cobertura.sourceforge.net/" 
default['tools']['cobertura']['description']['short'] = "A free Java tool that calculates the percentage of code accessed by tests"
default['tools']['cobertura']['description']['long'] = "Code coverage is a metric that measures the code that was run when tests are executed. It does not measure how much of the code is tested, although well-written tests should make the difference immaterial."

#Yasca
default['tools']['php']['version'] = 'php5.6'
default['tools']['php']['bin'] = "/usr/bin/#{node['tools']['php']['version']}"
default['tools']['installations']['yasca'] = "#{node['tools']['php']['version']}-cli", "#{node['tools']['php']['version']}-xml", "libapache2-mod-#{node['tools']['php']['version']}"

default['tools']['yasca']['user'] = 'yasca'
default['tools']['yasca']['group'] = 'www-data'

default['tools']['yasca']['version'] = '2.21'
default['tools']['yasca']['url'] = "http://sourceforge.net/projects/yasca/files/Yasca%202.x/Yasca%202.2/yasca-core-#{node['tools']['yasca']['version']}.zip"

default['tools']['yasca']['base'] = '/usr/java'
default['tools']['yasca']['home'] = "#{node['tools']['yasca']['base']}/yasca"
default['tools']['yasca']['output'] = "#{node['tools']['yasca']['home']}/YascaScanResults"

