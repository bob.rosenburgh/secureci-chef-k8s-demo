#
# Cookbook Name:: java_tools
# Recipe:: default
#
# Copyright (C) 2014 Max Saperstone
#
# All rights reserved - Do Not Redistribute
#

include_recipe "zip"
include_recipe "java_oracle"

directory node['tools']['base_dir'] do
  owner node['tools']['user']
  group node['tools']['group']
  recursive true
  mode 00755
end

#get some jar files
for jar in node['tools']['jars'] do
  remote_file "#{node['tools']['base_dir']}/#{jar}-#{node['tools'][jar]['version']}.jar" do
    source node['tools'][jar]['url']
    owner node['tools']['user']
    group node['tools']['group']
    mode 0755
    action :create_if_missing
  end
end

#get some zip files
for zip in node['tools']['zips'] do
  remote_file "#{node['tools']['base_dir']}/#{zip}-#{node['tools'][zip]['version']}.zip" do
    source node['tools'][zip]['url']
    owner node['tools']['user']
    group node['tools']['group']
    mode 0755
    action :create_if_missing
    not_if { ::File.directory?("#{node['tools']['base_dir']}/#{zip}-#{node['tools'][zip]['version']}") }
  end
  execute "unzip #{zip}" do
    cwd node['tools']['base_dir']
    command "7z x -y #{zip}-#{node['tools'][zip]['version']}.zip -o#{zip}-#{node['tools'][zip]['version']} "
    user "root"
    action :run
    not_if { ::File.directory?("#{node['tools']['base_dir']}/#{zip}-#{node['tools'][zip]['version']}") }
  end
  execute "remove #{zip}" do
    cwd node['tools']['base_dir']
    command "rm -rf #{zip}-#{node['tools'][zip]['version']}.zip"
    user "root"
    action :run
  end
  execute "chown #{zip}" do
    cwd node['tools']['base_dir']
    command "chown -R #{node['tools']['user']}:#{node['tools']['group']} #{zip}-#{node['tools'][zip]['version']}"
    user "root"
    action :run
  end
end

#get some tar files
for tar in node['tools']['tars'] do
  remote_file "#{node['tools']['base_dir']}/#{tar}-#{node['tools'][tar]['version']}-bin.tar.gz" do
    source node['tools'][tar]['url']
    owner node['tools']['user']
    group node['tools']['group']
    mode 0755
    action :create_if_missing
    not_if { ::File.directory?("#{node['tools']['base_dir']}/#{tar}-#{node['tools'][tar]['version']}") }
  end
  execute "untar #{tar}" do
    cwd node['tools']['base_dir']
    command "tar zxvf #{tar}-#{node['tools'][tar]['version']}-bin.tar.gz"
    user "root"
    action :run
    not_if { ::File.directory?("#{node['tools']['base_dir']}/#{tar}-#{node['tools'][tar]['version']}") }
  end
  execute "remove #{tar}" do
    cwd node['tools']['base_dir']
    command "rm -rf #{tar}-#{node['tools'][tar]['version']}-bin.tar.gz"
    user "root"
    action :run
  end
  execute "chown #{tar}" do
    cwd node['tools']['base_dir']
    command "chown -R #{node['tools']['user']}:#{node['tools']['group']} #{tar}-#{node['tools'][tar]['version']}"
    user "root"
    action :run
  end
end

#set secureci testing framework to mode 777 so all users can execute
execute "fixing permissions on secureci-testing-framework" do
  cwd node['tools']['base_dir']
  command "chmod -R 777 secureci-testing-framework-#{node['tools']['secureci-testing-framework']['version']}"
  user "root"
  action :run
  only_if { node['tools']['all'].include? "secureci-testing-framework" }
end

template "/etc/profile.d/java_tools.sh" do
  source "java_tools.sh.erb"
  mode '0755'
  owner 'root'
  group 'root'
end

include_recipe "java_tools::yasca"
