name             'java_tools'
maintainer       'Max Saperstone'
maintainer_email 'max.saperstone@coveros.com'
license          'All rights reserved'
description      'Installs/Configures java_tools'
long_description 'Installs/Configures java_tools'
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          '0.1.10'

supports 'ubuntu'

depends 'aptupdate'
depends 'zip'
depends 'java_oracle'
depends 'openssl'

# 0.1.10 updating unit/integration tests
# 0.1.9 food critic fixes
# 0.1.8 updating attributes + updated yasca script
# 0.1.7 added yasca
# 0.1.6 added unit/integration test + kitchen
# 0.1.5 update checkstyle and pmd-bin to point their download URLs to sourceforge
# 0.1.4 upgrading versions and adding back in findbugs
# 0.1.3 updating java tool versions
# 0.1.2 added metadata for link friendly names
# 0.1.1 added dependence on java tools
