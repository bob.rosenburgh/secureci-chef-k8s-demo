require 'spec_helper'

describe 'Yasca Configs' do
  let(:node) { JSON.parse(IO.read('/tmp/kitchen/chef_node.json')) }

  describe user("yasca") do
    it { should exist }
  end
  describe group("www-data") do
    it { should exist }
  end
  describe command("cut -d: -f1 /etc/passwd") do
    its(:stdout) { should contain(node['default']['tools']['yasca']['user']) }
    its(:exit_status) { should eq 0 }
  end
  describe file("/usr/java/yasca") do
    it { should be_owned_by node['default']['tools']['yasca']['user'] }
    it { should be_grouped_into node['default']['tools']['yasca']['group'] }
  end
  describe file("/usr/java/yasca/YascaScanResults") do
    it { should be_owned_by node['default']['tools']['yasca']['user'] }
    it { should be_grouped_into node['default']['tools']['yasca']['group'] }
  end
 end
