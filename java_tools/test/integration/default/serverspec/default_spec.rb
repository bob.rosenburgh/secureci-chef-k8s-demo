require 'spec_helper'

def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

tools_base = node['default']['tools']['base_dir']
describe 'Java Tools Dir' do

describe user("www-data") do
    it { should exist }
  end
describe group("www-data") do
    it { should exist }
  end
describe file("#{tools_base}") do
    it { should be_owned_by node['default']['tools']['user'] }
    it { should be_grouped_into node['default']['tools']['group'] }
  end
end

describe 'Java Tools' do
  let(:node) { JSON.parse(IO.read('/tmp/kitchen/chef_node.json')) }

describe file("#{tools_base}/findbugs-#{node['default']['tools']['findbugs']['version']}/findbugs-#{node['default']['tools']['findbugs']['version']}/lib/findbugs.jar") do
    it { should exist }
    it { should be_owned_by node['default']['tools']['user'] }
    it { should be_grouped_into node['default']['tools']['group'] }
   
    it { should be_readable.by(:owner) }
    it { should be_readable.by(:group) }
    it { should be_readable.by(:others) }
   
    it { should     be_writable.by(:owner) }
    it { should_not be_writable.by(:group) }
    it { should_not be_writable.by(:others) } 

    it { should_not be_executable.by(:owner) }
    it { should_not be_executable.by(:group) }
    it { should_not be_executable.by(:others) }
  end
describe file("#{tools_base}/jdepend-#{node['default']['tools']['jdepend']['version']}/jdepend-#{node['default']['tools']['jdepend']['version']}/lib/jdepend-#{node['default']['tools']['jdepend']['version']}.jar") do
    it { should exist }
    it { should be_owned_by node['default']['tools']['user'] }
    it { should be_grouped_into node['default']['tools']['group'] }
   
    it { should be_readable.by(:owner) }
    it { should be_readable.by(:group) }
    it { should be_readable.by(:others) }
   
    it { should     be_writable.by(:owner) }
    it { should_not be_writable.by(:group) }
    it { should_not be_writable.by(:others) } 

    it { should_not be_executable.by(:owner) }
    it { should_not be_executable.by(:group) }
    it { should_not be_executable.by(:others) }
  end
describe file("#{tools_base}/secureci-testing-framework-#{node['default']['tools']['secureci-testing-framework']['version']}/") do
    it { should exist }
    it { should be_owned_by node['default']['tools']['user'] }
    it { should be_grouped_into node['default']['tools']['group'] }
   
    it { should be_readable.by(:owner) }
    it { should be_readable.by(:group) }
    it { should be_readable.by(:others) }
   
    it { should be_writable.by(:owner) }
    it { should be_writable.by(:group) }
    it { should be_writable.by(:others) } 

    it { should be_executable.by(:owner) }
    it { should be_executable.by(:group) }
    it { should be_executable.by(:others) }
  end
describe file("#{tools_base}/cobertura-#{node['default']['tools']['cobertura']['version']}/cobertura-#{node['default']['tools']['cobertura']['version']}.jar") do
    it { should exist }
    it { should be_owned_by node['default']['tools']['user'] }
    it { should be_grouped_into node['default']['tools']['group'] }
   
    it { should be_readable.by(:owner) }
    it { should be_readable.by(:group) }
    it { should be_readable.by(:others) }
   
    it { should     be_writable.by(:owner) }
    it { should_not be_writable.by(:group) }
    it { should_not be_writable.by(:others) } 

    it { should_not be_executable.by(:owner) }
    it { should_not be_executable.by(:group) }
    it { should_not be_executable.by(:others) }
  end
describe file("#{tools_base}/pmd-bin-#{node['default']['tools']['pmd-bin']['version']}/pmd-bin-#{node['default']['tools']['pmd-bin']['version']}/lib/pmd-java-#{node['default']['tools']['pmd-bin']['version']}.jar") do
    it { should exist }
    it { should be_owned_by node['default']['tools']['user'] }
    it { should be_grouped_into node['default']['tools']['group'] }
   
    it { should be_readable.by(:owner) }
    it { should be_readable.by(:group) }
    it { should be_readable.by(:others) }
   
    it { should     be_writable.by(:owner) }
    it { should_not be_writable.by(:group) }
    it { should_not be_writable.by(:others) } 

    it { should_not be_executable.by(:owner) }
    it { should_not be_executable.by(:group) }
    it { should_not be_executable.by(:others) }
  end
describe file("#{tools_base}/selenium-#{node['default']['tools']['selenium']['version']}/selenium-#{node['default']['tools']['selenium']['version']}/selenium-java-#{node['default']['tools']['selenium']['version']}.jar") do
    it { should exist }
    it { should be_owned_by node['default']['tools']['user'] }
    it { should be_grouped_into node['default']['tools']['group'] }
   
    it { should be_readable.by(:owner) }
    it { should be_readable.by(:group) }
    it { should be_readable.by(:others) }
   
    it { should     be_writable.by(:owner) }
    it { should_not be_writable.by(:group) }
    it { should_not be_writable.by(:others) } 

    it { should_not be_executable.by(:owner) }
    it { should_not be_executable.by(:group) }
    it { should_not be_executable.by(:others) }
  end
describe command("ls -ld #{tools_base}/secureci-testing-framework-#{node['default']['tools']['secureci-testing-framework']['version']}") do
    its(:stdout) { should contain('drwxrwxrwx')}
    its(:exit_status) { should eq 0 }
  end
describe command("find #{tools_base}/jdepend-#{node['default']['tools']['jdepend']['version']}") do	
    its(:stdout) { should contain("build.xml")}	
    its(:exit_status) { should eq 0 }
  end
end
