# Spec:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

require 'spec_helper'


describe 'java_tools::default' do
  let :chef_run do
      ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end
let :base do  
    chef_run.node['tools']['base_dir']
  end
let :user do 
    chef_run.node['tools']['user']
  end
let :group do 
    chef_run.node['tools']['group']
  end 
jars = %w{checkstyle selenium-server junit testng}
zips = %w{jdepend pmd-bin selenium  secureci-testing-framework findbugs}
tars = %w{cobertura}

before(:each) do
    allow(File).to receive(:directory?).with(anything).and_call_original
    #zips.each do |zip|
    #allow(File).to receive(:directory?).with("/usr/java/#{zip}-#{chef_run.node['tools'][zip]['version']}").and_return(true) 
   # end
    allow(File).to receive(:directory?).with("/usr/java/jdepend-2.9.1").and_return(true)
    allow(File).to receive(:directory?).with("/usr/java/pmd-bin-5.5.2").and_return(true)
    allow(File).to receive(:directory?).with("/usr/java/selenium-2.53.1").and_return(true)
    allow(File).to receive(:directory?).with("/usr/java/secureci-testing-framework-1.3.0").and_return(true)
    allow(File).to receive(:directory?).with("/usr/java/findbugs-3.0.1").and_return(true)
    allow(File).to receive(:directory?).with("/usr/java/cobertura-2.1.1").and_return(true)
end
it 'should set the base_dir user/group' do
    chef_run.should create_directory base
  end

it 'changes stf version permissions to "777"' do
    expect(chef_run).to run_execute("chmod -R 777 secureci-testing-framework-#{chef_run.node['tools']['secureci-testing-framework']['version']}")
  end
jars.each do |jar|
it 'should create remote file' do
    expect(chef_run).to create_remote_file_if_missing("#{base}/#{jar}-#{chef_run.node['tools'][jar]['version']}.jar")
  end
 end
zips.each do |zip|
it 'should create remote file' do
    expect(chef_run).to_not create_remote_file_if_missing("#{base}/#{zip}-#{chef_run.node['tools'][zip]['version']}.zip")
  end
it "should unzip #{zip}" do
    expect(chef_run).to_not run_execute("7z x -y #{zip}-#{chef_run.node['tools'][zip]['version']}.zip -o#{zip}-#{chef_run.node['tools'][zip]['version']} ")
  end
it "should remove #{zip}" do
    expect(chef_run).to run_execute("rm -rf #{zip}-#{chef_run.node['tools'][zip]['version']}.zip")
  end
it "should chown #{zip}" do
     expect(chef_run).to run_execute("chown -R #{user}:#{group} #{zip}-#{chef_run.node['tools'][zip]['version']}")  
  end
 end
tars.each do |tar|
it 'should create remote file' do
    expect(chef_run).to_not create_remote_file_if_missing("#{base}/#{tar}-#{chef_run.node['tools'][tar]['version']}-bin.tar.gz")
  end   
it "should untar #{tar}" do
    expect(chef_run).to_not run_execute("tar zxvf #{tar}-#{chef_run.node['tools'][tar]['version']}-bin.tar.gz")
  end   
it "should remove #{tar}" do
    expect(chef_run).to run_execute("rm -rf #{tar}-#{chef_run.node['tools'][tar]['version']}-bin.tar.gz")
  end
it "should chown #{tar}" do 
     expect(chef_run).to run_execute("chown -R #{user}:#{group} #{tar}-#{chef_run.node['tools'][tar]['version']}")
  end
 end 
it 'should create template file' do
     expect(chef_run).to create_template('/etc/profile.d/java_tools.sh').with(
      source: "java_tools.sh.erb",
      mode:   "0755",
      owner:  "root",
      group:  "root"
    )
  end

at_exit { ChefSpec::Coverage.report! }
end
