require 'spec_helper'

 describe 'java_tools::yasca' do
 let :chef_run do
   ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end
 let :yascazip do
   "yasca-core-#{chef_run.node['tools']['yasca']['version']}.zip"
 end
 let :home do
   chef_run.node['tools']['yasca']['home']
 end

before(:each) do
    allow(File).to receive(:directory?).with(anything).and_call_original
    allow(File).to receive(:directory?).with("/usr/java/yasca/YascaScanResults").and_return(true)
    
    allow(File).to receive(:exists?).with(anything).and_call_original
    allow(File).to receive(:exists?).with("/usr/java/yasca/yasca").and_return(true)
end

  it 'should create a yasca user' do 
    expect(chef_run).to create_user(chef_run.node['tools']['yasca']['user'])
 end
  it 'should change ownership of "/usr/java/yasca"' do
    expect(chef_run).to create_directory('/usr/java/yasca').with(
    owner: 'yasca',
    group: 'www-data',
   ) 
 end
  it 'should change ownership of "/usr/java/yasca/YascaScanResults"' do
    expect(chef_run).to create_directory('/usr/java/yasca/YascaScanResults').with(
    owner: 'yasca',
    group: 'www-data',
   )
 end

  it 'should create a remote file' do 
    expect(chef_run).to_not create_remote_file_if_missing("#{chef_run.node['tools']['yasca']['home']}/#{yascazip}")
  end
  it 'unzips yasca' do
    expect(chef_run).to_not run_execute("unzip #{yascazip}")
  end
  it 'removes yasca zip' do
    expect(chef_run).to run_execute("rm -rf #{yascazip}")
  end
  it 'runs chown on yasca' do
    expect(chef_run).to run_execute('chown -R yasca:www-data yasca')
  end
  it 'adds php5 resources' do
    expect(chef_run).to run_execute('sudo add-apt-repository ppa:ondrej/php')
  end
  it 'installs php5-cli' do
    expect(chef_run).to install_package(chef_run.node['tools']['installations']['yasca'])
  end
  it 'runs sudo apt-get update' do
    expect(chef_run).to run_execute('sudo apt-get update')
  end 
  it 'disables php5' do
    expect(chef_run).to run_execute("sudo a2dismod #{chef_run.node['tools']['php']['version']}")
  end 
  it 'sets php' do
    expect(chef_run).to run_execute("sudo update-alternatives --set php #{chef_run.node['tools']['php']['bin']}")
  end 
 files = %w{yasca plugins/Grep.php checkResults.py}
  files.each do |file|
  it "should create cookbook file #{file}" do
    expect(chef_run).to create_cookbook_file("#{home}/#{file}")
  end
 end
end
