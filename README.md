# SecureCI Chef Infrastructure  
SecureCI machines can be built via Jenkins  
https://coveros.secureci.com/hudson/job/SecureCI-Build  
Applications can be installed piece-wise  
Machine size can be selected  
Drive Size can be selected  
Machine type can be selected  
  
Chef Server is located at  
https://chef.dev.secureci.com


## Where We Are
The chef server needs to be spun up on AWS, and then can be accessed via chef.dev.secureci.com.  
We need to start talking to [Jonathan] about possibly making these recipes friendly across other platforms, 
so we can reuse them for a Windows/.NET SecureCI.  
All of the application configuration is being done by the chef recipes, including installing plugins, except for Nexus (wip).  
  
Below is where we stand with the basic items that are needed to get chef released.  
Once those are completed, we can start adding and expanding on them.  
So far, these recipes are all idempotent, which will allow us to manage these chef instances for other individuals.  

## Getting Started

If you want to build a SecureCI server, do the following.

1) install chef, testkitchen
2) put your ec2 secrets in ~/.ec2/credentials
3) check out secureci chef repo
4) move replace .kitchen.yml with .kitchen.ec2.yml
4) berks install # while online
5) kitchen converge # ----wait a few minutes
6) kitchen login
7) then log out, this will give you the ec2 url

the user and password are set in .kitchen.yml, by default they are user: default password:password

Installing pre-req tools:

- Download, install Chef (13.x)
- Download, install Kitchen
- Download, Install Vagrant
- gem install kitchen-vagrant
- gem install berkshelf (note: requires ruby > 2.2.0)

## What this produces

- Builds a complete SecureCI environment with all the tools installed (Jenkins, Nexus, Sonar, Trac, etc.)
- Creates named CI user that gets credentials and permissions to talk between all the servers. (This is not a front-end user)
- The "first-run" script which creates an Admin user on the system with sudo rights, admin users in each of the major apps (Jenkins, Nexus, Trac, etc.)
- There is also an 'add-user' script that can run to create normal administrators. THere's a matching 'delete-user' which removes all that. (This doesn't use LDAP but creates all the independent accounts in the various apps)

## Docs TODO

All these need to be produced into this README (or linked ones) so that people can find them.

[ ] How to checkout and do a local build/test of SecureCI
[ ] How to do an EC2-based build/test of SecureCI
[ ] How to package a final release of SecureCI and ship it

## Testing
We still need to determine tests (at the least sanity tests) for each of the applications/pieces that we install

These break down to needing tests for
- Maven
- Ant
- Java Tools
- Ratproxy
- Sonar
- Nexus
- Jenkins
- Subversion
- Git
- Trac
- SSL Certificate
- Firewall
- User Creation
- OpenSCAP
- YASCA
- ZAP
- Tomcat7
- MySQL

## Questions
- [ ] on AMI how much sense does it make to create a new user? Still unable to connect via new user, and no keys are copied over there
- [ ] how can you reset your password - perhaps remove auth for trac, and rework permissions for unauthenticated

## Bugs

need to enter sudo password on firstrun
should switch to new user (restart?) on boot
should delete initial ubuntu user - unless ubuntu user is also created
no ipv4 assigned when booting up my vm
unable to connect to vm
should see if we can package with vmware tools

- [ ] after packaging and relaunching, all IPs are incorrect - need to throw it into firstrun
- [ ] on running packaging in jenkins, we get an error on trying to restart/reload applications
- [ ] can only launch packaged image as medium, not ebs machine
- [ ] add other aliases back into ssl.erb
- [ ] repo resync cron job doesn't work (sudo trac-admin /var/lib/trac/secureci repository resync '*')
- [ ] nexus sonar repo that is setup doesn't appear to properly connect
- [ ] sonar needs to be run as user sonar
- [ ] nexus needs to be run as user nexus
- [ ] make application links open in new window from trac
- [ ] jenkins needs to be run as user jenkins
- [ ] accessing selenium grid console over https causes errors
- [ ] upgrading sonar won't grab all of the plugins - is this a problem?
- [ ] 'hudson.plugins.sonar.SonarPublisher.xml.erb' in jenkins template uses hard coded passwords
- [ ] only need to install jenkins SONAR plugin if sonar is in the run_list
- [ ] only need to install trac JENKINS plugin if jenkins is in the run_list
- [ ] trac plugins are getting installed multiple times, also unsure how upgradable/idempotent they are
- [ ] determine if variables in java_tools.sh are being set properly
- [ ] python 3.4 is getting installed. Need to fix that
- [ ] 'openjdk' is getting installed with tomcat7, we are setting oracle to default, but should we remove openjdk or install tomcat7 differently
- [ ] need to add link to new machine from jenkins job
- [ ] see if we can auto create ami from jenkins on 'distribute' option
- [x] default.conf is getting overwritten
- [x] resetDbPasswords not properly getting sonar password
- [x] need to remove installation of chef for packaged version
- [x] after packaging and relaunching, htpasswd is not available
- [x] nexus and sonar do not start on startup
- [x] add python as a supporting tool to wiki
- [x] add postfix as a supporting tool to wiki
- [x] mysql version isn't showing on wiki
- [x] able to access /jenkins, /nexus, or /sonar over http, but not /, only over https
- [x] unable to relocate nexus_home. it's stuck in /usr/share/tomcat7/sonatype-work/
- [x] are we still using the firstboot '/var/www-firstboot/' - no remove it
- [x] 'create-dbs.sql' in mysql files currently uses hard coded usernames and passwords
- [x] re-running recipe causes users to get wiped out
- [x] findbugs sometimes shows in trac as greyed, need to determine smarter way to place it (using !)
- [x] jenkins recipe should be split, installing plugins as a separate recipe
- [x] configure grid to be accessible over http - proxy config, and add to wiki
- [x] sonar recipe should be split, installing plugins as a separate recipe
- [x] trying to install a new version of jenkins or nexus won't work as jenkins.war will always be there - consider saving war with version, and softlinking once war is exploded
- [x] create zip install recipe and call it, stop running it multiple times
- [x] add installing postfix to initialize
- [x] initial login takes you to dir page for /var/www/ due to proxy location access
- [x] when aws starts up image, it replaces hostname with it's own
- [x] only install wiki pages if tools are installed
- [x] trac recipe should be split, installing wiki as a separate recipe
- [x] trac recipe should be split, installing plugins as a separate recipe
- [x] remove feedback form from trac
- [x] trac recipe can't run twice, tries to initialize initenv twice
- [x] bug in python source, preventing trac install: http://trac.edgewall.org/ticket/10903#comment:5 
- [x] subversion recipe can't run twice, tries to startup subversion even if it is currently running
- [x] currently setting apj when install jenkins, sonar, and/or nexus. this requires duplicate code and templates, need to create apache templates for each one
- [x] 'openjdk' is somehow getting installed, and being set at the default java
- [x] '/etc/hosts' gets reset at some point - secureci is no longer listed under 127.0.1.1

## New Features
- [ ] jenkins is the last app running as war in tomcat7 - do we want to do a full install for it (will give it it's own user
- [ ] do we want to configure jenkins to be a selenium hub
- [ ] agilo add on for trac
- [ ] option to add browsers (chrome, firefox - each would need xvfb)

## Task List
- [x] Initialize system (update aptitude & packages, set hostname)
- [x] Install Java (oracle v7)
- [x] Install Apache2
- [x] Configure AJP
- [x] Install SVN and setup a SecureCI project
- [x] Configured SVN to be accessed over http
- [x] Configured SVN as a service
- [x] Install python
- [x] Install mysql and setup trac on sonar db
- [x] Install Ant
- [x] Install Maven
- [x] Install Java Tools
- [x] Install Tomcat
- [x] Install Jenkins
- [x] Install Jenkins plugins
- [x] Configure Jenkins via XML
- [x] Install Perl
- [x] Install Sonar
- [x] Install Sonar Plugins
- [x] Configure Sonar via XML
- [x] Install OpenSSL
- [x] Configure OpenSSL
- [x] Configure SSL
- [x] Install Ratproxy
- [x] Install Postfix
- [x] Install Nexus
- [x] Configure Nexus via XML
- [x] Install Firewall
- [x] Install Trac
- [x] Configure Trac
- [x] Install Trac Plugins
- [x] Install Trac Wiki
  - [x] New Wiki For TestNG
  - [x] New Wiki For Selenium Server
  - [x] Rewrite Wiki For Selenium
  - [ ] Rewrite Wiki for Tomcat
  - [ ] Write Wiki for Ant
  - [ ] Write Wiki for Maven
  - [ ] Write Wikis for Git and GitBlit
- [x] Install Git
- [x] Install Gitblit
- [x] Configure Gitblit via properties file
- [x] Update gitblit in Trac
- [x] Setup boot scripts
- [x] Package