normal['jenkins']['master']['user'] = 'jenkins'
normal['jenkins']['master']['group'] = 'jenkins'
normal['jenkins']['master']['install_method'] = 'package'
normal['jenkins']['master']['home'] = '/var/lib/jenkins'
default['jenkins']['home_dir']      = node['jenkins']['master']['home']
normal['jenkins']['master']['port'] = '8083'
default['secureci-jenkins']['version'] = node['jenkins']['master']['version']
normal['jenkins']['master']['jenkins_args'] = "--webroot=/var/cache/jenkins/war --httpListenAddress=$HTTP_LISTEN_ADDRESS --httpPort=$HTTP_PORT --ajp13Port=$AJP_PORT --prefix=$PREFIX"
normal['jenkins']['master']['endpoint'] = "http://#{node['jenkins']['master']['host']}:#{node['jenkins']['master']['port']}/jenkins"
normal['jenkins']['master']['jvm_options'] = "-Djenkins.install.runSetupWizard=false"
default['secureci-jenkins']['properties'] = '/var/lib/tomcat7/webapps/jenkins/META-INF/maven/org.jenkins-ci.main/jenkins-war/pom.properties'

default['secureci-jenkins']['plugins'] = [ "Xvfb", "analysis-core", "checkstyle", "cobertura", "dry", "findbugs", "jdepend", "pmd", "warnings", "sonar", "trac", "git", "docker-build-publish", "docker-custom-build-environment","gradle", "workflow-multibranch","workflow-aggregator" ]

default['sonar']['scanner']['version'] = '2.8'
