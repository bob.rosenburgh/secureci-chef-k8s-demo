require 'spec_helper'

def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

RSpec::Matchers.define :match_key_value do |key, value|
  match do |actual|
    actual =~ /^\s*?#{key}\s*?=\s*?#{value}/
  end
end
jenkins_home  = node['default']['jenkins']['home_dir'] 
jenkins_user  = node['default']['jenkins']['master']['user']
jenkins_group = node['default']['jenkins']['master']['group']

describe 'Jenkins' do

it 'should listen on port 8080' do
  expect(port(8080)).to be_listening
 end
it 'should enable and start the jenkins service' do
  expect(service('jenkins')).to be_enabled
  expect(service('jenkins')).to be_running
 end
it 'should place the htpasswd config' do
  htpasswd_config = file("#{jenkins_home}/config.xml")
  expect(htpasswd_config).to be_file
  expect(htpasswd_config).to be_mode(644)
  expect(htpasswd_config).to be_owned_by(jenkins_user)
 end
describe command ("cat #{jenkins_home}/config.xml | grep 'use' | cut -d '<' -f2 | cut -d '>' -f2") do
   its(:stdout) { should contain("true") }
 end
describe file ("#{jenkins_home}/.ssh") do
  it { should be_directory }
  it { should be_owned_by jenkins_user }
  it { should be_grouped_into jenkins_group }
  it { should_not be_readable.by 'others'}
end
describe file ("#{jenkins_home}/.ssh/secureci") do
  it { should exist } 
  it { should be_owned_by jenkins_user }
  it { should_not be_readable.by 'others'}
 end
describe file ("#{jenkins_home}/credentials.xml") do
  it { should exist } 
  it { should be_owned_by jenkins_user }
  it { should be_grouped_into jenkins_group }
  it { should be_readable.by 'others'}
 end 
#will fail if secureci-ant isn't included in runlist
describe file ("#{jenkins_home}/hudson.tasks.Ant.xml") do
  it { should exist } 
  it { should be_owned_by jenkins_user }
  it { should be_grouped_into jenkins_group }
  it { should be_readable.by 'others'}
 end 
#will fail if secureci-maven isn't included in runlist
describe file ("#{jenkins_home}/hudson.tasks.Maven.xml") do
  it { should exist } 
  it { should be_owned_by jenkins_user }
  it { should be_grouped_into jenkins_group }
  it { should be_readable.by 'others'}
 end
describe file ("#{jenkins_home}/jenkins.model.JenkinsLocationConfiguration.xml") do
  it { should exist } 
  it { should be_owned_by jenkins_user }
  it { should be_grouped_into jenkins_group }
  it { should be_readable.by 'others'}
 end 
end
