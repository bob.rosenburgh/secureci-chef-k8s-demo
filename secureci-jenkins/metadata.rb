name             "secureci-jenkins"
maintainer       "Max Saperstone"
maintainer_email "max.saperstone@coveros.com"
license          "All Rights Reserved"
description      "Install jenkins into our application"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "install jenkins"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.1.11"

supports 'ubuntu'

depends 'aptupdate'
depends 'zip'
depends 'apache2'
depends 'java_tools'
depends 'java_oracle'
depends 'secureci-ant'
depends 'secureci-maven'
depends 'secureci-mysql'
depends 'sonar'
depends 'jenkins' 
depends 'windows'
depends 'initialuser'
depends 'curl'
depends 'docker'

# TODO remoge dependancy on sonar
#0.1.11 - updating unit tests / updating dependacies
#0.1.10 - food critic fixes
#0.1.9 - Fixing plugins, and SQ integration
#0.1.8 - updating jenkins version/updating plugins
#0.1.7 - set dependency to use secureci-ant
#0.1.6 - tying down jenkins versions
#0.1.5 - fixed anonymous permissions bug
#0.1.4 - skip running setup wizard during installation; fixes issue installing 2.x versions of jenkins
#0.1.3 - adding in plugin for htpasswd for jenkins
#0.1.1 - updating plugin versions
#0.1.0 - Major rewrite to use opscode jenkins cookbook
#0.0.7 - minor update to allow for port selection.
#0.0.6 - split out plugins into separate recipe
#0.0.5 - making jenkins updatable
#0.0.4 - moved apache2 proxy config into apache2 cookbook
#0.0.3 - added xml configs
#0.0.2 - adding plugins
#0.0.1 - initial recipe
