#
# Cookbook Name:: jenkins
# Recipe:: default
# Author:: Max Saperstone
#
# Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
include_recipe "secureci-ant"
include_recipe "secureci-maven"
include_recipe "java_tools"
include_recipe 'java_oracle::default'
include_recipe 'jenkins::master'
include_recipe 'curl'
include_recipe 'initialuser'


#create a public/private keypair for jenkins
directory File.join(node['jenkins']['home_dir'],'.ssh') do
  mode '0700'
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['group']
end
execute "Generating Public/Private Keypair" do
  user node['jenkins']['master']['user']
  command "ssh-keygen -t rsa -P '' -f #{node['jenkins']['home_dir']}/.ssh/secureci"
  not_if { ::File.exists?(File.join( node['jenkins']['home_dir'],'.ssh','secureci' ))}
end

#Add these credentials into Jenkins
template "#{node['jenkins']['home_dir']}/credentials.xml" do
  source "credentials.xml.erb"
  mode '0644'
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['group']
end

#Configure Ant
template "#{node['jenkins']['home_dir']}/hudson.tasks.Ant.xml" do
  source "hudson.tasks.Ant.xml.erb"
  mode '0644'
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['group']
  only_if { ::File.directory?(node['secureci-ant']['home']) }
end

#Configure Maven
template "#{node['jenkins']['home_dir']}/hudson.tasks.Maven.xml" do
  source "hudson.tasks.Maven.xml.erb"
  mode '0644'
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['group']
  only_if { ::File.directory?(node['maven']['m2_home']) }
end

include_recipe 'secureci-jenkins::plugins'

group 'docker' do
  action :modify
  append true
  members 'jenkins'
  notifies :restart, 'service[jenkins plugin restart]', :immediately
  only_if "getent group docker"
end

 
#configure docker
bash "wait for jenkins" do
  code <<-EOH
    sleep 10; while [ `curl -I http://localhost:8080/jenkins/credentials/store/system/domain/_/createCredentials 2>/dev/null | grep HTTP |sed -e 's/.*\([0-9]\{3\}\).*/\1/g'` != "503" ] ; do sleep 10; echo still 503; done
  EOH
  timeout 240
end

payload="json={\'\': \'0\', \'credentials\': {\'scope\': \'GLOBAL\', \'id\': \'docker\', \'username\': \'" + node['initial_user'] + "\', \'password\': \'" + node['password'] + "\', \'description\': \'docker user\', \'\$class\': \'com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl\' } }"
http_request "Jenkins docker user" do
  action :post
  url 'http://localhost:8080/jenkins/credentials/store/system/domain/_/createCredentials'
  headers ({
    'Content-Type' => 'application/x-www-form-urlencoded'
  })
  ignore_failure true
  retries 5
  retry_delay 5
  message "#{URI.encode("#{payload}")}" # ~FC002 ignored because this is the only way to do it
end
