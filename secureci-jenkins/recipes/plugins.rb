#
# Cookbook Name:: jenkins
# Recipe:: plugins
# Author:: Max Saperstone
#
# Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#no point in installing these plugins if java_tools aren't put on there
#include_recipe "java_tools"

#temporarily disable security 
template "#{node['jenkins']['master']['home']}/config.xml" do
  source "temp_config.xml.erb"
  mode '0644'
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['group']
end
service "jenkins plugin restart" do
  service_name   "jenkins"
  action :restart
end

execute "waitforjenkins" do
  command "sleep 100"
end

for plugin in node['secureci-jenkins']['plugins'] do
  execute "install plugins" do
    command "java -jar /var/cache/jenkins/war/WEB-INF/jenkins-cli.jar -s http://127.0.0.1:#{node['jenkins']['master']['port']}/jenkins install-plugin #{plugin}"
  end
end

#install htpasswd plugin
cookbook_file "htpasswd-auth.hpi" do
  path File.join(node['jenkins']['master']['home'],"plugins","htpasswd-auth.hpi")
  mode '0755'
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['group']
  action :create
end

#Configure Sonar Plugins
%w[ hudson.plugins.sonar.SonarPublisher.xml hudson.plugins.sonar.SonarRunnerInstallation.xml ].each do |plugin|
  template File.join(node['jenkins']['master']['home'],plugin) do
    source "#{plugin}.erb"
    mode '0644'
    owner node['jenkins']['master']['user']
    group node['jenkins']['master']['group']
    only_if { ::File.directory?(node['sonar']['home']) } 
  end
end

#configure htpasswd plugin
template "#{node['jenkins']['master']['home']}/config.xml" do
  source "config.xml.erb"
  mode '0644'
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['group']
end

#configure jenkins location plugin
template "#{node['jenkins']['master']['home']}/jenkins.model.JenkinsLocationConfiguration.xml" do
  source "jenkins.model.JenkinsLocationConfiguration.xml.erb"
  mode '0644'
  owner node['jenkins']['master']['user']
  group node['jenkins']['master']['group']
end

service "jenkins plugin restart" do
  service_name   "jenkins"
  action :restart
end

# update jenkins plugins
execute "Updating Plugins via CLI" do
  command <<-EOF
    sleep 60;
    UPDATE_LIST=$( java -jar  /var/cache/jenkins/war/WEB-INF/jenkins-cli.jar -s http://127.0.0.1:#{node['jenkins']['master']['port']}/jenkins list-plugins | grep -e '[0-9])$' | awk '{ print $1 }' ); 
    if [ ! -z "${UPDATE_LIST}" ]; then 
      echo Updating Jenkins Plugins: ${UPDATE_LIST}; 
      java -jar /var/cache/jenkins/war/WEB-INF/jenkins-cli.jar -s http://127.0.0.1:#{node['jenkins']['master']['port']}/jenkins install-plugin ${UPDATE_LIST};
      java -jar /var/cache/jenkins/war/WEB-INF/jenkins-cli.jar -s http://127.0.0.1:#{node['jenkins']['master']['port']}/jenkins safe-restart;
    fi
  EOF
end
