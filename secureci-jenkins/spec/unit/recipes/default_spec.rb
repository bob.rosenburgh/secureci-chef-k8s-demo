# Spec:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

require 'spec_helper'


describe 'secureci-jenkins::default' do

before(:each) do
  allow(File).to receive(:directory?).with(anything).and_call_original
  allow(File).to receive(:directory?).with('/usr/share/ant').and_return true
  allow(File).to receive(:directory?).with('/usr/local/maven').and_return true

  allow(File).to receive(:exists?).with(anything).and_call_original
  allow(File).to receive(:exists?).with("/var/lib/jenkins/.ssh/secureci").and_return(true)
 end

let :chef_run do
      ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end

let :jenkins_home do
    chef_run.node['jenkins']['home_dir'] 
 end

it 'includes cookbook java_oracle' do
    expect(chef_run).to include_recipe('java_oracle')
  end
it 'includes cookbook java_tools' do
    expect(chef_run).to include_recipe('java_tools')
  end
it 'should create ssh dir' do
    expect(chef_run).to create_directory "#{jenkins_home}/.ssh"
  end
it "execute 'Generating Public/Private Keypair'" do
    expect(chef_run).to_not run_execute("Generating Public/Private Keypair")
  end
it 'creates a template with attributes' do
    expect(chef_run).to create_template("#{jenkins_home}/credentials.xml").with(
      source:  'credentials.xml.erb',
      user:    'jenkins',
      group:   'jenkins', 
      mode:    '0644'
    )
  end
it 'creates a template to config ant' do
    expect(chef_run).to create_template("#{jenkins_home}/hudson.tasks.Ant.xml").with(
      source:  'hudson.tasks.Ant.xml.erb',
      user:    'jenkins',
      group:   'jenkins',
      mode:    '0644'
    )
  end
it 'creates a template to config maven' do
    expect(chef_run).to create_template("#{jenkins_home}/hudson.tasks.Maven.xml").with(
      source: 'hudson.tasks.Maven.xml.erb',
      user:   'jenkins',
      group:  'jenkins',
      mode:   '0644'
    )
  end
end

describe 'secureci-jenkins::plugins' do
  
before(:each) do
  allow(File).to receive(:directory?).with(anything).and_call_original
  allow(File).to receive(:directory?).with('/opt/sonar-6.7.6').and_return true
 end

let :chef_run do
      ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end

let :jenkins_home do
     chef_run.node['jenkins']['home_dir']
  end 
jenkins_plugins = %w{analysis-core checkstyle cobertura dry findbugs jdepend pmd warnings sonar trac git}

it 'creates a template with attributes' do
    expect(chef_run).to create_template("#{jenkins_home}/config.xml").with(
      source:  'config.xml.erb',
      user:    'jenkins',
      group:   'jenkins', 
      mode:    '0644'
    )
   end
jenkins_plugins.each do |plugins|
it 'install jenkins plugins' do 
     expect(chef_run).to install_jenkins_plugin(plugins)
   end
  end
it 'renders cookbook file htpasswd-auth.hpi' do
     expect(chef_run).to render_file('htpasswd-auth.hpi')
   end
it 'creates a template with attributes' do
    expect(chef_run).to create_template("#{jenkins_home}/hudson.plugins.sonar.SonarPublisher.xml").with(
      source:  'hudson.plugins.sonar.SonarPublisher.xml.erb',
      user:    'jenkins',
      group:   'jenkins', 
      mode:    '0644'
    )
   end
it 'creates a template with attributes' do
    expect(chef_run).to create_template("#{jenkins_home}/hudson.plugins.sonar.SonarRunnerInstallation.xml").with(
      source:  'hudson.plugins.sonar.SonarRunnerInstallation.xml.erb',
      user:    'jenkins',
      group:   'jenkins', 
      mode:    '0644',
    )
   end
it 'creates a template with attributes' do
    expect(chef_run).to create_template("#{jenkins_home}/jenkins.model.JenkinsLocationConfiguration.xml").with(
      source:  'jenkins.model.JenkinsLocationConfiguration.xml.erb',
      user:    'jenkins',
      group:   'jenkins', 
      mode:    '0644',
    )
   end
it 'should restart jenkins' do 
    expect(chef_run).to restart_service('jenkins')
   end
it "Update plugins via CLI" do
    expect(chef_run).to run_execute("Updating Plugins via CLI")
  end 
at_exit { ChefSpec::Coverage.report! }
end
