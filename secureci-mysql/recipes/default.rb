#
# Cookbook Name:: secureci-mysql
# Recipe:: default
# Author:: Walter Mitchell
# Copyright (c) 2016 The Authors, All Rights Reserved.

include_recipe 'apt'

mysql_client 'default' do
  action :create
end

mysql_service node['mysql']['service_name'] do
  version node['mysql']['version']
  port node['mysql']['port']
  data_dir node['mysql']['data_dir']
  initial_root_password node['mysql']['server_root_password']
  action [:create, :start]
end


execute "copy mysql configs" do
  command "sudo cp #{node['mysql']['config_base']}-#{node['mysql']['service_name']}/my.cnf #{node['mysql']['config_base']}/"
end
