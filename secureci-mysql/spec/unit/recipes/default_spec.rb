#
# Cookbook Name:: secureci-mysql
# Spec:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'secureci-mysql::default' do

	let :chef_run do
		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
	end
	let :socket do 
		chef_run.node['mysql']['socket']
	end
	let :password do
		chef_run.node['mysql']['server_root_password']
	end
	it 'should config mysql cient' do
		chef_run.converge(described_recipe)
		expect(chef_run).to create_mysql_client 'default'
	end
	it 'should install/config mysql service via mysql_service resource' do 
		expect(chef_run).to create_mysql_service chef_run.node['mysql']['service_name']
	end

	it 'copies mysql configs' do
		expect(chef_run).to run_execute("copy mysql configs")
	end
	at_exit { ChefSpec::Coverage.report! }
end 
