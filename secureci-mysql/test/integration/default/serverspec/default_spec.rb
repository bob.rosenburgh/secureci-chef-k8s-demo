require 'spec_helper'

def node
   JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

RSpec::Matchers.define :match_key_value do |key, value|
  match do |actual|
    actual =~ /^\s*?#{key}\s*?=\s*?#{value}/
  end
end

port       = node['default']['mysql']['port']
socket     = node['default']['mysql']['socket']
sqlconfig  = '/etc/mysql-secureci/my.cnf'
sqldir     = '/var/lib/mysql'
sqlcommand = "mysql -S #{socket} -e 'SHOW DATABASES;' -uroot -p#{node['default']['mysql']['server_root_password']}"

describe 'mysql' do

describe service('mysql-secureci') do
  it { should be_enabled }
  it { should be_running }
end

describe port(port) do
  it { should be_listening }
end

describe command("mysql --version") do 
  its(:stdout) { should contain(node['default']['mysql']['version'])}
    its(:exit_status) { should eq 0 }
end
describe file(sqlconfig) do
  it { should be_file }
end
describe file(sqldir) do
  it { should be_directory }
  it { should be_owned_by 'mysql' }
  it { should be_grouped_into 'mysql' }
  it { should_not be_readable.by 'others'}
  end
describe command(sqlcommand) do 
  its(:stdout) { should contain("Database")}
  its(:exit_status) { should eq 0 }
end 

end
