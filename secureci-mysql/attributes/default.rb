#
default['mysql']['service_name'] = 'secureci'

# passwords
default['mysql']['server_root_password'] = 'potatOEz'

# paths
default['mysql']['socket'] = "/var/run/mysql-#{node['mysql']['service_name']}/mysqld.sock"
default['mysql']['data_dir'] = '/var/lib/mysql'
default['mysql']['config_base'] = '/etc/mysql'

# port
default['mysql']['port'] = '3306'

# server package version 
default['mysql']['version'] = '5.7'
default['mysql']['version_full'] = node['mysql']['version']
