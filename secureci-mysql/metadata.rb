name 'secureci-mysql'
maintainer 'Walter Mitchell'
maintainer_email 'walter.mitchell@coveros.com'
license 'All Rights Reserved'
description 'Installs/Configures secureci-mysql'
long_description 'Installs/Configures secureci-mysql'
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version '0.2.0'

supports 'ubuntu'

depends 'yum-mysql-community'
depends 'mysql', '= 8.1.1'
depends 'apt'


#0.2.0 - updating unit tests + dependencies
#0.1.0 - created mysql wrapper
