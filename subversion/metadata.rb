name              'subversion'
maintainer        'Opscode, Inc.'
maintainer_email  'cookbooks@opscode.com'
license           'All Rights Reserved'
description       'Installs subversion'
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

supports 'centos'
supports 'debian'
supports 'fedora'
supports 'redhat'
supports 'suse'
supports 'ubuntu'
supports 'windows'

depends 'secureci-apache2'
depends 'windows', '~> 1.10'
depends 'apt'
depends 'aptupdate'
depends 'initialuser'
depends 'apache2'

recipe 'subversion', 'Includes the client recipe.'
recipe 'subversion::client', 'Subversion Client installs subversion and some extra svn libs'
recipe 'subversion::server', 'Subversion Server (Apache2 mod_dav_svn)'

#attribute 'subversion/list_parent_path',
#          :display_name => 'Subversion List Parent Path?',
#          :description  => %q(a choice of "on" or "off".  When set to "on" the list of repositories in the `node['subversion']['repo_dir']` will be indexed at http://<server_name>/svn.  Default is "off"),
#          :required     => 'optional',
#          :default      => 'off',
#          :recipes      => ['subversion::server']

version           '1.3.9'

#1.3.9
#1.3.8 - food critic fixes
#1.3.7 - removing unneeded install of wsgi mod
#1.3.6 - upgrading subversion, adding unit/integration tests
#1.3.5 - upgrading subversion, and fixing bug from 494 of permissions to commit
#1.3.4 - modified dav_svn.conf to include fix for bug 346
#1.3.3 - added in access over apache '/svn'
#1.3.2 - added in subversion as a service
#1.3.1 - added setting servername in secureci recipe, added in mod wsgi
