require 'serverspec'
require 'spec_helper'

def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

svn_user        = node['default']['subversion']['user']
svn_group        = node['default']['subversion']['group']
svn_version     = node['default']['subversion']['version'] 
svn_pass        = node['default']['subversion']['password_dir']

describe 'subversion::secureci' do

  describe command('which svn') do
    its(:exit_status) { should eq 0 }
  end
  describe command('svn --version') do
    its(:stdout) { should contain("svn, version") }
  end
  describe user(svn_user) do
    it { should exist }
  end
  describe file(svn_pass) do
  it { should be_directory }
  it { should be_owned_by svn_user }
  it { should be_grouped_into svn_group }
  it { should be_readable.by 'others'}
  end
  describe command("cat /etc/apache2/apache2.conf | grep 'ServerName'") do 
  its(:stdout) { should contain("secureci") }
  end
  
end
