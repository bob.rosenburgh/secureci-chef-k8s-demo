require 'spec_helper'

describe 'subversion::secureci' do
	def node
		JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
	end

	describe file("/var/lib/passwd/dav_svn.passwd") do
		it { should exist }
	end
end