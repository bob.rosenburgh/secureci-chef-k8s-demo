require 'spec_helper'

describe 'subversion::secureci' do

 before do
  stub_command('/usr/sbin/apache2 -t').and_return('Syntax OK')
  stub_command("test -L /etc/init.d/svnserve").and_return('Syntax OK')
  stub_command("test -L /var/lib/svn").and_return('Syntax OK')
  stub_command('/usr/sbin/httpd -t').and_return('Syntax OK')
  
  allow(File).to receive(:exists?).with(anything).and_call_original
end

let :chef_run do
  ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
end

let :svn_pass do 
 chef_run.node['subversion']['password_dir']
end
let :svn_repo_name do 
 chef_run.node['subversion']['repo_name']
end
let :svn_repodir do 
 chef_run.node['subversion']['repo_dir']
end

it 'includes cookbook apache2' do
  chef_run.converge(described_recipe)
  expect(chef_run).to include_recipe('apache2')
end
it 'creates svn user' do 
  expect(chef_run).to create_user('svn')
end
it 'creates repo' do
 expect(chef_run).to run_execute("svnadmin create #{svn_repodir}/#{svn_repo_name}")
end
it 'should set the password dir user/group' do
  expect(chef_run).to create_directory svn_pass
end
it 'executes ruby block' do 
 expect(chef_run).to run_ruby_block('edit dav_svn.load')
end
it 'executes ruby block' do 
 expect(chef_run).to run_ruby_block('set apache ServerName')
end
service= '/etc/init.d/svnserve'
it 'creates a template with attributes' do
  expect(chef_run).to create_template("#{service}").with(
    source: 'svnserve.erb',
    mode:   '0700',
    owner:  'root',
    group:  'root',
    ) 
end
it 'executes restart on apache2' do
  expect(chef_run).to restart_service('apache2')
end
it 'executes restart on apache2' do
  expect(chef_run).to restart_service('svnserve')
end
at_exit { ChefSpec::Coverage.report! }
end
