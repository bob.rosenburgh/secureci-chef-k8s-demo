require 'spec_helper'

describe 'subversion::server' do
  let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe) 
  end

  before(:each) do
    stub_command('/usr/sbin/apache2 -t').and_return('Syntax OK')
  end

  it 'includes mod_dav_svn' do
    chef_run.converge(described_recipe)
    expect(chef_run).to include_recipe('apache2::mod_dav_svn')
  end

  it 'includes the client recipe' do
    expect(chef_run).to include_recipe('subversion::client')
  end

  it 'creates the repo_dir' do
    expect(chef_run).to create_directory(chef_run.node['subversion']['repo_dir']).with(
      user:  'www-data',
      group: 'www-data',
      mode: '0755',
      recursive: true
      )
  end
  it 'installs package' do 
    expect(chef_run).to install_package('apache2-utils')
  end
  it 'should create htpasswd file' do 
    expect(chef_run).to run_execute('create htpasswd file')
  end
  at_exit { ChefSpec::Coverage.report! }
end
