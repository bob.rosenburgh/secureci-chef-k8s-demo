#
# Cookbook Name:: subversion
# Attributes:: server
#
# Copyright 2009, Daniel DeLeo
# Copyright 2013, Opscode
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

default['subversion']['version'] = '1.9.2'
default['subversion']['repo_dir'] = '/var/lib/svn/repos'
default['subversion']['repo_name'] = 'secureci'
default['subversion']['server_name'] = 'svn'
default['subversion']['user'] = 'svn'
default['subversion']['group'] = 'svn'
default['subversion']['password'] = 'subversion'
default['subversion']['list_parent_path'] = 'on'


#
# User Authentication
default['subversion']['password_dir'] = '/var/lib/passwd'
default['subversion']['auth_type'] = 'Basic'
default['subversion']['auth_name'] = 'Subversion Repository'
default['subversion']['auth_user_file'] = File.join(node['subversion']['password_dir'],'dav_svn.passwd')
default['subversion']['auth_access_file'] = File.join(node['subversion']['password_dir'],'dav_svn.authz')

# For Windows
default['subversion']['msi_source'] = 'http://downloads.sourceforge.net/project/win32svn/1.7.0/Setup-Subversion-1.7.0.msi'
default['subversion']['msi_checksum'] = '2ee93a3a2c1f9b720917263295466f92cac6058c6cd8af65592186235cf0ed71'
