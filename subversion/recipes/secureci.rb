#
# Cookbook Name:: subversion
# Recipe:: secureci
# Author:: Max Saperstone
#
# Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe 'secureci-apache2'
include_recipe 'subversion::client'
include_recipe 'initialuser'


directory node['subversion']['repo_dir'] do
  owner node['subversion']['user']
  group node['subversion']['group']
  mode '0755'
  recursive true
end

execute 'svnadmin create repo' do
  command "svnadmin create #{node['subversion']['repo_dir']}/#{node['subversion']['repo_name']}"
  creates "#{node['subversion']['repo_dir']}/#{node['subversion']['repo_name']}"
  user node['subversion']['user']
  group node['subversion']['group']
end

#replace_file /etc/apache2/mods-available/dav_svn.conf
apache_module 'dav_svn' do
  conf true
  template 'dav_svn.conf.erb'
  server_name "#{node['subversion']['server_name']}.#{node['domain']}"
  notifies 'restart[apache2]'
end

directory node['subversion']['password_dir'] do
  owner node['subversion']['user']
  group node['subversion']['group']
  mode '0775'
  recursive true
end
#add path to dav_module to dav_svn.load
ruby_block "edit dav_svn.load" do
  block do
   file = Chef::Util::FileEdit.new("/etc/apache2/mods-enabled/dav_svn.load")
   file.search_file_delete("LoadModule dav_svn_module /usr/lib/apache2/modules/mod_dav_svn.so") 
   file.insert_line_if_no_match("/LoadModule dav_module /usr/lib/apache2/modules/mod_dav.so/", "LoadModule dav_module /usr/lib/apache2/modules/mod_dav.so") 
   file.insert_line_if_no_match("/LoadModule dav_svn_module /usr/lib/apache2/modules/mod_dav_svn.so/", "LoadModule dav_svn_module /usr/lib/apache2/modules/mod_dav_svn.so")
   file.write_file
  end
end

#replace_file /etc/apache2/httpd.conf - this is updated here
ruby_block "set apache ServerName" do
  block do
    file = Chef::Util::FileEdit.new("/etc/apache2/apache2.conf")
    file.insert_line_if_no_match("/ServerName secureci/", "ServerName secureci")
    file.write_file
  end
end

#install_file /var/lib/passwd/dav_svn.passwd
#install_file /var/lib/passwd/dav_svn.authz
%w[ passwd authz ].each do |file|
  full_file = File.join(node['subversion']['password_dir'],"dav_svn.#{file}")
  cookbook_file "dav_svn.#{file}" do
    path full_file
    action :create
    not_if { ::File.exist?(full_file) }
  end
end

execute 'Add user to htpasswd file' do
  command "htpasswd -b /var/lib/passwd/dav_svn.passwd #{node['initial_user']} #{node['password']}"
end


#install svn as a service

template "/lib/systemd/system/svnserve.service" do
  source "svnserve.erb"
  mode '0755'
  owner 'root'
  group 'root'
end

service "apache2" do
  action :restart
end

#startup subversion
service "svnserve" do
  action [ :enable, :restart ]
end



#TODO
#a2enmod authz_svn
