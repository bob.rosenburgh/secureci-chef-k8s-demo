#!/bin/bash

# aws ec2 run-instances --associate-public-ip-address --image-id ami-026c8acd92718196b --count 1 --instance-type t2.large --key-name bob.rosenburgh1 --security-group-ids sg-08220fda7655873b3 sg-2568db54 --subnet-id subnet-bfb2c2f7
#   --user-data file://my_script.txt

echo 'Installing chef...'
wget https://packages.chef.io/files/stable/chefdk/3.2.30/ubuntu/18.04/chefdk_3.2.30-1_amd64.deb
dpkg -i chefdk_3.2.30-1_amd64.deb



# # install chef omnibus, checkout cookbook, install test kitchen, then run kitchen converge in the userdata script
# curl -L https://omnitruck.chef.io/install.sh | sudo bash
#
# sudo apt-get install ruby -y
#
# sudo apt-get install ruby-dev -y
#
# gem install chef-zero
#
# gem install test-kitchen




#TODO: setup ssh access to repo
git clone --depth 1 --branch feature/microk8s-demo git@gitlab.com:coveros/secureci-chef.git

cd secureci-chef

kitchen converge

# aws ec2 associate-address --instance-id i-0b8c973d220d67710 --allocation-id eipalloc-05b912c0b42f49594
