#
# Cookbook Name:: openvas::installation
# Recipe:: default
# Author:: Ben Pick
#
# Copyright 2015, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe "apt"

user node['openvas']['user'] do
  system true
  shell "/bin/false"
  action :create
end

directory node['openvas']['base'] do
  owner node['openvas']['user']
  group node['openvas']['group']
  recursive true
  mode 00755
end

directory node['openvas']['home'] do
  owner node['openvas']['user']
  group node['openvas']['group']
  mode 0755
  action :create
end

for prepackage_dependencies in node['installations']['build_dependencies'] do
  package prepackage_dependencies do
    action :install
  end
end

execute "add openvas to apt-get repo" do
  command "sudo add-apt-repository #{node['installations']['apt_repository']}"
  user "root"
  action :run
end

#update the system with new repository
execute "apt-get-update-periodic" do
  command "sudo apt-get update"
  ignore_failure true
  user "root"
  action :run
end

package node['installations']['package'] do
    action :install
end

directory node['openvas']['output'] do
  owner node['tomcat']['user']
  group node['tomcat']['group']
  mode 0755
  action :create
end

# run the synchronize scripts 
execute "openvas-nvt-sync" do
  command "sudo openvas-nvt-sync"
  user "root"
  action :run
end
execute "openvas-scapdata-sync" do
  command "sudo openvas-scapdata-sync"
  user "root"
  action :run
end
execute "openvas-certdata-sync" do
  command "sudo openvas-certdata-sync"
  user "root"
  action :run
end

# set the port for the default openvas web ui
execute "openvas web ui set port" do
  command "sudo gsad --port=9392 "
  user "root"
  action :run
end

# restart the openvas services
execute "openvas scanner restart" do
  command "sudo service openvas-scanner restart"
  user "root"
  action :run
end
execute "openvas manager restart" do
  command "sudo service openvas-manager restart"
  user "root"
  action :run
end
execute "openvasmd rebuild" do
  command "sudo openvasmd --rebuild --progress"
  user "root"
  action :run
end

# Place configuration file in intilization directory for startup
cookbook_file 'openvas-gsa' do
  path "#{node['openvas']['initial_config_dir']}/#{node['openvas']['initial_config_file']}"
  mode 0755
  action :create
end
