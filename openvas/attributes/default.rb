default['openvas']['user'] = 'openvas'
default['openvas']['group'] = 'www-data'

default['openvas']['base'] = '/opt'
default['openvas']['home'] = "#{node['openvas']['base']}/openvas"
default['openvas']['output'] = "#{node['openvas']['home']}/openvasScanResults"

default['openvas']['service'] = "openvas"

# Tomcat user/group is needed for Jenkins scripts to run
default['tomcat']['user'] = 'tomcat7'
default['tomcat']['group'] = 'tomcat7'

# Prerequisite library installations
default['installations']['build_dependencies'] = [ "sqlite3", "nmap", "alien", "nsis" ]
default['installations']['package'] = "openvas"
default['installations']['apt_repository'] = "ppa:mrazavi/#{node['installations']['package']}"

# v1: cmake", "pkg-config", "libssh-dev", "libglib2.0-dev", "libpcap-dev", "libgpgme11-dev", "uuid-dev", "bison", "libksba-dev", "libgnutls-dev", "doxygen", "sqlfairy", "xmltoman", "sqlite3", "libsqlite3-dev", "wamerican", "libmicrohttpd-dev", "libxml2-dev", "libxslt1-dev", "xsltproc" ]
# v2: #default['installations']['all'] = [ "build-essential", "autoconf", "devscripts", "dpatch", "bison", "flex", "cmake", "pkg-config", "libldap2-dev", "libssh2-1-dev", "libksba-dev", "libssh-dev", "libassuan-dev", "libwrap0-dev", "libopenvas2", "libgmp3-dev", "libgmp-dev", "libpcre3-dev", "libpth-dev", "libglib2.0-0", "libglib2.0-dev", "libgnutls26", "libgnutls-dev", "libgnutls28-dev", "libpcap0.8", "libpcap0.8-dev", "libgpgme11", "libgpgme11-dev", "doxygen", "libuuid1", "uuid-dev", "libsql-translator-perl", "sqlfairy", "xmltoman", "sqlite3", "libsqlite3-0", "libsqlite3-dev", "libxml2-dev", "libxslt1.1", "libxslt1-dev", "xsltproc", "libmicrohttpd-dev", "quilt", "wamerican", "nmap" ]

default['openvas']['initial_config_dir'] = "/etc/init.d"
default['openvas']['initial_config_file'] = "openvas-gsa"
