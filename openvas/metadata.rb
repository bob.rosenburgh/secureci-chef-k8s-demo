name             'openvas'
maintainer       "Ben Pick"
maintainer_email "ben.pick@coveros.com"
license          "All Rights Reserved"
description      "Install policy analysis tool, openvas, into our application"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "install openscap"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.0.5"

supports 'ubuntu'
# 0.0.5 - Customized post install scripts. SLOW installation, but works. 
# 0.0.4 - Installs software from mrazavi's repo. No tar files downloaded. 
# 0.0.3 - This cookbook is in progress. It installs without errors, but is missing the last key step
# 0.0.2 - adding berks dependency for ark
# 0.0.1 - initial setup

depends 'ark'
depends 'apt'
