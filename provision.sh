#!/bin/bash

# NOTES: current ec2 instance i-0b8c973d220d67710

## Creates the cloud formation template specified by MDAS_CFT.json
## Expected usage: ./create-cft NAME REGION SSH_KEY OKD_AMI VPN_AMI
## Must be run from the infrastructure/CI directory

# Get command line arguments. Maybe do something fancier with this idk
NAME='SecureCIK8SDemoStack'
REGION=us-east-1
SSH_KEY=bob.rosenburgh1
OKD_AMI=ami-026c8acd92718196b
VPN_AMI=vpc-0669d1fe44fc186d1
EIP_ADDRESS=52.203.157.75

# get vpc id and cidr range
echo 'Get Default VPC ID...'
VPC_ID=$(aws ec2 describe-vpcs --query 'Vpcs[?IsDefault].VpcId' --output text)
echo 'Get Default CidrBlock...'
CIDR_BLOCK=$(aws ec2 describe-vpcs --query 'Vpcs[?IsDefault].CidrBlock' --output text | grep -Eo '^[[:digit:]]{1,3}\.[[:digit:]]{1,3}')

#  get ig id
echo 'Get Internet Gateway ID...'
IG_ID=$(aws ec2 describe-internet-gateways --query 'InternetGateways[].InternetGatewayId' --output text)

# Detach main ig from the primary vpc
# aws ec2 detach-internet-gateway --internet-gateway-id $IG_ID --vpc-id $VPC_ID

# Import AWS AWS cert
# aws acm import-certificate --certificate file://teamawesomemdaschallenge.net.pub.pem --private-key file://teamawesomemdaschallenge.net.pem --certificate-chain file://teamawesomemdaschallenge.net.chain.pem
# CERT_ARN=$(aws acm list-certificates --query 'CertificateSummaryList[].CertificateArn' --output text)

# Create the Cloud formation template
# base64 encoding options vary by system
#
# ParameterKey="ALBCertArn",ParameterValue="$CERT_ARN" \
CREATE_STACK=$(aws cloudformation create-stack --capabilities CAPABILITY_NAMED_IAM --stack-name $NAME --template-body file://SECURECI_K8S_DEMO.json --parameters \
  ParameterKey="InstanceType",ParameterValue="t2.large" \
  ParameterKey="AMIId",ParameterValue="$OKD_AMI" \
  ParameterKey="EIPAddress",ParameterValue="$EIP_ADDRESS" \
  ParameterKey="KeyName",ParameterValue="$SSH_KEY")
  # ParameterKey="UserData",ParameterValue=$(base64 demo-master-user-data.sh))

  # ParameterKey="CIDROctets",ParameterValue="$CIDR_BLOCK" \
  # ParameterKey="VPCId",ParameterValue="$VPC_ID" \
  #
  #
  # ParameterKey="InstanceType",ParameterValue="t2.large" \
  # ParameterKey="VPNAMIId",ParameterValue="$VPN_AMI" \
  # ParameterKey="AvailabilityZone1",ParameterValue="${REGION}a" \
  # ParameterKey="VPCRegion",ParameterValue="$REGION" \

# base64 encoding options vary by system - try again if the above flag fails
# if [ $? -ne 0 ]; then
#   echo "retrying base64 encoding for user data"
#   CREATE_STACK=$(aws cloudformation create-stack --capabilities CAPABILITY_NAMED_IAM --stack-name $NAME --template-body file://MDAS_CFT.json --parameters \
#     ParameterKey="CIDROctets",ParameterValue="$CIDR_BLOCK" \
#     ParameterKey="VPCId",ParameterValue="$VPC_ID" \
#     ParameterKey="AMIId",ParameterValue="$OKD_AMI" \
#     ParameterKey="KeyName",ParameterValue="$SSH_KEY" \
#     ParameterKey="ALBCertArn",ParameterValue="$CERT_ARN" \
#     ParameterKey="InstanceType",ParameterValue="m5d.large" \
#     ParameterKey="VPNAMIId",ParameterValue="$VPN_AMI" \
#     ParameterKey="AvailabilityZone1",ParameterValue="${REGION}a" \
#     ParameterKey="AvailabilityZone2",ParameterValue="${REGION}b" \
#     ParameterKey="AvailabilityZone3",ParameterValue="${REGION}c" \
#     ParameterKey="VPCRegion",ParameterValue="$REGION" \
#     ParameterKey="UserData",ParameterValue=$(base64 -w0 openshift-master-user-data.sh))
# fi

echo "Creating stack..."
# wait for the cft to finish creating before moving on
aws cloudformation wait stack-create-complete --stack-name $NAME
