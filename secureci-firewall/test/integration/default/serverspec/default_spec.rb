require 'spec_helper'
def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

describe 'secureci-firewall::default' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html
  describe command("cat /etc/firewall/secureci.fw | grep 'files:'") do
  	its(:stdout) { should contain    'secureci'}
  	its(:exit_status) { should eq 0 }
  end
end
