#
# Cookbook Name:: secureci-firewall
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

#TODO: This needs to be replaced with the supermarket iptables cookbook
#####################
# Install firewall. #
#####################
directory node['secureci-firewall']['home'] do
  owner node['secureci-firewall']['owner']
  group node['secureci-firewall']['group']
  recursive true
  mode 00755
end
#install_file /etc/firewall/secureci.fw +x
cookbook_file "secureci.fw" do
  path "#{node['secureci-firewall']['home']}/secureci.fw"
  action :create_if_missing
  mode 0755
end
