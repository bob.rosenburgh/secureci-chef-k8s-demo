#
# Cookbook Name:: secureci-firewall
# Spec:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'secureci-firewall::default' do
	let :chef_run do
		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
	end

	it 'converges successfully' do
		chef_run.converge(described_recipe)
		expect { chef_run }.to_not raise_error
	end


	describe 'openssl::firewall' do
		let :chef_run do
			ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
		end

		it 'creates a dir' do
			chef_run.converge(described_recipe)
			expect(chef_run).to create_directory(chef_run.node['firewall']['home']).with(
				owner:     chef_run.node['openssl']['owner'],
				group:     chef_run.node['openssl']['group'],
				recursive: true
				)
		end 
		it 'renders cookbook file secureci.fw' do
			expect(chef_run).to create_cookbook_file_if_missing('secureci.fw').with(
				path:    '/etc/firewall/secureci.fw')
		end
	end
	at_exit { ChefSpec::Coverage.report! }
end
