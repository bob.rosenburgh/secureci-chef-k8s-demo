require 'spec_helper'

describe 'secureci-cleanup::default' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html
  describe file('/root/distribute_setup.py') do
  	it { should_not exist }
  end
  describe file('/etc/issue-standard') do 
  	it { should exist } 
  end 
  describe file('/etc/rc.local') do 
  	it { should exist }
  end 
  describe command("update-rc.d set-external-ip defaults") do 
  	its(:stdout) { should contain("System start/stop links for /etc/init.d/set-external-ip already exist")}
  	its(:exit_status) { should eq 0 }
  end
end
