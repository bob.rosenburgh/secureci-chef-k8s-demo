#! /usr/bin/perl

use strict;

my $ifconfig = '/sbin/ifconfig';
open(IFCONFIG, "$ifconfig |") or die "Cannot run $ifconfig: $!\n";

my %ips;
while (<IFCONFIG>) {
  if (my($if) = /^(eth\S+)/) {
    while (<IFCONFIG>) {
      if (my($ip) = /\binet addr:(\d{1,3}(\.\d{1,3}){3})/) {
      	$ips{$if} = $ip unless ($ip =~ /^127\.0+\.0+\.0*1$/);
      	last;
      }
    }
  }
}

my @ips;
foreach my $if (sort keys %ips) {
	push(@ips, $ips{$if});
}

if (@ips) {
	print successMessage(@ips);
	exit 0;
} else {
  print notFoundMessage();
  exit 1;
}

sub successMessage {
  my($ip, @otherIps) = @_;
	my $msg =<<"END";
You can connect to the web interface by using 
your browser to visit:

    https://$ip/

END

	if (0 < @otherIps) {
	  $msg .= 'This server also listens on ' . join(', ', @otherIps) . ".\n";
	}
	return $msg;
}

sub notFoundMessage {
	return <<"END";
The external IP address of this SecureCI server 
could not be found.

Please make sure that networking is on and
connected, and then restart the virtual machine.

END
}

1;