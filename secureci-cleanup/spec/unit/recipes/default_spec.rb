#
# Cookbook Name:: secureci-cleanup
# Spec:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'secureci-cleanup::default' do
	let :chef_run do
		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
	end

	it 'converges successfully' do
		chef_run.converge(described_recipe)
		expect { chef_run }.to_not raise_error
	end
	before(:each) do
		allow(File).to receive(:exists?).with(anything).and_call_original
		allow(File).to receive(:exists?).with('/etc/init.d/gitblit').and_return true 
		allow(File).to receive(:exists?).with('/etc/init.d/jenkins').and_return true

		allow(File).to receive(:directory?).with(anything).and_call_original
		allow(File).to receive(:directory?).with('/usr/lib/jvm/java-8-oracle-amd64').and_return true
		allow(File).to receive(:directory?).with('/opt/nexus-3.2.0-01').and_return true
		allow(File).to receive(:directory?).with('/var/lib/tomcat7').and_return true
		stub_command("test -L /etc/init.d/").and_return('Syntax OK')
		stub_command("test -L /etc/init.d/sonar").and_return('Syntax OK')
	end
	it 'should enforce firstrun execution' do
		expect(chef_run).to create_cookbook_file('/etc/init.d/show-ipaddr.pl').with(
			mode:   '0755',
			owner:  'root',
			group:  'root',
			)
	end
	it 'should execute "copy issue file"' do 
		expect(chef_run).to run_execute('cp /etc/issue /etc/issue-standard')
	end
	it 'should create cookbook file' do 
		expect(chef_run).to create_cookbook_file('/etc/rc.local')
	end
	it 'should execute apt-get-autoremove' do
		expect(chef_run).to run_execute('apt-get --assume-yes autoremove')
	end
	it 'should execute ruby block' do 
		expect(chef_run).to run_ruby_block('update-alternatives')
	end
	it 'should create remote file' do 
		expect(chef_run).to create_remote_file('/root/distribute_setup.py').with(
			source: "https://svn.apache.org/repos/asf/oodt/tools/oodtsite.publisher/trunk/distribute_setup.py"
			)
	end
	it 'should remove remote file' do 
		expect(chef_run).to run_execute('/root/python distribute_setup.py; rm -f /root/distribute_setup.py')
	end
	at_exit { ChefSpec::Coverage.report! }
end
