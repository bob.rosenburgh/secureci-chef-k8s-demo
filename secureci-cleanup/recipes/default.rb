#
# Cookbook Name:: initialize
# Recipe:: cleanup
# Author:: Max Saperstone
#
# Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe "apt"

#cleanup distribute_setup
remote_file "/root/distribute_setup.py" do
  source "https://svn.apache.org/repos/asf/oodt/tools/oodtsite.publisher/trunk/distribute_setup.py"
end
execute "remove distribute_setup.py" do
  command "/root/python distribute_setup.py; rm -f /root/distribute_setup.py"
end

#setup our init files
cookbook_file "show-ipaddr.pl" do
  path "/etc/init.d/show-ipaddr.pl"
  mode '0755'
  owner 'root'
  group 'root'
  action :create
end

#setup our initial scripts
execute "copy issue file" do
  command "cp /etc/issue /etc/issue-standard"
end
cookbook_file "rc.local" do
  path "/etc/rc.local"
  action :create
end

#clean up installs
execute "apt-get-autoremove" do
  command "apt-get --assume-yes autoremove"
  ignore_failure true
end

ruby_block "update-alternatives" do
    block do
        run_context.include_recipe 'java_oracle::update-alternatives'
    end
    action :run
    only_if { ::File.directory?("/usr/lib/jvm/java-8-oracle-amd64") }
end

######################################
## ensure on restart we keep our ip ##
######################################
template "/usr/bin/set-external-ip" do
  source "set-external-ip.erb"
  mode '0755'
  owner node['openssl']['user']
  group node['openssl']['group']
end
template "/etc/systemd/system/set-external-ip.service" do
  source "systemd_external_ip.erb"
  mode '0755'
  owner node['openssl']['user']
  group node['openssl']['group']
end

# TODO: confirm with Glenn. Error on run. See https://askubuntu.com/questions/1029531/how-to-setup-a-static-ip-on-ubuntu-server-18-04
# service 'set-external-ip' do
#   action :enable
#   notifies :restart, 'service[set-external-ip]', :delayed
# end
