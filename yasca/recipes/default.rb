#
# Cookbook Name:: yasca
# Recipe:: default
# Author:: Ben Pick
#
# Copyright 2015, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe "zip"
include_recipe "ark"

user node['yasca']['user'] do
  system true
  shell "/bin/false"
  action :create
end

directory node['yasca']['base'] do
  owner node['yasca']['user']
  group node['yasca']['group']
  recursive true
  mode 00755
end

directory node['yasca']['home'] do
  owner node['yasca']['user']
  group node['yasca']['group']
  mode 0755
  action :create
end

# #download and unzip the yasca installation file from sourceforge.net
# # Errors when using ark to install. It extracted most of the files to the top level
# # This broke the references. Needs troubleshooting. 
#  ark 'yasca' do
#    url "#{node['yasca']['url']}"
#    #extension "zip"
#    version "#{node['yasca']['version']}"
#    owner node['yasca']['user']
#    group node['yasca']['group']
#    mode 0755
#    prefix_home "#{node['yasca']['base']}"
#    action :install 
#  end


# Instead, use git to clone repository

yascazip = "yasca-core-#{node['yasca']['version']}.zip"
# download and unzip our yasca file
remote_file "#{node['yasca']['home']}/#{yascazip}" do
  source node['yasca']['url']
  owner node['yasca']['user']
  group node['yasca']['group']
  mode 0755
  action :create_if_missing
  not_if { ::File.exists?("#{node['yasca']['home']}/yasca-core-#{node['yasca']['version']}.zip") }
end
execute "unzip #{yascazip}" do
  cwd node['yasca']['home']
  command "unzip #{yascazip}"
  user "root"
  action :run
  not_if { ::File.directory?("#{node['yasca']['home']}/yasca") }
end
execute "remove #{yascazip}" do
  cwd node['yasca']['home']
  command "rm -rf #{yascazip}"
  user "root"
  action :run
end
execute "chown #{yascazip}" do
  cwd node['yasca']['base']
  command "chown -R #{node['yasca']['user']}:#{node['yasca']['group']} yasca"
  user "root"
  action :run
end



directory node['yasca']['output'] do
  owner node['yasca']['user']
  group node['yasca']['group']
  mode 0777
  action :create
end

# Installs required library
package node['installations']['yasca'] do
  action :install
end

cookbook_file "#{node['yasca']['home']}/yasca" do
  source "yasca.erb"
  mode 0755
  owner node['yasca']['user']
  group node['yasca']['group']
end

cookbook_file "#{node['yasca']['home']}/plugins/Grep.php" do
  source "Grep.erb"
  mode 0755
  owner node['yasca']['user']
  group node['yasca']['group']
end

cookbook_file "#{node['yasca']['home']}/checkResults.py" do
  source "checkResults.erb"
  mode 0755
  owner node['yasca']['user']
  group node['yasca']['group']
end
