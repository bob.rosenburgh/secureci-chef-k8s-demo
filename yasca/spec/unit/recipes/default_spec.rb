require 'spec_helper'

 describe 'yasca::default' do
 let :chef_run do
   ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '14.04')
 end

  it 'should change ownership of "/opt"' do
    chef_run.converge(described_recipe)
    expect(chef_run).to_not create_directory('/opt').with(
    owner: 'yasca',
    group: 'www-data',
    mode:  '00755')
  end
  it 'should change ownership of "/opt/yasca"' do
    chef_run.converge(described_recipe)
    expect(chef_run).to create_directory('/opt/yasca').with(
    owner: 'yasca',
    group: 'www-data',
    mode:  '0755')
  end
  it 'should change ownership of "/opt/yasca/YascaScanResults"' do
    chef_run.converge(described_recipe)
    expect(chef_run).to create_directory('/opt/yasca/YascaScanResults').with(
    owner: 'yasca',
    group: 'www-data',
    mode:  '0777')
  end
  it 'unzips yasca' do
    chef_run.converge(described_recipe)
    expect(chef_run).to run_execute('unzip yasca-core-2.21.zip')
  end
  it 'removes yasca zip' do
    chef_run.converge(described_recipe)
    expect(chef_run).to run_execute('rm -rf yasca-core-2.21.zip')
  end
  it 'runs chown on yasca' do
    chef_run.converge(described_recipe)
    expect(chef_run).to run_execute('chown -R yasca:www-data yasca')
  end
  it 'installs php5-cli' do
    chef_run.converge(described_recipe)
    expect(chef_run).to install_package('php5-cli')
  end
at_exit { ChefSpec::Coverage.report! }
end
