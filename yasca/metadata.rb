name             'yasca'
maintainer       "Ben Pick"
maintainer_email "ben.pick@coveros.com"
license          "All Rights Reserved"
description      "Install static code analysis tool, yasca, into our application"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "install yasca"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.0.9"

supports 'ubuntu'
# 0.0.9 - food critic fixes
# 0.0.8 - removed jenkins dependency
# 0.0.7 - made jenkins user dependency more dynamic
# 0.0.6 - more Jenkins customizations needed
# 0.0.5 - Jenkins customizations needed
# 0.0.4 - revised cookbook to work with Jenkins 
# 0.0.3 - ark broke the cookbook. commented out scripts and replaced with manual install
# 0.0.2 - adding berks dependency for ark
# 0.0.1 - initial setup

depends 'zip'
depends 'ark'
