default['yasca']['user'] = 'yasca'
default['yasca']['group'] = 'www-data'

default['yasca']['version'] = '2.21'
default['yasca']['url'] = "http://sourceforge.net/projects/yasca/files/Yasca%202.x/Yasca%202.2/yasca-core-#{node['yasca']['version']}.zip"

default['yasca']['base'] = '/opt'
default['yasca']['home'] = "#{node['yasca']['base']}/yasca"
default['yasca']['output'] = "#{node['yasca']['home']}/YascaScanResults"

default['installations']['yasca'] = 'php5-cli'