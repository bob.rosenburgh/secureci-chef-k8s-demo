#!/bin/bash
wget https://sonatype-download.global.ssl.fastly.net/nexus/3/nexus-3.2.0-01-unix.tar.gz
mv nexus-3.2.0-01-unix.tar.gz nexus-3.2.0-01.tar.gz
wget https://src.fedoraproject.org/repo/pkgs/openscap/openscap-1.2.11.tar.gz/4cac5357617aa94fa697a8ddaedf8860/openscap-1.2.11.tar.gz
wget https://sonarsource.bintray.com/Distribution/sonarqube/sonarqube-6.1.zip
wget https://s3.amazonaws.com/securecibuild/tool.tar.gz
wget https://sourceforge.net/projects/checkstyle/files/checkstyle/7.2/checkstyle-7.2-all.jar
wget http://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.0.jar
wget https://github.com/junit-team/junit/releases/download/r4.12/junit-4.12.jar
wget https://sourceforge.net/projects/findbugs/files/findbugs/3.0.1/findbugs-3.0.1.zip
wget http://www.clarkware.com/software/jdepend-2.9.1.zip
wget https://sourceforge.net/projects/pmd/files/pmd/5.5.2/pmd-bin-5.5.2.zip
wget http://selenium-release.storage.googleapis.com/2.53/selenium-java-2.53.1.zip
wget https://s3.amazonaws.com/SecureCI_Tools/secureci-testing-framework-1.3.0.zip
wget http://central.maven.org/maven2/org/testng/testng/6.9.13.6/testng-6.9.13.6.jar
wget https://sourceforge.net/projects/cobertura/files/cobertura/2.1.1/cobertura-2.1.1-bin.tar.gz
wget http://sourceforge.net/projects/yasca/files/Yasca%202.x/Yasca%202.2/yasca-core-2.21.zip