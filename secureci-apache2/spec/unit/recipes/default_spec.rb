#
# Cookbook Name:: secureci-apache2
# Spec:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'secureci-apache2::default' do
	context 'When all attributes are default, on an unspecified platform' do
		let :chef_run do
			ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
		end


		it 'converges successfully' do
			chef_run.converge(described_recipe)
			expect { chef_run }.to_not raise_error
		end


		before do  
			stub_command("/usr/sbin/apache2 -t").and_return('Syntax OK')
			stub_command("test -L /usr/share/ca-certificates/secureci.crt").and_return('Syntax OK')

			allow(File).to receive(:exists?).with(anything).and_call_original
			allow(File).to receive(:exists?).with("/etc/apache2/ssl/secureci.crt").and_return(true)
		end 

		let :chef_run do
			ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
		end
		let :owner do 
			chef_run.node['openssl']['user']
		end
		let :group do
			chef_run.node['openssl']['group']
		end
		let :apache_dir do
			"#{chef_run.node['apache']['dir']}/sites-available"
		end

		it 'installs apache2-utils' do 
			expect(chef_run).to install_package('apache2-utils')
		end
		it 'should change ownership of "ssl"' do
			expect(chef_run).to_not create_directory(chef_run.node['openssl']['home']).with(
				owner: owner,
				group: group,
				mode:  '00755')
		end
		it 'creates a template with attributes' do
			expect(chef_run).to create_template('/var/www/html/index.html').with(
				source: 'index.html.erb',
				mode:   '0777',
				owner:  owner,
				group:  group,
				) 
		end
		it 'creates a template with attributes' do
			expect(chef_run).to create_template("#{apache_dir}/default.conf").with(
				source: 'default.erb',
				owner:  owner,
				group:  group,
				) 
		end
		it 'creates a template with attributes' do
			expect(chef_run).to create_template("#{apache_dir}/ssl.conf").with(
				source: 'ssl.erb',
				owner:  owner,
				group:  group,
				) 
		end
	end
	at_exit { ChefSpec::Coverage.report! }
end
