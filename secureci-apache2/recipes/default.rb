#
# Cookbook Name:: secureci-apache2
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.
include_recipe "aptupdate"
include_recipe "apache2"
include_recipe "apache2::mod_proxy_http"
include_recipe "apache2::mod_proxy_ajp"
include_recipe "apache2::mod_proxy"
include_recipe "apache2::mod_ssl"
include_recipe "apache2::mod_rewrite"
include_recipe "openssl"

#allow user create
package "apache2-utils" do
  action :install
end

package "python-pip" do
  action :purge
end

package "python-setuptools" do
  action :install
end

package "python-mysqldb" do
  action :install
end

package "python-subversion" do
  action :install
end

package "python-babel" do
  action :install
end

package "libapache2-mod-python" do
  action :install
end
package "python-pip" do
  action :install
end



################################
# Enable HTTPS for web server. #
################################
#mkdir -p /etc/apache2/ssl

#setting up site forwarding
template "#{node['apache']['docroot_dir']}/index.html" do
  source "index.html.erb"
  mode '0777'
  owner node['secureci-apache2']['user']
  group node['secureci-apache2']['group']
end

#setup our http site
template "#{node['apache']['dir']}/sites-available/default.conf" do
  source "default.erb"
  mode '0755'
  owner node['secureci-apache2']['user']
  group node['secureci-apache2']['group']
end
#a2ensite default
web_app 'default' do
  template 'default.erb'
  notifies 'restart[apache2]'
end

#setup our https site
template "#{node['apache']['dir']}/sites-available/ssl.conf" do
  source "ssl.erb"
  mode '0755'
  owner node['secureci-apache2']['user']
  group node['secureci-apache2']['group']
end

template "#{node['apache']['dir']}/ports.conf" do
  source "ports.conf.erb"
  mode '0755'
  owner node['secureci-apache2']['user']
  group node['secureci-apache2']['group']
end

execute "copy_ssl_key" do
	command "cp #{node['openssl']['key']} #{node['secureci-apache2']['key']}"
	notifies :restart, 'service[apache2]', :delayed
end

execute "copy_ssl_crt" do
	command "cp #{node['openssl']['crt']} #{node['secureci-apache2']['crt']}"
	notifies :restart, 'service[apache2]', :delayed
end

#a2ensite ssl
web_app 'ssl' do
  template 'ssl.erb'
  notifies 'restart[apache2]'
end
