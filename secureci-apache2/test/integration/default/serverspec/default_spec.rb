require 'spec_helper'

def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

describe 'secureci-apache2::default' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html
  describe file("/etc/apache2/ssl") do
  	it { should be_owned_by node['default']['secureci-apache2']['user'] }
  	it { should be_grouped_into node['default']['secureci-apache2']['group'] }
  end
  describe file("/etc/apache2/ssl/secureci.key") do
  	it { should exist }
  	it { should be_owned_by node['default']['secureci-apache2']['user'] }
  	it { should be_grouped_into node['default']['secureci-apache2']['group'] }
  	
  	it { should be_readable.by(:owner) }
  	it { should be_readable.by(:group) }
  	it { should be_readable.by(:others) }
  	
  	it { should     be_writable.by(:owner) }
  	it { should_not be_writable.by(:group) }
  	it { should_not be_writable.by(:others) }
  	
  	it { should_not be_executable.by(:owner) }
  	it { should_not be_executable.by(:group) }
  	it { should_not be_executable.by(:others) }  
  end
  describe command ("netstat -atn |grep '80'") do
  	its(:stdout) { should contain  'LISTEN'}
  end
  describe port('80') do
  	it { should be_listening }
  end
  describe service("apache2") do
  	it { should be_enabled }
  	it { should be_running }
  end
  describe command ("cat /etc/apache2/sites-available/ssl.conf | grep 'secureci'") do
  	its(:stdout) {should contain 'secureci'}
  end
end
