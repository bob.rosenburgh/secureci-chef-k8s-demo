#
# Cookbook Name:: secureci-hostinfo
# Spec:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'secureci-hostinfo::default' do
	let :chef_run do
		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
	end

	it 'converges successfully' do
		chef_run.converge(described_recipe)
		expect { chef_run }.to_not raise_error
	end
	it 'should execute changing hostname' do 
		expect(chef_run).to run_execute("sudo hostname #{hostname}")
	end
	it 'should make hostname change permanent' do
		expect(chef_run).to create_template('/etc/hostname').with(
			source: 'hostname.erb',
			mode:   '0644',
			owner:  'root',
			group:  'root',
			) 
	end
	it 'add hostname to hosts' do
		expect(chef_run).to create_template('/etc/hosts').with(
			source: 'hosts.erb',
			mode:   '0644',
			owner:  'root',
			group:  'root',
			) 
	end
	it 'should setup set-external-ip' do
		expect(chef_run).to create_template('/usr/bin/set-external-ip').with(
			source: 'set-external-ip.erb',
			mode:   '0755',
			owner:  openssl_user,
			group:  openssl_group,
			) 
	end
	it 'should setup systemd-external-ip' do
		expect(chef_run).to create_template('/etc/systemd/system/set-external-ip.service').with(
			source: 'systemd_external_ip.erb',
			mode:   '0755',
			owner:  openssl_user,
			group:  openssl_group,
			) 
	end
	it 'executes restart on set-external-ip' do
		expect(chef_run).to enable_service('set-external-ip')
	end
end

at_exit { ChefSpec::Coverage.report! }