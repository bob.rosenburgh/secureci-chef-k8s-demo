#
# Cookbook Name:: secureci-hostinfo
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.
#set our hostname

execute 'changing hostname' do
  command "sudo hostname #{node['secureci']['hostname']}"
end

#make our hostname change permanent
template "/etc/hostname" do
  source "hostname.erb"
  mode '0644'
  owner 'root'
  group 'root'
end

#fix our hostname, by adding it to hosts
template "/etc/hosts" do
  source "hosts.erb"
  mode '0644'
  owner 'root'
  group 'root'
end

#capture our public IP
ipv4 =  Mixlib::ShellOut.new("ec2metadata | grep 'public-ipv4:' | cut -d ' ' -f 2")
#ipv4 = `ec2metadata | grep 'public-ipv4:' | cut -d ' ' -f 2`
if (ipv4 == "unavailable" or ipv4 == "") 
  ipv4 = Mixlib::ShellOut.new("ec2metadata | grep 'local-ipv4:' | cut -d ' ' -f 2")
end
if (ipv4 == "unavailable" or ipv4 == "")
  ipv4 = Mixlib::ShellOut.new("ifconfig | grep Bcast | cut -d ':' -f 2 | cut -d ' ' -f 1")
end
node.default['ipv4'] = ipv4
