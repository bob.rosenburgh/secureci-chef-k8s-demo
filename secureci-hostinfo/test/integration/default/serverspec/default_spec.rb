require 'spec_helper'

def node
	JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

describe 'secureci-hostinfo::default' do


	hostname  = node['default']['secureci']['hostname']


	describe command("hostname") do
		its(:stdout) { should contain("#{hostname}")}
	end
end
