require 'spec_helper'

describe 'sonar::default' do

 before do
  stub_command('/usr/sbin/apache2 -t').and_return('Syntax OK')
  stub_command("test -L /opt/sonar").and_return('Syntax OK')
  stub_command("test -L /etc/init.d/sonar").and_return(true)
  stub_command('/usr/sbin/httpd -t').and_return('Syntax OK')
  stub_command("test -L /var/lib/sonar").and_return('Syntax OK')
  stub_command("test -L /usr/share/ca-certificates/secureci.crt").and_return('Syntax OK')
end

let :chef_run do
  ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
end

let :version do
 chef_run.node['sonar']['version']
end 
let :conf do
 chef_run.node['sonar']['conf']
end
templates = %w{ sonar.properties wrapper.conf }

it 'creates sonar user' do 
  chef_run.converge(described_recipe)
  expect(chef_run).to create_user "sonar"
end
it 'installs sonar via ark' do
 expect(chef_run).to put_ark "sonar-#{version}"
end
it 'should create link' do 
 link = chef_run.link(chef_run.node['sonar']['home_link']) 
 expect(link).to link_to chef_run.node['sonar']['home'] 
end

it 'includes cookbook initialize::zip' do
  expect(chef_run).to include_recipe('initialize::zip')
end
it 'includes cookbook java_oracle' do
  expect(chef_run).to include_recipe('java_oracle')
end
it 'includes cookbook mysql' do
  expect(chef_run).to include_recipe('secureci-mysql')
end
it 'create template file create-sonar-db.sql' do
  expect(chef_run).to create_template('/root/create-sonar-db.sql').with(
    source: 'create-sonar-db.sql.erb',
    ) 
end
it 'run mysql script' do
 expect(chef_run).to run_execute("mysql -S #{socket} --user=root --password=#{password} < /root/create-sonar-db.sql")
end
it 'should delete a file' do 
 expect(chef_run).to delete_file('/root/create-sonar-db.sql')
end
it 'should set the default user to "sonar"' do
  expect(chef_run.node['sonar']['user']).to eq('sonar')
end
templates.each do |template|
  it "should configure sonar #{template}" do
    expect(chef_run).to create_template("#{conf}/#{template}").with(
      source: "#{template}.erb",
      mode: '0644',
      owner: chef_run.node['sonar']['user'],
      group: chef_run.node['sonar']['group']
      )
  end
end
it 'should set the base dir user/group' do
  base_dir = chef_run.node['sonar']['base']
  expect(chef_run).to create_directory base_dir
end
it 'sets sonar startup on boot' do
 expect(chef_run).to run_execute('update-rc.d sonar defaults')
end
it 'executes restart on sonar' do
  expect(chef_run).to restart_service('sonar')
end
end 
describe 'sonar::plugins' do

  let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
  end
  plugins = %w{ build-breaker checkstyle clirr java motion-chart pmd sonargraph timeline useless-code-tracker }

  it 'should create dir' do 
    chef_run.converge(described_recipe)
    expect(chef_run).to create_directory(chef_run.node['sonar']['downloads']).with(
      owner: chef_run.node['sonar']['user'] ,
      group: chef_run.node['sonar']['group']
      )
  end
  plugins.each do |plugin|
    it "should install plugin #{plugin}" do
      plug = plugin << "-#{chef_run.node['sonar']['plugins'][plugin]['version']}"
      expect(chef_run).to create_remote_file_if_missing("#{chef_run.node['sonar']['downloads']}/#{plug}.jar")
    end
  end

  at_exit { ChefSpec::Coverage.report! }
end
