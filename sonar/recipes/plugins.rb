#
# Cookbook Name:: sonar
# Recipe:: plugins
# Author:: Max Saperstone
#
# Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#no point in installing these plugins if java_tools aren't put on there
#include_recipe "java_tools"

#setup plugins
directory node['sonar']['downloads'] do
  owner node['sonar']['user']
  group node['sonar']['group']
  recursive true
  mode 00755
end
#get some jar files
node['sonar']['plugins'].each do |plugin, vals|
# for plugin in node['sonar']['plugins'] do
  remote_file "#{node['sonar']['downloads']}/#{plugin}-#{vals['version']}.jar" do
    source vals['url']
    owner node['sonar']['user']
    group node['sonar']['group']
    mode 0755
    action :create_if_missing
    not_if { ::File.exists?("#{node['sonar']['downloads']}/#{plugin}-#{vals['version']}.jar")}
  end
end

service "sonar" do
  action :restart
end
