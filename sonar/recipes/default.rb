#
# Cookbook Name:: sonar
# Recipe:: default
# Author:: Max Saperstone
#
# Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe "zip"
include_recipe "java_oracle"
include_recipe "secureci-mysql"

user node['sonar']['user'] do
  system true
  shell "/bin/false"
  action :create
end

directory node['sonar']['base'] do
  owner node['sonar']['user']
  group node['sonar']['group']
  recursive true
  mode 00755
end

#install sonar
ark "sonar-#{node['sonar']['version']}" do
  url node['sonar']['url']
  version node['sonar']['version']
  path node['sonar']['base']
  home_dir node['sonar']['home_link']
  owner node['sonar']['user']
  group node['sonar']['group']
  action :put
end

#make soft link from nexus home
link node['sonar']['home_link'] do
  action :delete
  only_if "test -L #{node['sonar']['home_link']}"
end
link node['sonar']['home_link'] do
  to node['sonar']['home']
  action :create
end

##need to setup sonar as a service
#service="/etc/init.d/#{node['sonar']['service']}"
#link service do
#  action :delete
#  only_if "test -L #{service}"
#end
#link service do
#  to node['sonar']['startup']
#  action :create
#end
##add sonar startup on boot
#execute "add sonar startup on boot" do
#  command "update-rc.d #{node['sonar']['service']} defaults" 
#end

#configure sonar
template "#{node['sonar']['home']}/conf/sonar.properties" do
  source "sonar.properties.erb"
  mode '0644'
  owner node['sonar']['user']
  group node['sonar']['group']
end
template "#{node['sonar']['home']}/conf/wrapper.conf" do
  source "wrapper.conf.erb"
  mode '0644'
  owner node['sonar']['user']
  group node['sonar']['group']
end

#configure sonar using systemd (experimental)
template "/etc/systemd/system/sonar.service" do
  source "sonar.service.erb"
  mode '0644'
  owner 'root'
  owner 'root'
end

template node['sonar']['startup'] do
  source "sonar.sh.erb"
  mode '0744'
  owner node['sonar']['user']
  group node['sonar']['group']
end

#should enable sonar using systemd
systemd_unit 'sonar.service' do
  triggers_reload true
  action [:enable, :restart]
end

template "create-sonar-db.sql" do
  path File.join('/root/','create-sonar-db.sql')
  action :create
end

execute "secureci sonar db" do
  command "mysql -S #{node['mysql']['socket']} --user=root --password=#{node['mysql']['server_root_password']} < /root/create-sonar-db.sql"
end

file "create-dbs.sql" do
  path File.join('/root/','create-sonar-db.sql')
  action :delete
end


service "sonar" do
  action :restart
end

include_recipe "sonar::plugins"
