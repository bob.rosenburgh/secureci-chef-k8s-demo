name             "sonar"
maintainer       "Max Saperstone"
maintainer_email "max.saperstone@coveros.com"
license          "All Rights Reserved"
description      "Install sonar into our application"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "install sonar"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.1.6"

supports 'ubuntu'

depends 'ark'
depends 'zip'
depends 'java_oracle'
depends 'apache2'
depends 'secureci-mysql'
depends 'poise-python'

# 0.1.7 - updating unit test + removed openssl from recipe
# 0.1.6 - food critic fixes
# 0.1.5 - fixing plugins to be 6.1 compatible
# 0.1.4 - fixing home links, and modifying install to use ark
# 0.1.3 - upgrading to sonar 6.1
# 0.1.2 - upgrading to sonar 5.1.2
# 0.1.1 - upgrading to sonar 5.1.1
# 0.1.0 - upgrading to sonar 5.0.1
# 0.0.10 - minor update to add port selection for sonar.
# 0.0.9 - updated source location for plugins to central.maven.org
# 0.0.8 - added in database attribute information
# 0.0.7 - split out plugins into separate recipe
# 0.0.6 - remove tomcat7
# 0.0.5 - added in sonar service
# 0.0.4 - added in configuration templates
# 0.0.3 - added in plugins
# 0.0.2 - initial setup, still needs configs for mysql and wrapper
