default['sonar']['user'] = 'sonar'
default['sonar']['group'] = 'www-data'

default['sonar']['jdbc']['user'] = 'sonar'
default['sonar']['jdbc']['pass'] = 'sonar'

default['sonar']['version'] = '6.7.6'
default['sonar']['url'] = "https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-#{node['sonar']['version']}.zip"

default['sonar']['base'] = '/opt'
default['sonar']['home'] = "#{node['sonar']['base']}/sonar-#{node['sonar']['version']}"
default['sonar']['conf'] = "#{node['sonar']['home']}/conf"

default['sonar']['downloads'] = "#{node['sonar']['home']}/extensions/downloads"
default['sonar']['startup'] = "#{node['sonar']['home']}/bin/linux-x86-64/sonar.sh"

default['sonar']['service'] = "sonar"
default['sonar']['port'] = "9000"
default['sonar']['home_link'] = "/var/lib/sonar"

##############################
#############database attributes
##############################
default['sonar']['database']['name'] = 'sonar'
default['sonar']['database']['user'] = 'sonar'
default['sonar']['database']['password'] = 'sonar'
default['sonar']['database']['domain'] = 'localhost'

###############
######plugins
###############
#Build Breaker
default['sonar']['plugins']['build-breaker']['version'] = '2.1'
default['sonar']['plugins']['build-breaker']['url'] = "https://github.com/SonarQubeCommunity/sonar-build-breaker/releases/download/#{node['sonar']['plugins']['build-breaker']['version']}/sonar-build-breaker-plugin-#{node['sonar']['plugins']['build-breaker']['version']}.jar"
#Checkstyle
default['sonar']['plugins']['checkstyle']['version'] = '3.4'
default['sonar']['plugins']['checkstyle']['url'] = "https://github.com/checkstyle/sonar-checkstyle/releases/download/#{node['sonar']['plugins']['checkstyle']['version']}/checkstyle-sonar-plugin-#{node['sonar']['plugins']['checkstyle']['version']}.jar"
#Clirr
default['sonar']['plugins']['clirr']['version'] = '1.2'
default['sonar']['plugins']['clirr']['url'] = "http://central.maven.org/maven2/org/codehaus/sonar-plugins/sonar-clirr-plugin/#{node['sonar']['plugins']['clirr']['version']}/sonar-clirr-plugin-#{node['sonar']['plugins']['clirr']['version']}.jar"
#Java
default['sonar']['plugins']['java']['version'] = '4.4.0.8066'
default['sonar']['plugins']['java']['url'] = "https://binaries.sonarsource.com/Distribution/sonar-java-plugin/sonar-java-plugin-#{node['sonar']['plugins']['java']['version']}.jar"
#Motion Chart
default['sonar']['plugins']['motion-chart']['version'] = '1.7'
default['sonar']['plugins']['motion-chart']['url'] = "https://github.com/SonarQubeCommunity/sonar-motion-chart/releases/download/#{node['sonar']['plugins']['motion-chart']['version']}/sonar-motion-chart-plugin-#{node['sonar']['plugins']['motion-chart']['version']}.jar"
#PMD
default['sonar']['plugins']['pmd']['version'] = '2.6'
default['sonar']['plugins']['pmd']['url'] = "https://github.com/SonarQubeCommunity/sonar-pmd/releases/download/#{node['sonar']['plugins']['pmd']['version']}/sonar-pmd-plugin-#{node['sonar']['plugins']['pmd']['version']}.jar"
#Sonargraph
default['sonar']['plugins']['sonargraph']['version'] = '3.5'
default['sonar']['plugins']['sonargraph']['url'] = "https://github.com/SonarQubeCommunity/sonar-sonargraph/releases/download/sonar-sonargraph-plugin-#{node['sonar']['plugins']['sonargraph']['version']}/sonar-sonargraph-plugin-#{node['sonar']['plugins']['sonargraph']['version']}.jar"
#timeline
default['sonar']['plugins']['timeline']['version'] = '1.5'
default['sonar']['plugins']['timeline']['url'] = "https://github.com/SonarQubeCommunity/sonar-timeline/releases/download/sonar-timeline-plugin-#{node['sonar']['plugins']['timeline']['version']}/sonar-timeline-plugin-#{node['sonar']['plugins']['timeline']['version']}.jar"
#Useless Code Tracker
default['sonar']['plugins']['useless-code-tracker']['version'] = '1.0'
default['sonar']['plugins']['useless-code-tracker']['url'] = "http://central.maven.org/maven2/org/codehaus/sonar-plugins/sonar-useless-code-tracker-plugin/#{node['sonar']['plugins']['useless-code-tracker']['version']}/sonar-useless-code-tracker-plugin-#{node['sonar']['plugins']['useless-code-tracker']['version']}.jar"
