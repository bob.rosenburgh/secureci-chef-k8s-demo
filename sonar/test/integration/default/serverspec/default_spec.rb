require 'spec_helper'

def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

sonar_user        = node['default']['sonar']['user']
sonar_properties  = node['default']['sonar']['conf']
sonar_wrapperconf = node['default']['sonar']['conf']
port       = node['default']['mysql']['port']
socket     = node['default']['mysql']['socket'] 
sqlcommand = "mysql -S #{socket} -e 'SHOW DATABASES;' -uroot -p#{node['default']['mysql']['server_root_password']}"

RSpec::Matchers.define :match_key_value do |key, value|
  match do |actual|
    actual =~ /^\s*?#{key}\s*?=\s*?#{value}/
  end
end

describe 'sonar' do

describe service('sonar') do
  it { should be_enabled }
  it { should be_running }
end
describe user(sonar_user) do
    it { should exist }
end
describe file("#{sonar_properties}/sonar.properties") do
    it { should be_owned_by node['default']['sonar']['user'] }
    it { should be_grouped_into node['default']['sonar']['group'] }
    its(:content) { should match_key_value('sonar.jdbc.username', node['default']['sonar']['jdbc']['user']) }
    its(:content) { should match_key_value('sonar.jdbc.password', node['default']['sonar']['jdbc']['pass']) }
    its(:content) { should match_key_value('sonar.web.port', node['default']['sonar']['port']) }
  end
describe file("#{sonar_wrapperconf}/wrapper.conf") do
    it { should be_owned_by node['default']['sonar']['user'] }
    it { should be_grouped_into node['default']['sonar']['group'] }
    its(:content) { should match_key_value('wrapper.java.command', 'java') }
  end
describe command("update-rc.d sonar defaults") do 
  its(:exit_status) { should eq 0 }
end
describe port(node['default']['sonar']['port']) do
  it { should be_listening }
end
describe command("#{sqlcommand} | grep sonar") do 
  its(:stdout) { should contain("sonar")}
  its(:exit_status) { should eq 0 }
end

end
