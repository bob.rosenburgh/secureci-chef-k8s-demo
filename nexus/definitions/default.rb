define :nexus_config, :json=> nil, :scriptname => nil do # ~FC015

  userpass = node['nexus']['username'] + ":" + node['nexus']['password']
    
  http_request "load docker repo script #{params[:scriptname]}" do
    action :post
    url 'http://localhost:8181/nexus/service/siesta/rest/v1/script/'
    headers ({
      'AUTHORIZATION' => "Basic #{Base64.encode64("#{userpass}")}", # ~FC002 ignored because this is the only way to do it
      'Content-Type' => 'application/json'
    })
    message "#{params[:json]}" # ~FC002
  end

  http_request "run docker repo script #{params[:scriptname]}" do 
    action :post
    url "http://localhost:8181/nexus/service/siesta/rest/v1/script/#{params[:scriptname]}/run"
    headers ({
      'AUTHORIZATION' => "Basic #{Base64.encode64("#{userpass}")}", # ~FC002 ignored because this is the only way to do it
      'Content-Type' => 'text/plain'
    })
  end

  http_request "delete docker repo script #{params[:scriptname]}" do
    action :delete
    url "http://localhost:8181/nexus/service/siesta/rest/v1/script/#{params[:scriptname]}"
    headers ({
      'AUTHORIZATION' => "Basic #{Base64.encode64("#{userpass}")}", # ~FC002 ignored because this is the only way to do it
      'Content-Type' => 'application/json'
    })
  end
end
