default['nexus']['user'] = 'nexus'
default['nexus']['group'] = 'www-data'

default['nexus']['version'] = '3.2.0-01'
default['nexus']['url'] = "https://sonatype-download.global.ssl.fastly.net/nexus/3/nexus-#{node['nexus']['version']}-unix.tar.gz"
default['nexus']['port'] = '8181'

default['nexus']['base'] = '/opt'
default['nexus']['home'] = "#{node['nexus']['base']}/nexus-#{node['nexus']['version']}"
default['nexus']['work'] = "#{node['nexus']['base']}/sonatype-work"
default['nexus']['conf'] = "#{node['nexus']['work']}/nexus3/etc"

default['nexus']['service'] = "nexus"
default['nexus']['startup'] = "#{node['nexus']['home']}/bin/#{node['nexus']['service']}"

default['nexus']['home_link'] = "/var/lib/nexus"
default['nexus']['work_link'] = "/var/lib/sonatype-work"
  
default['nexus']['rest_config']['createdocker']['json']="{ \"name\": \"docker\", \"type\": \"groovy\", \"content\": \"repository.createDockerHosted('docker-internal',8182,0)\"}"
default['nexus']['rest_config']['createdocker']['name']="docker" 
default['nexus']['rest_config']['adminuser']['json']="{ \"name\": \"adminuser\", \"type\": \"groovy\", \"content\": \"security.addUser('#{node['initial_user']}','secureci','coveros','secureci@coveros.com',true,'#{node['password']}',['nx-admin'])\"}"
default['nexus']['rest_config']['adminuser']['name']="adminuser"   
default['nexus']['rest_config']['jenkins_creds']['json']="{ '': '0', 'credentials': { 'scope': 'GLOBAL', 'id': 'docker',  'username': '#{node['initial_user']}', 'password': '#{node['password']}', 'description': 'CI User',  '\$class': 'com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl' } }"  
default['nexus']['username']="admin"
default['nexus']['password']="admin123" 
  
  
  
  