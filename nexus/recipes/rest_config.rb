

nexus_config do
  json node['nexus']['rest_config']['createdocker']['json']
  scriptname node['nexus']['rest_config']['createdocker']['name']
end

nexus_config do
  json node['nexus']['rest_config']['adminuser']['json']
  scriptname node['nexus']['rest_config']['adminuser']['name']
end

#usercommand='curl -X POST \'http://localhost:8080/jenkins/credentials/store/system/domain/_/createCredentials\' --data-urlencode "json={ \'\': \'0\', \'credentials\': { \'scope\': \'GLOBAL\', \'id\': \'docker\', \'username\': \'' + node['initial_user'] + '\', \'password\': \'' + node['password'] + '\', \'description\': \'docker user\', \'\$class\': \'com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl\' } }"'
#execute "jenkins docker user" do
#  command "#{usercommand}"
#end

# This appears to fail, it really doesnt. Its becase jenkins returns a redirect on a post
# and http_request can't handle that. 





# running chef on the instance
# chef-solo -c solo.rb -j dna.json

# list
# curl -v -u admin:admin123 'http://localhost:8181/nexus/service/siesta/rest/v1/script'

# put
# curl -v -u admin:admin123 --header "Content-Type: application/json" 'http://localhost:8181/nexus/service/siesta/rest/v1/script/' -d @dockerCreate.json

# run
# curl -v -X POST -u admin:admin123 --header "Content-Type: text/plain" "http://localhost:8181/nexus/service/siesta/rest/v1/script/docker/run"

# delete
# curl -v -X DELETE -u admin:admin123  "http://localhost:8181/nexus/service/siesta/rest/v1/script/docker"

# update
# curl -v -X PUT -u admin:admin123 --header "Content-Type: application/json" "http://localhost:8181/nexus/service/siesta/rest/v1/script/docker" -d @dockerCreate.json