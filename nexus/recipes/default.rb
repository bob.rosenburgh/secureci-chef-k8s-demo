#
# Cookbook Name:: nexus
# Recipe:: default
# Author:: Max Saperstone
#
# Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
include_recipe "java_oracle"
include_recipe "curl"

user node['nexus']['user'] do
  system true
  shell "/bin/false"
  action :create
end

directory node['nexus']['base'] do
  owner node['nexus']['user']
  group node['nexus']['group']
  recursive true
  mode 00755
end

#install nexus
ark "nexus-#{node['nexus']['version']}" do
  url node['nexus']['url']
  version node['nexus']['version']
  path node['nexus']['base']
  home_dir node['nexus']['home_link']
  owner node['nexus']['user']
  group node['nexus']['group']
  action :put
end

#fix our workspace install
directory node['nexus']['work']
execute "Fixing Nexus Workspace Install" do
  command "mv #{node['nexus']['home']}/nexus3 #{node['nexus']['work']}/"
  not_if { ::File.exists?("#{node['nexus']['work']}/nexus3")}
end

#make soft link from nexus home
link node['nexus']['home_link'] do
  action :delete
  only_if "test -L #{node['nexus']['home_link']}"
end
link node['nexus']['home_link'] do
  to node['nexus']['home']
  action :create
end

#make soft link from nexus workspace
link node['nexus']['work_link'] do
  action :delete
  only_if "test -L #{node['nexus']['work_link']}"
end
link node['nexus']['work_link'] do
  to node['nexus']['work']
  action :create
end

###Fixing Nexus Service
template "/etc/systemd/system/nexus.service" do
  source "service.erb"
  mode '0755'
  owner node['nexus']['user']
  group node['nexus']['group']
end


directory node['nexus']['conf']
template "#{node['nexus']['conf']}/nexus.properties" do
  source "nexus.properties.erb"
  mode '0644'
  owner node['nexus']['user']
  group node['nexus']['group']
  notifies :restart, 'service[nexus]', :immediately
end

#add nexus startup on boot
service 'nexus' do
  action [ :enable, :start ]
  notifies :run, 'bash[wait_for_server]', :delayed
end

bash "wait_for_server" do
  code <<-EOH
    run=1;
    while [ $run -ne 0 ] ; do
        curl -k http://localhost:8181/nexus/ 1>/dev/null 2>/dev/null
        run=$?
    done
  EOH
end