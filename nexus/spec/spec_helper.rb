require 'chefspec'
require 'chefspec/berkshelf'

def install_ark(resource_name)
  ChefSpec::Matchers::ResourceMatcher.new(:ark, :put, resource_name)
end
