require 'spec_helper'

describe 'nexus::default' do

 before do
    stub_command('/usr/sbin/apache2 -t').and_return('Syntax OK')
    stub_command("test -L /opt/nexus").and_return('Syntax OK')
    stub_command("test -L /var/lib/nexus").and_return('Syntax OK')
    stub_command('/usr/sbin/httpd -t').and_return('Syntax OK')
    stub_command("test -L /var/lib/sonatype-work").and_return('Syntax OK')  

    allow(File).to receive(:exists?).with(anything).and_call_original
    allow(File).to receive(:exists?).with("/opt/sonatype-work/nexus3").and_return(true)
 end

let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end

let :nexus_user do 
     chef_run.node['nexus']['user']
  end
let :nexus_group do
     chef_run.node['nexus']['group']
  end
let :nexus_base do 
     chef_run.node['nexus']['base']
  end
let :nexus_version do 
     chef_run.node['nexus']['version']
  end
let :nexus_work do 
     chef_run.node['nexus']['work']
  end
let :nexus_home do 
     chef_run.node['nexus']['home']
  end
let :nexus_conf do 
     chef_run.node['nexus']['conf']
  end
let :nexus_homelink do
     chef_run.node['nexus']['home_link']
  end
let :nexus_worklink do
     chef_run.node['nexus']['work_link']
  end
it 'should create nexus user' do
    expect(chef_run).to create_user nexus_user
  end
it 'should set the base dir user/group' do
    expect(chef_run).to create_directory nexus_base
  end
it "should install nexus3 via ark" do
    expect(chef_run).to put_ark "nexus-#{nexus_version}"
  end
it 'should set the nexus work dir' do
    expect(chef_run).to create_directory nexus_work
  end
it 'should set the nexus conf dir' do
   expect(chef_run).to create_directory nexus_conf
  end
it "execute 'Fixing Nexus Workplace install'" do
    expect(chef_run).to_not run_execute("mv #{nexus_home}/nexus3 #{nexus_work}/")
  end 
it 'creates soft link' do
    link = chef_run.link(nexus_homelink)
    expect(link).to link_to(nexus_home)
  end
it 'creates soft link' do
    link = chef_run.link(nexus_worklink)
    expect(link).to link_to(nexus_work)
   end
it 'creates a template with attributes' do
    expect(chef_run).to create_template("#{nexus_conf}/nexus.properties").with(
      source: 'nexus.properties.erb',
      mode:   '0644',
      owner:   nexus_user,
      group:   nexus_group,
    ) 
 end
it 'creates a template with attributes' do
    expect(chef_run).to create_template("/etc/systemd/system/nexus.service").with(
      source: 'service.erb',
      mode:   '0755',
      owner:   nexus_user,
      group:   nexus_group,
   )
 end
it 'executes restart on nexus' do
    expect(chef_run).to enable_service('nexus')
  end
at_exit { ChefSpec::Coverage.report! }
end
