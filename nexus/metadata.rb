name             "nexus"
maintainer       "Max Saperstone"
maintainer_email "max.saperstone@coveros.com"
license          "All Rights Reserved"
description      "Install nexus into our application"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "install nexus"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.0.11"

supports 'ubuntu'

depends 'ark'
depends 'java_oracle'
depends 'curl'
depends 'apache2'

# 0.0.11 - updating unit tests
# 0.0.10 - food critic fixes
# 0.0.9 - tying nexus home to version
# 0.0.8 - rewriting for nexus 3.2
# 0.0.7 - updating nexus version to 3.1
# 0.0.6 - updating nexus version
# 0.0.5 - installing nexus via tar instead of using war within tomcat7
# 0.0.4 - making nexus upgradable
# 0.0.3 - adding in configuration
# 0.0.2 - updated to 2.11 version of nexus. added in apache port forwarding
# 0.0.1 - initial nexus recipe
