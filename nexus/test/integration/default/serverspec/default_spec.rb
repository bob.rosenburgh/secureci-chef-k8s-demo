require 'spec_helper'

def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

nexus_user        = node['default']['nexus']['user']
nexus_group       = node['default']['nexus']['group']
nexus_home        = node['default']['nexus']['home']
nexus_version     = node['default']['nexus']['version'] 
nexus_work        = node['default']['nexus']['work']

RSpec::Matchers.define :match_key_value do |key, value|
  match do |actual|
    actual =~ /^\s*?#{key}\s*?=\s*?#{value}/
  end
end

describe 'nexus' do

describe user(nexus_user) do
    it { should exist }
end



describe service('nexus') do
  it { should be_running }
end

describe file( node['default']['nexus']['home_link']) do
  it { should be_linked_to nexus_home }
end

describe file('/etc/systemd/system/nexus.service') do
  it { should exist }
  it { should be_owned_by nexus_user }
  it { should be_grouped_into nexus_group }
end

describe file(node['default']['nexus']['work_link']) do
  it { should be_linked_to node['default']['nexus']['work'] }
end

describe file("#{nexus_home}") do
  it { should be_directory }
  it { should be_owned_by nexus_user }
  it { should be_grouped_into nexus_group }
  it { should be_readable.by 'others'}
end
describe file("#{nexus_work}/nexus3/etc/nexus.properties") do
    it { should be_owned_by node['default']['nexus']['user'] }
    it { should be_grouped_into node['default']['nexus']['group'] }
    its(:content) { should match_key_value('application-port', node['default']['nexus']['port']) }
    its(:content) { should match_key_value('nexus-context-path', '/nexus') }
  end
end
describe port(node['default']['nexus']['port']) do
  it { should be_listening }
end