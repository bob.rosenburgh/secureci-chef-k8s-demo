require 'spec_helper'

describe 'tomcat7::default' do
  let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
  end
  it 'installs Tomcat7' do
    chef_run.converge(described_recipe)
    expect(chef_run).to install_package('tomcat7').with(
      version: chef_run.node['tomcat']['install']['version'] 
      )
  end
end

describe 'tomcat7::secureci' do 
  let :chef_run do 
   ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end
 it 'converges successfully' do
  chef_run.converge(described_recipe)
  expect { chef_run }.to_not raise_error
end
it 'sets correct java version' do
  expect(chef_run).to run_execute('update-java-alternatives -s java-7-oracle-amd64')
end
it 'creates a template with attributes' do
  expect(chef_run).to create_template('/etc/tomcat7/tomcat-users.xml').with(
    source: 'tomcat-users.xml.erb',
    mode:   '0640',
    owner:  'root',
    group:  'tomcat7',
    ) 
end
it 'creates a template with attributes' do
  expect(chef_run).to create_template('/etc/tomcat7/server.xml').with(
    source: 'server.xml.erb',
    mode:   '0644',
    owner:  'root',
    group:  'tomcat7',
    ) 
end
it 'sets tomcat defaults' do
 expect(chef_run).to run_execute('update-rc.d tomcat7 defaults')
end
it 'creates a template with attributes' do
  expect(chef_run).to create_template('/usr/share/tomcat7/bin/setenv.sh').with(
    source: 'setenv.sh.erb',
    mode:   '0755',
    owner:  'root',
    group:  'root',
    ) 
end
it 'creates a template with attributes' do
  expect(chef_run).to create_template('/etc/tomcat7/context.xml').with(
    source: 'context.xml.erb',
    mode:   '0755',
    ) 
end
at_exit { ChefSpec::Coverage.report! }
end
