name             "tomcat7"
maintainer       "Max Saperstone"
maintainer_email "max.saperstone@coveros.com"
license          "All Rights Reserved"
description      "Initializes and updates our empty system"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "installs tomcat7"
recipe           "secureci", "configures the secureci settings"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.0.6"

supports 'ubuntu'

depends 'apt'
depends 'java_oracle'
depends 'openssl'

#0.0.6 - updating unit tests
#0.0.5 - food critic fixes
#0.0.4 - upgrading tomcat / adding unit and integration tests
#0.0.3 - tied tomcat install to version specified in attributes
#0.0.2 - adding context.xml to setup jenkins and nexus home
#0.0.1 - setting initial recipe
