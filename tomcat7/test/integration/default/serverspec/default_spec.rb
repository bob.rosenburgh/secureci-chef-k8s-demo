require 'spec_helper'

describe 'Tomcat Version' do
  let(:node) { JSON.parse(IO.read('/tmp/kitchen/chef_node.json')) }

  describe command("/usr/share/tomcat7/bin/version.sh ") do
    its(:stdout) { should contain(node['default']['tomcat']['version'])}
    its(:exit_status) { should eq 0 }
  end
end

describe 'Tomcat Service' do
  let(:node) { JSON.parse(IO.read('/tmp/kitchen/chef_node.json')) }

  describe user("tomcat7") do
    it { should exist }
  end

  describe group("tomcat7") do
    it { should exist }
  end

describe file("/etc/tomcat7/server.xml") do
    it { should be_owned_by 'root' }
    it { should be_grouped_into node['default']['tomcat']['group'] }
  end

describe file("/etc/tomcat7/tomcat-users.xml") do
    it { should be_owned_by 'root' }
    it { should be_grouped_into node['default']['tomcat']['group'] }
  end

describe file("/usr/share/tomcat7/bin/setenv.sh") do
    it { should be_owned_by     'root' }
    it { should be_grouped_into 'root' }
  end

  describe service("tomcat7") do
    it { should be_enabled }
    it { should be_running }
  end

  describe port('8080') do
    it { should be_listening }
  end
end
