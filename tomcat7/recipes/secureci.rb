#
# Cookbook Name:: tomcat7
# Recipe:: secureci
#
# Copyright 2014, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe "apt"
include_recipe "tomcat7"
include_recipe "java_oracle"
include_recipe "java_oracle::update-alternatives"
#TODO - consider removing openjdk

template "/etc/tomcat7/tomcat-users.xml" do
  source "tomcat-users.xml.erb"
  mode '0640'
  owner 'root'
  group 'tomcat7'
end

template "/etc/tomcat7/server.xml" do
  source "server.xml.erb"
  mode '0644'
  owner 'root'
  group 'tomcat7'
end

execute "setting tomcat7 defaults" do
  command "update-rc.d tomcat7 defaults"
end

template "/usr/share/tomcat7/bin/setenv.sh" do
  source "setenv.sh.erb"
  mode '0755'
  owner 'root'
  group 'root'
end

#setting up application homes
template "/etc/tomcat7/context.xml" do
  source "context.xml.erb"
  mode '0755'
  owner node['jenkins']['user']
  group node['jenkins']['group']
end
