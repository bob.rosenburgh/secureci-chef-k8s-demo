default['tomcat']['version'] = "7.0.68"
default['tomcat']['install']['version'] = "#{node['tomcat']['version']}-1ubuntu0.1"

default['tomcat']['install_dir'] = "/var/lib/tomcat7"
default['tomcat']['webaps_dir'] = "#{node['tomcat']['install_dir']}/webapps"

default['tomcat']['user'] = 'tomcat7'
default['tomcat']['group'] = 'tomcat7'
