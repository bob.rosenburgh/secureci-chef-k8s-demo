default['git']['user'] = 'git'
default['git']['group'] = 'www-data'
default['git']['version'] = '2.14.2'
default['git']['install']['version'] = "1:#{node['git']['version']}-1~ppa0~ubuntu16.04.1"

default['gitblit']['user'] = 'gitblit'
default['gitblit']['port'] = '8082'
default['gitblit']['properties'] = '/var/lib/tomcat7/webapps/gitblit/WEB-INF/data/gitblit.properties'
default['gitblit']['version'] = '1.8.0'
default['gitblit']['url'] = "http://dl.bintray.com/gitblit/releases/gitblit-#{node['gitblit']['version']}.tar.gz"

default['gitblit']['storePassword'] = "secureci"
default['gitblit']['base_url'] = "https://secureci/gitblit"
default['gitblit']['base_ip'] = "https://#{node['ipaddress']}"
default['gitblit']['base'] = '/opt'
default['gitblit']['home'] = "#{node['gitblit']['base']}/gitblit-#{node['gitblit']['version']}"
default['gitblit']['data'] = "#{node['gitblit']['home']}/data"
default['gitblit']['groovy'] = "#{node['gitblit']['data']}/groovy"
default['gitblit']['repo'] = "#{node['gitblit']['data']}/git"
default['gitblit']['repo_link'] = "/var/lib/git"
default['gitblit']['home_link'] = "/var/lib/gitblit"

#needs to be here for java_oracle to install the correct version, it's a bug that needs to be fixed
default['java']['jdk_version'] = '8'
