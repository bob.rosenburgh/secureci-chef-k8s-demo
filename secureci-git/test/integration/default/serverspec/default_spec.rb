require 'spec_helper'

def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end

git_user         = node['default']['git']['user']
git_version      = node['default']['git']['version'] 

gitblit_user     = node['default']['gitblit']['user']
gitblit_version  = node['default']['gitblit']['version']
gitblit_homelink  = node['default']['gitblit']['home_link']
gitblit_home     = node['default']['gitblit']['home']
gitblit_repolink = node['default']['gitblit']['repo_link']
gitblit_link     = node['default']['gitblit']['link']
gitblit_repo     = node['default']['gitblit']['repo']

describe 'git' do

describe user(git_user) do
    it { should exist }
 end
describe command("git --version") do 
  its(:exit_status) { should eq 0 }
 end
describe command("git config --global user.name") do 
  its(:stdout) { should contain("SecureCI")}
  its(:exit_status) { should eq 0 }
 end
end

describe 'gitblit' do

describe user(gitblit_user) do
   it { should exist }
 end
describe file(gitblit_homelink) do
  it { should be_linked_to gitblit_home }
end
describe file(gitblit_repolink) do
  it { should be_linked_to gitblit_repo }
end
describe file("/etc/init.d/gitblit") do
  it { should exist }
end
end
