require 'spec_helper'

describe 'secureci-git::default' do

  before do
    stub_command('/usr/sbin/apache2 -t').and_return('Syntax OK')
    stub_command("test -L /opt/gitblit").and_return('Syntax OK')
    stub_command("test -L /var/lib/gitblit").and_return('Syntax OK')
    stub_command("test -L /var/lib/git").and_return('Sytax OK')
  end

  let :chef_run do
    ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
  end
  let :git_user do 
    chef_run.node['git']['user']
  end
  let :git_version do
   chef_run.node['git']['install']['version']
 end

 it 'should update repo' do
  chef_run.converge(described_recipe)
  expect(chef_run).to run_execute("sudo apt-get update")
end
it 'should install git via git_client' do
  expect(chef_run).to install_git_client 'default'
end
it 'creates a git user' do 
  expect(chef_run).to create_user(git_user)
end
it 'should setup git user' do 
  expect(chef_run).to set_git_config('Setup git user').with(
   scope:  'global',
   key:    'user.name',
   value:  'SecureCI'
   )
end
it 'should setup git email' do  
  expect(chef_run).to set_git_config('Setup git email').with(
   scope:  'global',
   key:    'user.email',
   value:  ''
   )
end
it 'adds new aptitude repo' do
  expect(chef_run).to run_execute("sudo add-apt-repository ppa:git-core/ppa -y")
end
it 'includes cookbook git::gitblit' do
  expect(chef_run).to include_recipe('secureci-git::gitblit')
end

end

describe 'secureci-git::gitblit' do 

  before do
   allow(File).to receive(:exists?).with(anything).and_call_original
   allow(File).to receive(:exists?).with('/var/lib/jenkins').and_return true
   
   allow(File).to receive(:symlink?).with(anything).and_call_original
   allow(File).to receive(:symlink?).with('/var/lib/git').and_return true
   
   stub_command('/usr/sbin/apache2 -t').and_return('Syntax OK')
   stub_command("test -L /opt/gitblit").and_return('Syntax OK')
   stub_command("test -L /var/lib/gitblit").and_return('Syntax OK')
   stub_command("test -L /var/lib/git").and_return('Syntax OK')
 end

 let :chef_run do
   ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
 end
 let :gitblit_user do 
   chef_run.node['gitblit']['user']
 end
 let :gitblit_group do
   chef_run.node['git']['group']
 end
 let :gitblit_base do 
   chef_run.node['gitblit']['base']
 end
 let :gitblit_home do 
   chef_run.node['gitblit']['home']
 end
 let :gitblit_homelink do 
   chef_run.node['gitblit']['home_link']
 end
 let :gitblit_repo do
   chef_run.node['gitblit']['repo']
 end
 let :gitblit_repolink do 
   chef_run.node['gitblit']['repo_link']
 end
 let :gitblit_groovy do 
   chef_run.node['gitblit']['groovy']
 end
 let :gitblit_data do
   chef_run.node['gitblit']['data']
 end
 let :gitblit_version do
   chef_run.node['gitblit']['version']
 end
 it 'includes cookbook java_oracle' do
  expect(chef_run).to include_recipe('java_oracle')
end
it 'creates a gitblit user' do
  expect(chef_run).to create_user(gitblit_user)
end
it 'should install gitblit via ark' do
  expect(chef_run).to put_ark("gitblit-#{gitblit_version}")
end
it 'should delete dir' do
  expect(chef_run).to_not delete_directory(gitblit_repolink)
end
it 'deletes soft link' do
  expect(chef_run).to delete_link(gitblit_homelink).with(link_type: :symbolic)
end
it 'creates soft link' do
  link = chef_run.link(gitblit_homelink)
  expect(link).to link_to(gitblit_home)
end
it 'deletes soft link' do
  expect(chef_run).to delete_link(gitblit_repolink).with(link_type: :symbolic)
end
it 'creates soft link' do
  link = chef_run.link(gitblit_repolink)
  expect(link).to link_to(gitblit_repo)
end
%w[ gitblit.properties users.conf ].each do |template_file|
 it 'should create templates' do 
  expect(chef_run).to create_template("#{gitblit_data}/#{template_file}").with(
    source: "#{template_file}.erb",
    owner:   gitblit_user,
    group:   gitblit_group,
    )
end
end
%w[ jenkins.groovy ].each do |template_file|
 it 'should create templates' do  
  expect(chef_run).to create_template("#{gitblit_groovy}/#{template_file}").with(
    source: "#{template_file}.erb",
    owner:   gitblit_user,
    group:   gitblit_group,
    )   
end 
end 
%w[ local_policy US_export_policy ].each do |security_jar|
  it 'should create cookbook files' do 
    java_home = chef_run.node['java']['home'] 
    expect(chef_run).to create_cookbook_file("#{security_jar}.jar").with(
      path: "#{java_home}/jre/lib/security/#{security_jar}.jar",
      )    
  end
end 
it 'should create a template' do  
  expect(chef_run).to create_template("#{gitblit_home}/service-ubuntu.sh").with(
    source: "service-ubuntu.sh.erb",
    mode:   '0755',
    owner:   gitblit_user,
    group:   gitblit_group,
    )   
end 
it 'sets gitblit as a service' do
 expect(chef_run).to run_execute('./install-service-ubuntu.sh')
end
it 'executes restart on gitblit' do
  expect(chef_run).to restart_service('gitblit')
end
end
at_exit { ChefSpec::Coverage.report! }
