#
# Cookbook Name:: git
# Recipe:: gitblit
# Author:: Max Saperstone
#
# Copyright 2015, Coveros
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe "java_oracle"

#setup our gitblit user
user node['gitblit']['user'] do
  system true
  shell "/bin/false"
  action :create
end

#install gitblit
ark "gitblit-#{node['gitblit']['version']}" do
  url node['gitblit']['url']
  version node['gitblit']['version']
  path node['gitblit']['base']
  home_dir node['gitblit']['home_link']
  owner node['gitblit']['user']
  group node['gitblit']['group']
  action :put
end

#make soft link from gitblit version to gitblit
link node['gitblit']['home_link'] do
  action :delete
  only_if "test -L #{node['gitblit']['home_link']}"
end
link node['gitblit']['home_link'] do
  to node['gitblit']['home']
  action :create
end

#this link was created when installing git
directory node['gitblit']['repo_link'] do
  action :delete
  not_if { ::File.symlink?(node['gitblit']['repo_link']) }
end
#want to link our repo directory to our gitblit data dir
link node['gitblit']['repo_link'] do
  action :delete
  only_if "test -L #{node['gitblit']['repo_link']}"
end
link node['gitblit']['repo_link'] do
  to node['gitblit']['repo']
  action :create
end

#configure gitblit
%w[ gitblit.properties users.conf ].each do |template_file|
  template "#{node['gitblit']['data']}/#{template_file}" do
    source "#{template_file}.erb"
    mode '0644'
    owner node['gitblit']['user']
    group node['git']['group']
    variables({ :is_package => node['package']})
  end
end

#configure groovy scripts from templates
%w[ jenkins.groovy ].each do |template_file|
  template "#{node['gitblit']['groovy']}/#{template_file}" do
    source "#{template_file}.erb"
    mode '0644'
    owner node['gitblit']['user']
    group node['git']['group']
    variables({ :is_package => node['package']})
    only_if { ::File.exists?(node['jenkins']['master']['home'])}
  end
end

#need to install update security jars
%w[ local_policy US_export_policy ].each do |security_jar|
  cookbook_file "#{security_jar}.jar" do
    path "#{node['java']['home']}/jre/lib/security/#{security_jar}.jar"
    action :create
  end
end

#update our startup template
template "#{node['gitblit']['home']}/service-ubuntu.sh" do
  source "service-ubuntu.sh.erb"
  mode '0755'
  owner node['gitblit']['user']
  group node['git']['group']
end

#need to setup gitblit as a service
execute "setup gitblit as service" do
  cwd node['gitblit']['home']
  command "./install-service-ubuntu.sh"
  user "root"
  action :run
end

service "gitblit" do
  action :restart
end
