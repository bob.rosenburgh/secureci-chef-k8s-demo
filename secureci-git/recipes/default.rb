#
# Cookbook Name:: git
# Recipe:: default
# Author:: Max Saperstone
#
# Copyright 2017, Coveros

include_recipe 'aptupdate'

#add ppa containing our git version
execute "Adding our new aptitude repo" do
  command "sudo add-apt-repository ppa:git-core/ppa -y"
end

#Updating our repo
execute "Update repo" do 
  command "sudo apt-get update"
end

#install git
git_client 'default' do
  action :install
end

#setup our git user
user node['git']['user'] do
  system true
  shell "/bin/false"
  action :create
end

#setup our base configs
git_config 'Setup git user' do
   scope 'global'
   key 'user.name'
   value 'SecureCI'
end
git_config 'Setup git email' do
   scope 'global'
   key 'user.email'
   value ''
end

include_recipe 'secureci-git::gitblit'
