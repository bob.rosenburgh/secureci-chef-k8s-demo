name             "secureci-git"
maintainer       "Max Saperstone"
maintainer_email "max.saperstone@coveros.com"
license          "All Rights Reserved"
description      "Install git and tools into our application"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
recipe           "default", "install git"
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version          "0.1.1"

supports 'ubuntu'

depends 'apt'
depends 'aptupdate'
depends 'git'
depends 'ark'
depends 'java_oracle'


# 0.1.1  - updating unit tests
# 0.1.0  - rewritten to use opscode git cookbook
# 0.0.16 - updating gitblit to use ark, and fixing links
# 0.0.15 - updating web url to use hostname instead of IP
# 0.0.14 - set the port location to reference the attributes
# 0.0.13 - set the htpasswd location to reference the subversion attribute
# 0.0.12 - upgrade to git 2.10.2
# 0.0.11 - upgrade to git 2.9.1
# 0.0.10 - upgrading git to 2.6.3
# 0.0.9 - upgrading gitblit from 1.6.2 to 1.7.1
# 0.0.7 - fix generation of gitblit users.conf from template to ensure that proper users 
#         are present for both package and no-package builds
# 0.0.6 - adding in admin user for gitblit
# 0.0.5 - re-organized gitblit links, fixed some bugs in properties file
# 0.0.4 - allow any authenticated user admin access
# 0.0.3 - tied git install to version specified in attributes
# 0.0.1 - initial git recipes
