#
# Cookbook:: microk8s
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

# Install Microk8s v1.13.
# TODO: Upgrade version when insecure registry support is fixed.
execute "install-microk8s" do
    command "sudo snap install microk8s --classic --channel=1.13/stable; microk8s.status --wait-ready; sudo snap alias microk8s.kubectl kubectl; sudo apt-get install bash-completion"
    action :run
end

execute "install-helm" do
    command "sudo snap install helm --classic; sudo helm init;"
    action :run
end

execute "Enable and Expose Dashboard" do
  # command "microk8s.enable registry dns dashboard; microk8s.kubectl -n kube-system patch svc kubernetes-dashboard  -p '{\"spec\": {\"type\": \"NodePort\", \"ports\":[{\"port\" : 443, \"nodePort\" : 30000}]}}'"
  command "microk8s.enable dns registry dashboard;"
  action :run
end

execute "Enable port forwarding" do
  # TODO: what about -> sudo apt-get install iptables-persistent
  command "sudo iptables -P FORWARD ACCEPT"
end

# DEPRECATED: Required only for microk8s <1.14
execute "Enable k8s kubectl allow-privileged" do
  command "sudo echo '--allow-privileged=true' >> /var/snap/microk8s/current/args/kubelet; sudo systemctl restart snap.microk8s.daemon-kubelet.service; "
end

execute "Enable k8s apiserver allow-privileged" do
  command "sudo echo '--allow-privileged=true' >> /var/snap/microk8s/current/args/kube-apiserver; sudo systemctl restart snap.microk8s.daemon-apiserver.service; "
end

#TODO: docker permissions?   Warning  FailedKillPod     12m (x9 over 14m)  kubelet, secureci  error killing pod: [failed to "KillContainer" for "jenkins-demo-postgresql" with KillContainerError: "rpc error: code = Unknown desc = Error response from daemon: cannot stop container: ab8d4234a2374564624678e595d869e6db867270ed5fadd43dad2a77f1bc0590: Cannot kill container ab8d4234a2374564624678e595d869e6db867270ed5fadd43dad2a77f1bc0590: unknown error after kill: docker-runc did not terminate #sucessfully: container_linux.go:387: signaling init process caused \"permission denied\"\n: unknown"
#, failed to "KillPodSandbox" for "37a31d4d-ae38-11e9-acde-0ac585d49212" with KillPodSandboxError: "rpc error: code = Unknown desc = Error response from daemon: cannot stop container: ab8456063b0154a7dd7f3fcfd459f6b083dca7a7802ab0efd0eee6ecf7db1f03: Cannot kill container ab8456063b0154a7dd7f3fcfd459f6b083dca7a7802ab0efd0eee6ecf7db1f03: unknown error after kill: docker-runc did not terminate sucessfully: container_linux.go:387: signaling init process caused \"permission denied\"\n: unknown"

# TODO: move elsewhere?
# execute "Install Jenkins Helm Chart" do
#   command "git clone git@gitlab.com:bob.rosenburgh/securecidemo-jenkins-k8s.git /home/ubuntu/securecidemo-jenkins-k8s; cd /home/ubuntu/securecidemo-jenkins-k8s; ./install.sh;"
# end


# template "/tmp/dummy-pod-nginx.yaml" do
#    source "dummy-pod-nginx.yaml.erb"
#    mode "0644"
# end
# execute "Deploy dummy pod" do
#   command "microk8s.kubectl apply -f /tmp/dummy-pod-nginx.yaml"
#   action :run
# end
#
# template "/tmp/dummy-pod-mysql.yaml" do
#   source "dummy-pod-mysql.yaml.erb"
#   mode "0644"
# end
# execute "Deploy dummy pod" do
#  command "microk8s.kubectl apply -f /tmp/dummy-pod-mysql.yaml"
#  action :run
# end
#
# template "/tmp/dummy-pod-tomcat1.yaml" do
#   source "dummy-pod-tomcat1.yaml.erb"
#   mode "0644"
# end
# execute "Deploy dummy pod" do
#  command "microk8s.kubectl apply -f /tmp/dummy-pod-tomcat1.yaml"
#  action :run
# end
#
# template "/tmp/dummy-pod-tomcat2.yaml" do
#   source "dummy-pod-tomcat2.yaml.erb"
#   mode "0644"
# end
# execute "Deploy dummy pod" do
#  command "microk8s.kubectl apply -f /tmp/dummy-pod-tomcat2.yaml"
#  action :run
# end
