name 'secureci-perl'
maintainer 'Coveros, Inc'
maintainer_email 'walter.mitchell@coveros.com'
license 'All Rights Reserved'
description 'Installs/Configures secureci-perl'
long_description 'Installs/Configures secureci-perl'
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"
chef_version     '>= 13.0' if respond_to?(:chef_version)

version '0.1.0'

supports 'ubuntu'

depends 'perl'


