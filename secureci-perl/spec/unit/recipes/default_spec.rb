require 'spec_helper'

describe 'perl::default' do

	let :chef_run do
		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
	end

	it 'installs perl' do
		chef_run.converge(described_recipe)
		expect(chef_run).to install_package('perl')
	end
	it 'installs libperl-dev' do
		expect(chef_run).to install_package('libperl-dev')
	end
end
describe 'secureci-perl::default' do

	let :chef_run do
		ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe)
	end

	it 'installs libpasswd-unix-perl' do
		chef_run.converge(described_recipe)
		expect(chef_run).to install_package('libpasswd-unix-perl')
	end
	it 'installs libapache-htpasswd-perl' do
		expect(chef_run).to install_package('libapache-htpasswd-perl')
	end
	it 'installs libnet-server-perl' do
		expect(chef_run).to install_package('libnet-server-perl')
	end
	at_exit { ChefSpec::Coverage.report! }
end
