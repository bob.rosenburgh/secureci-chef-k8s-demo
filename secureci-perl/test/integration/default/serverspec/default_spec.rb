require 'spec_helper'

describe 'perl' do

describe command('perl -v') do
  its('exit_status') { should eq 0 }
 end
describe command('sudo apt-get install libpasswd-unix-perl') do
  its(:stdout) { should contain("libpasswd-unix-perl is already the newest version")}
  its('exit_status') { should eq 0 }
end
describe command('sudo apt-get install libapache-htpasswd-perl') do
  its(:stdout) { should contain("libapache-htpasswd-perl is already the newest version")}
  its('exit_status') { should eq 0 }
end
describe command('sudo apt-get install libnet-server-perl') do
  its(:stdout) { should contain("libnet-server-perl is already the newest version")}
  its('exit_status') { should eq 0 }
end
end
