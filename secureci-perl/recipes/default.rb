#
# Cookbook Name:: secureci-perl
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

include_recipe "perl::default"

%w{libpasswd-unix-perl libapache-htpasswd-perl libnet-server-perl}.each do |pkg|
  package pkg do
    action :install
  end
end
