require 'spec_helper'


def node
  JSON.parse(IO.read('/tmp/kitchen/chef_node.json'))
end


describe 'initialuser::default' do
  # Serverspec examples can be found at
  # http://serverspec.org/resource_types.html

user = node['normal']['initial_user']
key  = node['normal']['key']

describe user(user) do
    it { should exist }
 end

describe command("sudo cat /home/#{user}/.ssh/authorized_keys") do 
  its(:stdout) { should contain("#{key}")}
 end

end