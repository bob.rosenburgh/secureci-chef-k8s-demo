#
# Cookbook Name:: initialuser
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

# This package is required to set the user password.
include_recipe 'openssl'
chef_gem "ruby-shadow"

Chef::Recipe.send(:include, OpenSSLCookbook::RandomPassword)

#password = node['password']
salt = random_password(length: 10)
crypt_password = node['password'].crypt("$6$#{salt}")

#create our user
user node['initial_user'] do
    comment  'Initial user for login.'
    manage_home true
    home     "/home/#{node['initial_user']}"
    shell    "/bin/bash"
    password crypt_password
end

group 'sudo' do
    append   true
    members  node['initial_user']
end

directory "/home/#{node['initial_user']}/.ssh"

file "/home/#{node['initial_user']}/.ssh/authorized_keys" do
    content  node['sshkey']
    mode     '600'
    owner    node['initial_user']
end


