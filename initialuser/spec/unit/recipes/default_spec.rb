#
# Cookbook Name:: initialuser
# Spec:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.

require 'spec_helper'

describe 'initialuser::default' do
	context 'When all attributes are default, on an unspecified platform' do
		let(:chef_run) do
			runner = ChefSpec::ServerRunner.new
			runner.converge(described_recipe)
		end

		it 'converges successfully' do
			expect { chef_run }.to_not raise_error
		end
		let :initial_user do
			chef_run.node['initial_user']
		end

		it 'installs ruby-shadow via chef_gem' do
			expect(chef_run).to install_chef_gem('ruby-shadow')
		end
		it 'Creates hashed password' do 
			expect(chef_run).to_not run_ruby_block('Create hashed password')
		end
		it 'sets up initial user' do 
			expect(chef_run).to create_user(initial_user)
		end
		it 'adds initial user to group' do
			expect(chef_run).to create_group('sudo')
		end
		it 'creates dir' do
			expect(chef_run).to create_directory("/home/#{initial_user}/.ssh")
		end
		it 'create a file' do 
			expect(chef_run).to create_file("/home/#{initial_user}/.ssh/authorized_keys")
		end
		before(:each) do
			allow(File).to receive(:directory?).with(anything).and_call_original
			allow(File).to receive(:directory?).with('home/ubuntu').and_return true 
			allow(File).to receive(:directory?).with('home/ubuntu/.ssh').and_return true 

			allow(File).to receive(:exists?).with(anything).and_call_original
			allow(File).to receive(:exists?).with('home/ubuntu/.ssh/authorized_keys').and_return true
		end
		let :initial_user do
			chef_run.node['initial_user']
		end
	end
end
