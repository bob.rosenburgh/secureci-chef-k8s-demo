name 'aptupdate'
maintainer 'Glenn Buckholz'
maintainer_email 'glenn.buckholz@coveros.com'
license 'All Rights Reserved'
description 'Installs/Configures aptupdate'
long_description 'Installs/Configures aptupdate'
issues_url       "https://gitlab.com/coveros/secureci-chef/issues"
source_url       "https://gitlab.com/coveros/secureci-chef"

version '0.1.0'
chef_version     '>= 13.0' if respond_to?(:chef_version)

supports 'ubuntu'

