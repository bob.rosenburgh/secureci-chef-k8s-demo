#
# Cookbook Name:: aptupdate
# Recipe:: default
#
# Copyright (c) 2017 The Authors, All Rights Reserved.


execute "aptcache" do
	command "apt update"
end

# Per https://github.com/torch/torch7/issues/1146 comment out python-software-properties
# package %w( software-properties-common python-software-properties cron)
package %w( software-properties-common cron)
